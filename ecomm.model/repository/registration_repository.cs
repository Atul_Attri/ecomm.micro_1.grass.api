﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;
using ecomm.util.entities;
using ecomm.dal;
using System.Data;
using System.Security.Cryptography;
using ecomm.util;
using MongoDB.Bson;
using MongoDB.Driver;
namespace ecomm.model.repository
{
    public class registration_repository
    {

        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        /// <summary>
        /// GetAccountActivationMailBody : Get the Activation Email Body
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public string GetAccountActivationMailBody(string userId, string guid, string strCompany)
        {
            string mailBody = string.Empty;
            string strCompanyURL = "";
            string strdisplayname = "";
            string strLogo = "";

            try
            {
                mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
                mailBody += "<head>";
                mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                mailBody += "<style type='text/css'>";
                mailBody += "* {";
                mailBody += "padding:0px;";
                mailBody += "margin:0px;";
                mailBody += "}";
                mailBody += "</style>";
                mailBody += "</head>";
                mailBody += "<body>";
                mailBody += "<div style='width: 100%; position: relative; float: left'>";
                mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
                mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
                mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
                mailBody += "<tr>";
                mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='222' height='54' /></a></td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
                mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
                mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                mailBody += "<tr>";
                mailBody += "<td height='75' align='center' valign='middle'>Account Activation</td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "<div style='width:620px; margin:0 auto;'>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "CONFIRM BY VISITING THE LINK BELOW:<br />";
                mailBody += "<span style='font-weight:bold; color:#0000ff;'><a href='[url]' style='font-weight: bold; font-size: 14px; color: #686464;'>[url]</a></span><br />";
                mailBody += "<br />";
                mailBody += "Click the link above to give us permission to open a annectoś Account, in your name.It’s fast and easy! If you cannot click the";
                mailBody += "full URL above, please copy and paste it into your web browser. By clicking the link you confirm your identiy.<br />";
                mailBody += "<br />";
                mailBody += "If you do not want access to online, simply ignore this message.<br />";
                mailBody += "<br />";
                mailBody += "When you click the link, your subscription will be confirmed.<br />";
                mailBody += "<br /><br />";
                mailBody += "<span style='color:#2c2b2b; font-weight:bold;'>Users:</span> <br />";
                mailBody += "Note, you need to activate the link or copy and paste the link directly into your browser window.<br />";
                mailBody += "<br /> Important:<br />";
                mailBody += "If the confirmation email above does not arrive in your inbox within a few minutes, check your “Spam” or “Bulk Folder”.Our message may have been filtered there.<br />";
                mailBody += "<br />";
                mailBody += "If you do find the email there, it’s important that you tag as “Not Spam” or “Mark as Safe.” This will tell your email provider not to filter our messages from annectoś.com.<br />";
                mailBody += "<br />";
                mailBody += "<span style='color:#2c2b2b; font-weight:bold;'>Whitelist our email address</span>";
                mailBody += "<br />";
                mailBody += "Be sure to whitelist “info@annectos.in” This will ensure that our messages arrive safely in your inbox and do not get filtered out.<br />";
                mailBody += "<br />";
                mailBody += "Regards,<br />";
                mailBody += "<span style='color:#0f0f0f;'>Annectos.com Team </span>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
                mailBody += "Copyright &copy; 1999-2001 Annectos.All rights reserved.<br />";
                mailBody += "<span style='color:#373737;'>http://www.annectos.in/</span>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</body>";
                mailBody += "</html>";


                string siteurl = ConfigurationSettings.AppSettings["SiteUrl"].ToString();


                DataAccess da = new DataAccess();
                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    strLogo = dtCompany.Rows[0]["logo"].ToString();
                    siteurl = strCompanyURL;
                }


                if (string.IsNullOrEmpty(guid))
                    siteurl += "login/login.html#/activation/" + userId;
                else
                    siteurl += "login/login.html#/activation/" + userId + "/" + guid;

                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                string email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                mailBody = mailBody.Replace("[email_logo]", strLogo);
                mailBody = mailBody.Replace("[url]", siteurl);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;
        }

        /// <summary>
        /// Submit Registration Information
        /// </summary>
        /// <param name="reg"></param>
        /// <param name="oc"></param>
        /// <returns></returns>
        /// 

        public string GetResetPasswordMailBody(string userId, string guid, string strCompany)
        {

            string mailBody = string.Empty;

            try
            {
                mailBody += "<html xmlns='http://www.w3.org/1999/xhtml'>";
                mailBody += "<head>";
                mailBody += "<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />";
                mailBody += "<style type='text/css'>";
                mailBody += "* {";
                mailBody += "padding:0px;";
                mailBody += "margin:0px;";
                mailBody += "}";
                mailBody += "</style>";
                mailBody += "</head>";
                mailBody += "<body>";
                mailBody += "<div style='width: 100%; position: relative; float: left'>";
                mailBody += "<div style='width:700px; line-height:20px; margin:0 auto; color:#494949; font-family:Arial, Helvetica, sans-serif; font-size: 14px;'>";
                mailBody += "<div style='float:left; height:70px; width:100%; background-color:#fdfdfd;'> ";
                mailBody += "<table width='100%' border='0' cellpadding='0' cellspacing='0'>";
                mailBody += "<tr>";
                mailBody += "<td background='[email_bg]' height='70'><a style='outline:none' target='_blank'  href='http://www.annectos.in'> <img style='border:none;' src='[email_logo]' alt='annectos' width='222' height='54' /></a></td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='float:left;  width:100%; background-color:#ffffff;'>";
                mailBody += "<div style='width:100%; font-size:24px; color:#1c66ca; height:75px; float:left; text-align:center;'>";
                mailBody += "<table width='100%' border='0' cellspacing='0' cellpadding='0'>";
                mailBody += "<tr>";
                mailBody += "<td height='75' align='center' valign='middle'>Reset Password</td>";
                mailBody += "</tr>";
                mailBody += "</table>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "<div style='width:620px; margin:0 auto;'>";
                mailBody += "<div style='width:100%; float:left;'>";
                mailBody += "CONFIRM BY VISITING THE LINK BELOW:<br />";
                mailBody += "<span style='font-weight:bold; color:#0000ff;'><a href='[url]' style='font-weight: bold; font-size: 14px; color: #686464;'>[url]</a></span><br />";
                mailBody += "<br />";
                mailBody += "Click the link above to reset the password of your Annectos Account, in your name.It’s fast and easy! If you cannot click the";
                mailBody += "full URL above, please copy and paste it into your web browser. By clicking the link you confirm your identiy.<br />";
                mailBody += "<br />";
                mailBody += "Be sure to whitelist “info@annectos.in” This will ensure that our messages arrive safely in your inbox and do not get filtered out.<br />";
                mailBody += "<br />";
                mailBody += "Regards,<br />";
                mailBody += "<span style='color:#0f0f0f;'>Annectos.com Team </span>";
                mailBody += "</div>";
                mailBody += "<div style='width:100%; float:left; text-align:center; font-size:12px; border-top:2px  solid #b41919; margin-top:50px; padding-top:25px; padding-bottom:25px; '>";
                mailBody += "Copyright &copy; 1999-2001 Annectos.All rights reserved.<br />";
                mailBody += "<span style='color:#373737;'>http://www.annectos.in/</span>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</div>";
                mailBody += "</body>";
                mailBody += "</html>";


                string siteurl = "";
                string email_logo = "";
                string strdisplayname = "";
                DataAccess da = new DataAccess();

                DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", userId));

                DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", strCompany));
                if (dtCompany != null && dtCompany.Rows.Count > 0)
                {
                    siteurl = dtCompany.Rows[0]["root_url_1"].ToString();
                    strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                    email_logo = dtCompany.Rows[0]["logo"].ToString();
                }




                if (string.IsNullOrEmpty(guid))
                    siteurl += "login/login.html#/reset_password/" + userId;
                else
                    siteurl += "login/login.html#/reset_password/" + userId + "/" + guid;

                string email_bg = ConfigurationSettings.AppSettings["email_bg"].ToString();
                //email_logo = ConfigurationSettings.AppSettings["email_logo"].ToString();

                mailBody = mailBody.Replace("[email_bg]", email_bg);
                mailBody = mailBody.Replace("[email_logo]", email_logo);
                mailBody = mailBody.Replace("[url]", siteurl);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return mailBody;

        }


        public string SubmitRegistrationInfo(mongo_user_info omo)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string strComp = omo.company;
            string result = "";
            try
            {
                guid = Guid.NewGuid().ToString();

                dal.DataAccess dal = new DataAccess();
                string q = "{email_id:'" + omo.email_id + "' , mobile_no:'" + omo.mobile_no + "', company:'" + omo.company + "'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("user", queryDoc);

                var match = false;

                foreach (var c in cursor)
                {
                    result = "This Email Is Already Registered";
                    match = true;
                    break;
                }

                if (match == false)
                {

                    omo._id = ObjectId.GenerateNewId().ToString();
                    omo.email_id = omo.email_id.ToString().ToLower();
                    omo.mobile_no = omo.mobile_no.ToString().ToLower();
                    omo.company = omo.company.ToString().ToLower();
                    omo.registration_date = System.DateTime.Now.ToLocalTime();
                    omo.shipping_data = new List<mongo_shipping_address>();
                    omo.status = 2;
                    BsonDocument bd_cat = omo.ToBsonDocument();
                    result = dal.mongo_write("user", bd_cat);
                    result = "Your Registration Has Been Successfully Completed";

                }




                //int i = da.ExecuteSP("user_registration_Insert", ref oc
                //                , da.Parameter("_first_name", reg.first_name)
                //                , da.Parameter("_last_name", reg.last_name)
                //                , da.Parameter("_gender", reg.gender)
                //                , da.Parameter("_email_id", reg.email_id)
                //                , da.Parameter("_mobile_no", reg.mobile_no)
                //                , da.Parameter("_office_no", "0796")
                //                , da.Parameter("_street", reg.street)
                //                , da.Parameter("_city", reg.city)
                //                , da.Parameter("_state", reg.state)
                //                , da.Parameter("_country", reg.country)
                //                , da.Parameter("_zipcode", reg.zipcode)
                //                , da.Parameter("_password", reg.password)
                //                , da.Parameter("_status", 1)
                //                , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                //                , da.Parameter("_company", reg.company)
                //                , da.Parameter("_guid", guid)
                //                , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                //                , da.Parameter("_action_flag", 1)
                //                , da.Parameter("_outmsg", String.Empty, true)
                //                );

                //if (i == 1)
                //{
                //    i = 0;

                //    if (oc[0].strParamValue.ToString() == "Your Registration Has Been Successfully Completed")
                //    {
                //        //Send Email for Account Activation
                //        helper_repository hr = new helper_repository();
                //        string mailfrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
                //        string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
                //        i = hr.SendMail(reg.email_id, "", "annectoś: Account Activation", GetAccountActivationMailBody(reg.email_id, guid, strComp));
                //    }
                //    /*
                //    oc[0].strParamValue Return Value May Be:                   
                //     * 1. Your Registration Has Been Successfully Completed.
                //     * 2. Your Account Activation Is Pending.
                //     * 3. This Email Is Already Registered!.
                //     * 4. Your Account Is Suspended/Terminated.
                //     * 5. Your Account Is Inactive                    
                //     */
                //    return oc[0].strParamValue.ToString();

                //}
                //else
                //    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                result = "Failed"; ;
            }

            return result;

        }

        public string UpdateRegistrationInfo(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";

            try
            {
                guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("update_user_information", ref oc
                                , da.Parameter("_first_name", reg.first_name)
                                , da.Parameter("_last_name", reg.last_name)
                                , da.Parameter("_gender", reg.gender)
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_mobile_no", reg.mobile_no)
                                , da.Parameter("_office_no", "0796")
                                , da.Parameter("_street", reg.street)
                                , da.Parameter("_city", reg.city)
                                , da.Parameter("_address", reg.address)
                                , da.Parameter("_state", reg.state)
                                , da.Parameter("_country", reg.country)
                                , da.Parameter("_zipcode", reg.zipcode)
                                , da.Parameter("_password", reg.password)
                                , da.Parameter("_status", 1)
                                , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_company", reg.company)
                                , da.Parameter("_guid", guid)
                                , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                                , da.Parameter("_action_flag", 1)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {

                    return oc[0].strParamValue.ToString();

                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }

        public string ResendActCode(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            try
            {
                guid = Guid.NewGuid().ToString();
                int i = da.ExecuteSP("user_registration_Insert", ref oc
                                , da.Parameter("_first_name", reg.first_name)
                                , da.Parameter("_last_name", reg.last_name)
                                , da.Parameter("_gender", reg.gender)
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_mobile_no", reg.mobile_no)
                                , da.Parameter("_office_no", "0796")
                                , da.Parameter("_street", reg.street)
                                , da.Parameter("_city", reg.city)
                                , da.Parameter("_state", reg.state)
                                , da.Parameter("_country", reg.country)
                                , da.Parameter("_zipcode", reg.zipcode)
                                , da.Parameter("_password", reg.password)
                                , da.Parameter("_status", 1)
                                , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_guid", guid)
                                , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                                , da.Parameter("_action_flag", 2)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {
                    i = 0;
                    //Send Email for Account Activation
                    helper_repository hr = new helper_repository();
                    string mailfrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
                    string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
                    i = hr.SendMail(reg.email_id, "", "annectoś: Account Activation", GetAccountActivationMailBody(reg.email_id, guid, ""));

                    /*
                    oc[0].strParamValue Return Value May Be:                   
                     * 1. Your Registration Has Been Successfully Completed.
                     * 2. Your Account Activation Is Pending.
                     * 3. This Email Is Already Registered!.
                     * 4. Your Account Is Suspended/Terminated.
                     * 5. Your Account Is Inactive                    
                     */
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }


        }



        private string CreateRandomPassword(int PassLength)
        {
            try
            {
                string _allowedChars = "0123456789abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ";
                Random randNum = new Random();
                char[] chars = new char[PassLength];
                int allowedCharCount = _allowedChars.Length;
                for (int i = 0; i < PassLength; i++)
                {
                    chars[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
                }
                string strNewRandomPass = new string(chars);

                return strNewRandomPass;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
        }


        public string CheckEmailToReset(registration reg)
        {
            string CheckResult = "";

            CheckResult = EmailToReset(reg);

            if (CheckResult == "Valid Email")
            {
                return ResetPassword(reg);
            }
            else
            {
                return CheckResult;
            }

        }

        public string EmailToReset(registration reg)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            try
            {

                //string strNewPassword = CreateRandomPassword(10);

                guid = Guid.NewGuid().ToString();

                int i = da.ExecuteSP("user_password_reset", ref oc
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_newpassword", guid)
                                , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_action_flag", 1)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }


        public string ResetPassword(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            try
            {

                //string strNewPassword = CreateRandomPassword(10);

                guid = Guid.NewGuid().ToString();

                int i = da.ExecuteSP("user_password_reset", ref oc
                                , da.Parameter("_email_id", reg.email_id)
                                , da.Parameter("_newpassword", guid)
                                , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_action_flag", 2)
                                , da.Parameter("_outmsg", String.Empty, true)
                                );

                if (i == 1)
                {
                    i = 0;
                    //Send Email for Account Activation
                    helper_repository hr = new helper_repository();
                    string mailfrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
                    string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
                    i = hr.SendMail(reg.email_id, "", "Annectos: Password Reset ", GetResetPasswordMailBody(reg.email_id, guid, reg.company));

                    /*
                    oc[0].strParamValue Return Value May Be:                   
                     * 1. Your Registration Has Been Successfully Completed.
                     * 2. Your Account Activation Is Pending.
                     * 3. This Email Is Already Registered!.
                     * 4. Your Account Is Suspended/Terminated.
                     * 5. Your Account Is Inactive                    
                     */
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }


        public string ActiveRegistration(registration_active reg_ac)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_registration_activate", ref oc
                                , da.Parameter("_email_id", reg_ac.email_id)
                                , da.Parameter("_guid", reg_ac.guid)
                                , da.Parameter("_activation_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_outmsg", String.Empty, true)
                                );


                if (i == 1)
                {
                    Msg = oc[0].strParamValue;
                }
                else
                    Msg = "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return Msg;
        }


        public string checkEmailGuid(registration_active reg_ac)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_pwd_reset_check", ref oc
                                , da.Parameter("_email_id", reg_ac.email_id)
                                , da.Parameter("_guid", reg_ac.guid)
                                , da.Parameter("_activation_date", System.DateTime.Now.ToLocalTime())
                                , da.Parameter("_outmsg", String.Empty, true)
                                );



                Msg = oc[0].strParamValue;

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return Msg;
        }

        // public string Feedback(Feedback feed)
        //{
        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();

        //    try
        //    {

        //        int i = da.ExecuteSP("Save_Feedback", ref oc
        //            , da.Parameter("first_name", feed.first_name)
        //            , da.Parameter("emailid", feed.emailid)
        //            , da.Parameter("last_name", feed.last_name)
        //            , da.Parameter("Comments", feed.Comments)
        //            , da.Parameter("Attachment", feed.Attachment)
        //            , da.Parameter("Choices", feed.Choices)
        //            , da.Parameter("_outmsg", String.Empty, true)
        //            );
        //        if (i > 0)
        //        {
        //            return "Feedback Saved Successfully";
        //        }
        //    }
        //    catch (Exception ex)
        //    { return "failed"; }
        //    return "";
        //}
        public string SaveNewPassword(registration reg)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_password_reset", ref oc
                               , da.Parameter("_email_id", reg.email_id)
                               , da.Parameter("_newpassword", reg.password)
                               , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                               , da.Parameter("_action_flag", 9)
                               , da.Parameter("_outmsg", String.Empty, true)
                               );


                Msg = oc[0].strParamValue.ToString();

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

            return Msg;
        }

        //public registration UserLogin(user_login user_log)
        //{
        //DataAccess da = new DataAccess();
        //List<OutCollection> oc = new List<OutCollection>();

        //string Msg = "";
        //string token_id = "";
        //login_msg return_msg = new login_msg();

        //try
        //{
        //    token_id = Guid.NewGuid().ToString();
        //    int i = da.ExecuteSP("user_login", ref oc
        //                    , da.Parameter("_email_id", user_log.email_id)
        //                    , da.Parameter("_password", user_log.password)
        //                    , da.Parameter("_login_date", System.DateTime.Now.ToLocalTime())
        //                    , da.Parameter("_token_id", token_id)
        //                    , da.Parameter("_status", 1)
        //                    , da.Parameter("_outmsg", String.Empty, true)
        //                    );


        //    if (i == 1)
        //    {
        //        Msg = token_id;
        //    }
        //    else
        //    {
        //        Msg = oc[0].strParamValue;
        //    }
        //    return_msg.type = "success";
        //    return_msg.value = Msg;

        //}
        //catch (Exception ex)
        //{
        //    return_msg.type = "system_error";
        //    return_msg.value = ex.Message;
        //}

        ////return Msg;
        //return return_msg;
        //}
        //public List<registration> UserLogin(user_login user_log)
        //{

        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();

        //    string Msg = "";
        //    string token_id = "";
        //    login_msg return_msg = new login_msg();
        //    List<registration> SL = new List<registration>();
        //    DataTable dtResult = new DataTable();

        //    try
        //    {
        //        string[] strout = new string[0];

        //        token_id = Guid.NewGuid().ToString();

        //        dtResult = da.ExecuteDataTable("user_login"
        //                        , da.Parameter("_email_id", user_log.email_id)
        //                        , da.Parameter("_password", user_log.password)
        //                        , da.Parameter("_login_date", System.DateTime.Now.ToLocalTime())
        //                        , da.Parameter("_token_id", token_id)
        //                        , da.Parameter("_status", 1)
        //                        , da.Parameter("_company", user_log.company)
        //                        , da.Parameter("_outmsg", "")
        //                        );




        //        if (dtResult != null && dtResult.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dtResult.Rows.Count; i++)
        //            {
        //                registration oreg = new registration();
        //                if (dtResult.Rows[i]["message"].ToString() != "Invalid Email Or Password")
        //                {
        //                    oreg.first_name = dtResult.Rows[i]["first_name"].ToString();
        //                    oreg.last_name = dtResult.Rows[i]["last_name"].ToString();
        //                    oreg.mobile_no = dtResult.Rows[i]["mobile_no"].ToString();
        //                    oreg.email_id = dtResult.Rows[i]["email_id"].ToString();
        //                    oreg.status = Convert.ToInt32(dtResult.Rows[i]["status"].ToString());
        //                    oreg.total_point = dtResult.Rows[i]["total_point"].ToString();
        //                    oreg.max_point_range = convertToDecimal(dtResult.Rows[i]["max_point_range"].ToString());
        //                    oreg.min_point_range = convertToDecimal(dtResult.Rows[i]["min_point_range"].ToString());
        //                    oreg.company = dtResult.Rows[i]["company"].ToString();
        //                    oreg.token_id = token_id;
        //                }
        //                oreg.message = dtResult.Rows[i]["message"].ToString();
        //                SL.Add(oreg);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }
        //    return SL;
        //}



        public List<mongo_user> UserLogin(user_login user_log)
        {

            string Msg = "";
            string token_id = "";

            List<mongo_user> olmu = new List<mongo_user>();
            payment_repository opr = new payment_repository();
            category_repository ocr = new category_repository();

            var company_detail = ocr.get_company_details(convertToString(user_log.company));

            try
            {
                string[] strout = new string[0];
                token_id = Guid.NewGuid().ToString();
                dal.DataAccess dal = new DataAccess();
                string q = "{email_id:'" + convertToString(user_log.email_id).ToLower() + "', password :'" + convertToString(user_log.password) + "' ,company : '" + convertToString(user_log.company) + "' }";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "first_name", "last_name", "email_id", "mobile_no", "street", "city", "state", "country", "zipcode", "company", "location", "address", "email_address", "qualification", "branch_name", "region" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("user", queryDoc, include_fields, exclude_fields);

                foreach (var c in cursor)
                {
                    mongo_user omu = new mongo_user();
                    omu.first_name = check_field(c.ToBsonDocument(), "first_name") ? convertToString(c.ToBsonDocument()["first_name"]) : null;
                    omu.last_name = check_field(c.ToBsonDocument(), "last_name") ? convertToString(c.ToBsonDocument()["last_name"]) : null;
                    omu.email_id = check_field(c.ToBsonDocument(), "email_id") ? convertToString(c.ToBsonDocument()["email_id"]) : null;
                    omu.mobile_no = check_field(c.ToBsonDocument(), "mobile_no") ? convertToString(c.ToBsonDocument()["mobile_no"]) : null;
                    omu.street = check_field(c.ToBsonDocument(), "street") ? convertToString(c.ToBsonDocument()["street"]) : null;
                    omu.city = check_field(c.ToBsonDocument(), "city") ? convertToString(c.ToBsonDocument()["city"]) : null;
                    omu.state = check_field(c.ToBsonDocument(), "state") ? convertToString(c.ToBsonDocument()["state"]) : null;
                    omu.country = check_field(c.ToBsonDocument(), "country") ? convertToString(c.ToBsonDocument()["country"]) : null;
                    omu.zipcode = check_field(c.ToBsonDocument(), "zipcode") ? convertToString(c.ToBsonDocument()["zipcode"]) : null;
                    omu.company = check_field(c.ToBsonDocument(), "company") ? convertToString(c.ToBsonDocument()["company"]) : null;
                    omu.location = check_field(c.ToBsonDocument(), "location") ? convertToString(c.ToBsonDocument()["location"]) : null;
                    omu.address = check_field(c.ToBsonDocument(), "address") ? convertToString(c.ToBsonDocument()["address"]) : null;
                    omu.email_address = check_field(c.ToBsonDocument(), "email_address") ? convertToString(c.ToBsonDocument()["email_address"]) : null;
                    omu.qualification = check_field(c.ToBsonDocument(), "qualification") ? convertToString(c.ToBsonDocument()["qualification"]) : null;
                    omu.branch_name = check_field(c.ToBsonDocument(), "branch_name") ? convertToString(c.ToBsonDocument()["branch_name"]) : null;
                    omu.region = check_field(c.ToBsonDocument(), "region") ? convertToString(c.ToBsonDocument()["region"]) : null;
                    omu.total_point = opr.GetUserPointBal(convertToString(user_log.email_id).ToLower(), convertToString(user_log.company));

                    if (company_detail != null && company_detail.Count != 0)
                    {
                        omu.max_point_range = convertToDouble(company_detail[0].point_range_max);
                        omu.min_point_range = convertToDouble(company_detail[0].point_range_min);
                    }
                    else
                    {
                        omu.max_point_range = 0;
                        omu.min_point_range = 0;
                    }
                    omu.token_id = token_id;
                    omu.message = "Log In Successfully";
                    olmu.Add(omu);
                }


                if (olmu == null || olmu.Count == 0)
                {
                    mongo_user omu1 = new mongo_user();
                    omu1.message = "Invalid Email Or Password";
                    olmu.Add(omu1);
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return olmu;
        }

        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        public Boolean RegistrationCheck(registration reg)
        {
            List<COMPANY> cmplist = new List<COMPANY>();
            cmplist = get_comp_details();

            if (cmplist.Count() == 0)
            {
                return false;
            }
            else
            {
                int match = 0;
                for (var i = 0; i < cmplist.Count(); i++)
                {
                    var inputemail = reg.email_id.ToString();
                    var charPos = inputemail.IndexOf('@') + 1;
                    var checkdomain = inputemail.Substring(charPos, (inputemail.Length - charPos));

                    if (cmplist[i].emailid.Contains(checkdomain))
                    {
                        match++;
                        break;
                    }
                }
                if (match == 0)
                {
                    return false;
                }
                else
                {
                    return true;

                }

            }
        }
        public string postFeedback(Feedback feed)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {

                int i = da.ExecuteSP("user_feedback_insert", ref oc
                    , da.Parameter("_feedback_id", feed.feedback_id)
                    , da.Parameter("_first_name", feed.first_name)
                    , da.Parameter("_last_name", feed.last_name)
                    , da.Parameter("_email_id", feed.email_id)
                    , da.Parameter("_feedback_choise", feed.feedback_choise)
                    , da.Parameter("_feedback_comment", feed.feedback_comment)
                    , da.Parameter("_company", feed.company)
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_outmsg", String.Empty, true)
                    );
                if (i > 0)
                {
                    Msg = oc[0].strParamValue.ToString();
                }
                else
                {
                    Msg = "Record Not Inserted";

                }
            }
            catch (Exception ex)
            {
                Msg = "failed";
            }
            return Msg;
        }


        public string postContactUs(Contact cnt)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string Msg = "";

            try
            {
                int i = da.ExecuteSP("contact_us_insert", ref oc
                    , da.Parameter("_contact_us_id", cnt.contact_us_id)
                    , da.Parameter("_name", cnt.name)
                    , da.Parameter("_email_id", cnt.email_id)
                    , da.Parameter("_telephone", cnt.telephone)
                    , da.Parameter("_comments", cnt.comments)
                    , da.Parameter("_company", cnt.company)
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_outmsg", String.Empty, true)
                    );
                if (i > 0)
                {
                    Msg = oc[0].strParamValue.ToString();
                }
                else
                {
                    Msg = "Record Not Inserted";

                }
            }
            catch (Exception ex)
            {
                Msg = "failed";
            }
            return Msg;
        }

        //sends only transactional sms; Diwakar 6/2/2014
        public string SendTRNSsms(List<String> strEmails)
        {
            DataAccess da = new DataAccess();

            string strdisplayname = "";
            string strCompanyURL = "";
            string strMobileNo = "";
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));
                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = convertToString(dtCompany.Rows[0]["root_url_2"]);
                            // strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }

                        helper_repository hp = new helper_repository();

                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();

                        string sms_message = "";

                        if (dt.Rows[0]["company"].ToString() == "xolo")
                        {
                            sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG3"].ToString();
                        }
                        else
                        {
                            sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG1"].ToString();
                        }
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSTRNID"].ToString();

                        sms_message = sms_message.Replace("##COMPANY##", strdisplayname);
                        sms_message = sms_message.Replace("##LOGIN##", strEmails[i].ToString());
                        sms_message = sms_message.Replace("##PWD##", dt.Rows[0]["password"].ToString());
                        sms_message = sms_message.Replace("##POINTS##", dt.Rows[0]["points"].ToString());
                        sms_message = sms_message.Replace("##COMP_URL##", strCompanyURL);


                        string ErroMsg = "";
                        int MobNoLength = 0;

                        long number1 = 0;

                        if (strMobileNo != null)
                        {
                            bool canConvert = long.TryParse(strMobileNo, out number1);
                            if (canConvert == false)
                            {
                                ErroMsg = "Mobile Number Is Not In Correct Format";
                                ExternalLogger.LogInfo(ErroMsg, this, "#");
                                return "Failure";
                            }

                            MobNoLength = strMobileNo.Length;

                            if (MobNoLength != 10)
                            {
                                ErroMsg = "Mobile Number Should Contain 10 Digits";
                                ExternalLogger.LogInfo(ErroMsg, this, "#");
                                return "Failure";
                            }
                            else
                            {
                                strMobileNo = "91" + strMobileNo;

                            }
                        }


                        if (!string.IsNullOrEmpty(strMobileNo))
                        {
                            string resp = hp.SendSMS(sms_user_id, sms_pwd, strMobileNo, sms_ssid, sms_message, "TRNS");
                        }
                        //strMobileNo
                    }
                }
            }

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public string getUserEMailAddress(string Email_Id)
        {
            DataAccess da = new DataAccess();
            string EmailAddress = "";

            if (Email_Id.Contains("@"))
            {
                EmailAddress = Email_Id;
            }
            else
            {
                EmailAddress = ConfigurationSettings.AppSettings["MailReplyToLava"].ToString();


                DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", Email_Id));
                if (dt != null && dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["EmailAddress"] != null)
                    {
                        if (dt.Rows[0]["EmailAddress"].ToString().Contains("@"))
                        {
                            EmailAddress = dt.Rows[0]["EmailAddress"].ToString();
                        }

                    }
                }

            }
            return EmailAddress.Trim();
        }

        public string getUserMobileNo(string Email_Id)
        {
            DataAccess da = new DataAccess();
            string strMobileNo = "";
            DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", Email_Id));
            if (dt != null && dt.Rows.Count > 0)
            {
                strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
            }
            return strMobileNo;
        }

        public List<COMPANY> get_comp_details()
        {

            DataAccess da = new DataAccess();
            List<COMPANY> lcomp = new List<COMPANY>();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("company_details"
                                 , da.Parameter("_comp_id", 0)
                                 , da.Parameter("_entry_mode", "A")
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        COMPANY comp = new COMPANY();
                        comp.id = Convert.ToInt32(dtResult.Rows[i]["id"].ToString());
                        comp.name = dtResult.Rows[i]["name"].ToString();
                        comp.emailid = dtResult.Rows[i]["emailid"].ToString();
                        lcomp.Add(comp);
                    }
                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return lcomp;
        }

        /// <summary>
        /// This will send the User Credentials by email and update the status flag for login
        /// </summary>
        /// <param name="strEmails"></param>
        /// <returns></returns>
        public string SendUserCredentials(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            string strSubject = "Welcome to annectoś rewards portal";
            string strBody = "";
            string strCompanyURL = "";
            string strdisplayname = "";
            string strStoreName = "";
            string strLogo = "";
            string strMobileNo = "";
            store_message osm = new store_message();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));
                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                            strStoreName = dtCompany.Rows[0]["name"].ToString();
                            strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }
                        /*
                        strBody="Hello " + dt.Rows[0]["first_name"].ToString() + "," + dt.Rows[0]["last_name"].ToString();
                        strBody += "<br/>";
                        strBody += "<br/>";
                        strBody +="Welcome to Annectos' rewards portal. Below is your sign in credential and information:";
                        strBody += "<br/>";
                        strBody += "<br/>";
                        strBody +="User Id  - " + strEmails[i];
                        strBody += "<br/>";
                        strBody +="Password - " +  dt.Rows[0]["password"].ToString();
                        strBody += "<br/>";
                        strBody +="Your point balance as per our record " + dt.Rows[0]["points"].ToString();
                        strBody += "<br/>";
                        strBody += "If you feel any discrepancy - please contact our support or your HR Representative.";
                        strBody += "<br/>";
                        */
                        /*************************************/
                        strSubject = "Welcome to " + strdisplayname;// +" Rewards platform"; Changed by Rahul on Dec 1
                        strBody = "<html>";
                        strBody += "<head>";
                        strBody += "</head>";
                        strBody += "<body style='padding: 0px; margin: 0px; background: #fff; text-align: center; padding: 10px; font-family: 'Trebuchet MS', Helvetica, sans-serif; font-size: 12px'>";
                        strBody += "<table cellpadding='0' cellspacing='0' width='580' align='center' style='border-left: 1px solid #dbdbdb; border-right: 1px solid #dbdbdb; border-bottom: 1px solid #dbdbdb'>";
                        strBody += "    <tr>";
                        strBody += "        <td style='background: #f5f5f5'>";
                        strBody += "            <div style=' -webkit-box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4); -moz-box-shadow:    0px 3px 6px rgba(50, 50, 50, 0.4);";
                        strBody += "box-shadow: 0px 3px 6px rgba(50, 50, 50, 0.4);'>";
                        strBody += "<div style='padding: 5px;'>";
                        strBody += "	            <table cellpadding='0' cellspacing='0' width='100%' >";
                        strBody += "		            <tr>";
                        strBody += "			            <td style='width: 76px; padding-right: 10px;' ><img src='" + strLogo + "' /></td>";
                        strBody += "			            <td style='width: 48px; padding-left: 10px; border-left:1px solid #999999' ><img src='img/emails/eselect_logo.jpg'</td>";
                        strBody += "			            <td style='width: 300px;;'></td>";
                        strBody += "			            <td style='width: 35px;' ><a href='#'></a></td>";
                        strBody += "			            <td style='width: 35px;' ><a href='#'></a></td>";
                        strBody += "			            <td style='width: 35px;'><a href='#'></a></td>";
                        strBody += "		            </tr>";
                        strBody += "		            </table>";
                        strBody += "		            </div>";
                        strBody += "		            <div style='background: #f5c405; line-height:15px;  font-style: italic; font-size:12px; padding: 5px;'>";
                        strBody += "			            <table cellpadding='0' cellspacing='0'  >";
                        strBody += "		            </table>";
                        strBody += "		            </div>";
                        strBody += "            </div>";
                        strBody += "        </td>";
                        strBody += "    </tr>";
                        strBody += "    <tr>";
                        strBody += "        <td style='background: #fff; padding: 10px;font-family: 'Trebuchet MS'; font-size: 13px'>";
                        strBody += "        Dear " + dt.Rows[0]["first_name"].ToString() + " " + dt.Rows[0]["last_name"].ToString() + ", <br /><br />";
                        strBody += "";
                        // changed by rahul on 1 Dec 2013
                        strBody += "        Welcome to " + strdisplayname + ". Please Login to the portal using the following link - " + strCompanyURL + ". Below is your sign in credential and information: <br /><br />";
                        strBody += "        <div style='font-family: 'Trebuchet MS'; font-size: 12px; color: #333'>";
                        strBody += "        User Id - " + strEmails[i] + "<br/>";
                        strBody += "        Password - " + dt.Rows[0]["password"].ToString() + "<br/>";
                        strBody += "        <br/>";
                        strBody += "        (please do not share your login details or password with anyone else. )<br/>";
                        strBody += "        <br/>";
                        strBody += "        The points balance in your account is " + dt.Rows[0]["points"].ToString() + "<br/>";
                        strBody += "        <br/>";
                        strBody += "        For further assistance call us on +91 9686202046 / 9972334590 or mail us at customerfirst@annectos.in <br/>";
                        strBody += "        <br/>";
                        strBody += "        Assuring you of our best services always. <br/>";
                        strBody += "        <br/>";
                        strBody += "        ** Please use Google Chrome for exploring through the portal. <br/>";
                        strBody += "        <br/>";
                        strBody += "        Sincerely<br/>";
                        strBody += "        <br/>";
                        strBody += "        Team Annectoś";
                        strBody += "        </div>";
                        strBody += "        </td>";
                        strBody += "        </tr>";
                        strBody += "        </table>";
                        strBody += "        </td>";
                        strBody += "        </tr>";
                        strBody += "<tr>";
                        strBody += "        <td style='background: #fff; padding: 10px;font-family: 'Trebuchet MS'; font-size: 13px'>";
                        strBody += "        </td>";
                        strBody += "</tr>";
                        strBody += "</table>";
                        strBody += "</body>";
                        strBody += "</html>";

                        strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
                        /*************************************/


                        helper_repository hp = new helper_repository();
                        string strbcc_email = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSSSID"].ToString();
                        string sms_message = ConfigurationSettings.AppSettings["SMS_USER_ACC_CREATION_MSG"].ToString();


                        //This Code is applicable only for airtel

                        if (strStoreName == "airtel")
                        {
                            strBody = "";
                            strSubject = "";

                            osm = get_store_message(strStoreName, "welcome");
                            if (osm != null)
                            {
                                strBody = osm.Msg_content;
                                strSubject = osm.Subject;
                            }

                            strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                            strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                            strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                            strBody = strBody.Replace("#COMPANY_LOGO#", strLogo);
                            strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                            strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                            strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                            strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());
                        }
                        //   This is for XoloMan

                        else if (strStoreName == "xolo")
                        {
                            strBody = "";
                            strSubject = "";

                            osm = get_store_message(strStoreName, "welcome");
                            if (osm != null)
                            {
                                strBody = osm.Msg_content;
                                strSubject = osm.Subject;
                            }

                            strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                            strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                            strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                            strBody = strBody.Replace("#COMPANY_LOGO#", strLogo);
                            strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                            strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                            strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                            strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());


                        }
                        else if (strStoreName == "intel")
                        {
                            strSubject = "Intel employee offers portal";
                        }

                        if (hp.SendMail(getUserEMailAddress(strEmails[i].ToString()), "", strbcc_email, strSubject, strBody) == 1)
                        {
                            int j = da.ExecuteSP("auto_activate_users", da.Parameter("_email_id", strEmails[i]));

                            if (!string.IsNullOrEmpty(strMobileNo))
                            {
                                string MobNo = strMobileNo;
                                if (CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                                {
                                    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message);
                                }
                            }
                            //strMobileNo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public string CheckMobileNo(string strMobileNo, ref string MobNo)
        {

            string ErroMsg = "";
            int MobNoLength = 0;

            long number1 = 0;

            if (strMobileNo != null)
            {
                bool canConvert = long.TryParse(strMobileNo, out number1);
                if (canConvert == false)
                {
                    ErroMsg = "Mobile Number Is Not In Correct Format";
                    ExternalLogger.LogInfo(ErroMsg, this, "#");
                    return "Failure";
                }

                MobNoLength = strMobileNo.Length;

                if (MobNoLength != 10)
                {
                    ErroMsg = "Mobile Number Should Contain 10 Digits";
                    ExternalLogger.LogInfo(ErroMsg, this, "#");
                    return "Failure";
                }
                else
                {
                    strMobileNo = "91" + strMobileNo;
                    MobNo = strMobileNo;
                    return strMobileNo;
                }
            }
            else
            {
                return "Failure";
            }

        }

        public string SendPromoWithCredentials(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            string strSubject = "";
            string strBody = "";
            string strCompanyURL = "";
            string strdisplayname = "";
            string strStoreName = "";
            string strLogo = "";
            string strMobileNo = "";
            store_message osm = new store_message();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));

                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                            strStoreName = dtCompany.Rows[0]["name"].ToString();
                            strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }

                        osm = get_store_message(strStoreName, "promo");
                        if (osm != null)
                        {
                            strBody = osm.Msg_content;
                            strSubject = osm.Subject;
                        }

                        strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                        strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                        strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                        strBody = strBody.Replace("#COMPANY_DISPLAY_NAME#", strdisplayname);
                        strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                        strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                        strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                        strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());

                        /*************************************/


                        helper_repository hp = new helper_repository();
                        string strbcc_email = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSSSID"].ToString();
                        string sms_message = ConfigurationSettings.AppSettings["SMS_USER_ACC_CREATION_MSG"].ToString();
                        if (hp.SendMail(getUserEMailAddress(strEmails[i].ToString()), "", strbcc_email, strSubject, strBody) == 1)
                        {
                            int j = da.ExecuteSP("auto_activate_users", da.Parameter("_email_id", strEmails[i]));

                            if (!string.IsNullOrEmpty(strMobileNo))
                            {
                                string resp = hp.SendSMS(sms_user_id, sms_pwd, strMobileNo, sms_ssid, sms_message);
                            }
                            //strMobileNo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public string SendPromo(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            string strSubject = "";
            string strBody = "";
            string strCompanyURL = "";
            string strStoreName = "";
            string strdisplayname = "";
            string strLogo = "";
            string strMobileNo = "";
            store_message osm = new store_message();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {
                    DataTable dt = da.ExecuteDataTable("get_new_user_info", da.Parameter("_email_id", strEmails[i]));
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        strMobileNo = dt.Rows[0]["Mobile_No"].ToString();
                        DataTable dtCompany = da.ExecuteDataTable("get_company_info", da.Parameter("cname", dt.Rows[0]["company"].ToString()));

                        if (dtCompany != null && dtCompany.Rows.Count > 0)
                        {
                            strCompanyURL = dtCompany.Rows[0]["root_url_1"].ToString();
                            strdisplayname = dtCompany.Rows[0]["displayname"].ToString();
                            strStoreName = dtCompany.Rows[0]["name"].ToString();
                            strLogo = dtCompany.Rows[0]["logo"].ToString();
                        }

                        osm = get_store_message(strStoreName, "promo");
                        if (osm != null)
                        {
                            strBody = osm.Msg_content;
                            strSubject = osm.Subject;
                        }

                        strBody = strBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");

                        strBody = strBody.Replace("#USER_FIRST_NAME#", dt.Rows[0]["first_name"].ToString());
                        strBody = strBody.Replace("#USER_LAST_NAME#", dt.Rows[0]["last_name"].ToString());
                        strBody = strBody.Replace("#COMPANY_DISPLAY_NAME#", strdisplayname);
                        strBody = strBody.Replace("#COMPANY_URL#", strCompanyURL);
                        strBody = strBody.Replace("#USER_EMAIL#", strEmails[i].ToString());
                        strBody = strBody.Replace("#USER_PASSWORD#", dt.Rows[0]["password"].ToString());
                        strBody = strBody.Replace("#PONTS_BALANCE#", dt.Rows[0]["points"].ToString());
                        /*************************************/


                        helper_repository hp = new helper_repository();
                        string strbcc_email = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                        string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                        string sms_ssid = ConfigurationSettings.AppSettings["SMSSSID"].ToString();
                        string sms_message = ConfigurationSettings.AppSettings["SMS_USER_ACC_CREATION_MSG"].ToString();
                        if (hp.SendMail(getUserEMailAddress(strEmails[i].ToString()), "", strbcc_email, strSubject, strBody) == 1)
                        {
                            int j = da.ExecuteSP("auto_activate_users", da.Parameter("_email_id", strEmails[i]));

                            if (!string.IsNullOrEmpty(strMobileNo))
                            {
                                string resp = hp.SendSMS(sms_user_id, sms_pwd, strMobileNo, sms_ssid, sms_message);
                            }
                            //strMobileNo
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        public store_message get_store_message(string Store_Id, string Msg_Type)
        {

            DataAccess da = new DataAccess();
            store_message osm = new store_message();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_store_message"
                                 , da.Parameter("_store_id", Store_Id)
                                 , da.Parameter("_msg_type", Msg_Type)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        osm.Msg_id = convertToLong(dtResult.Rows[i]["Msg_id"]);
                        osm.Store_id = convertToString(dtResult.Rows[i]["Store_id"]);
                        osm.Msg_content = convertToString(dtResult.Rows[i]["Msg_content"]);
                        osm.Msg_type = convertToString(dtResult.Rows[i]["Msg_type"]);
                        osm.Subject = convertToString(dtResult.Rows[i]["Subject"]);

                    }
                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return osm;
        }


        public string RegPassword(List<String> strEmails)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                for (int i = 0; i < strEmails.Count; i++)
                {

                    string Pwd = "";

                    Pwd = GetUniquePassword();
                    int j = da.ExecuteSP("user_password_reset", ref oc
                                       , da.Parameter("_email_id", strEmails[i].ToString())
                                       , da.Parameter("_newpassword", Pwd)
                                       , da.Parameter("_reset_date", System.DateTime.Now.ToLocalTime())
                                       , da.Parameter("_action_flag", 9)
                                       , da.Parameter("_outmsg", String.Empty, true)
                                       );
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }

            return "Success";

        }

        private string GetUniquePassword()
        {
            int maxSize = 7;
            char[] chars = new char[62];
            string a;
            a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            chars = a.ToCharArray();
            RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
            int size = maxSize;
            byte[] data = new byte[size];
            crypto.GetNonZeroBytes(data);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < data.Length; i++)
            {
                sb.Append(data[i].ToString("X2"));
            }
            return sb.ToString();

        }

    }
}
