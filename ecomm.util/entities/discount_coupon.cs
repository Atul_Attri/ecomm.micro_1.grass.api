﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class discount_coupon
    {
        public string discount_code { get; set; }
        public string rule { get; set; }
        public string content_url { get; set; }
    }


    public class disc_rule
    {
        public string discount_type { get; set; }
        public string discount_value { get; set; }
        public string min_value { get; set; }
        public string max_discount { get; set; }
    }
}
