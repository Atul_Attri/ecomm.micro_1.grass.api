﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class gift
    {
        public string _id { get; set; }
        public double NoOfVou { get; set; }
        public string gift_voucher_code { get; set; }
        public string giver_email { get; set; }
        public string rcv_email { get; set; }
        public int amount { get; set; }
        public string company { get; set; }
        public string status { get; set; } //0=Booed, 1=payment_pending, 2 = payment_received, redeemed
        public string write_by { get; set; }
        public DateTime create_date { get; set; }
        public int validity { get; set; }
        public int active { get; set; }
        public string event_desc { get; set; }
    }

    public class gv_voucher
    {
        public string _id { get; set; }
        public double NoOfVou { get; set; }
        public string gift_voucher_code { get; set; }
        public string giver_email { get; set; }
        public string rcv_email { get; set; }
        public int amount { get; set; }
        public string company { get; set; }
        public string status { get; set; } //0=Booed, 1=payment_pending, 2 = payment_received, redeemed
        public string write_by { get; set; }
        public DateTime create_date { get; set; }
        public DateTime valid_date { get; set; }
        public int validity { get; set; }
        public int active { get; set; }
        public string event_desc { get; set; }
    }

    public class expo_vou_grid
    {
        public List<vou_grid_data> GridData { get; set; }

    }
    public class vou_grid_data
    {
        public string gift_voucher_code { get; set; }
        public string create_date { get; set; }
        public string giver_email { get; set; }
        public string rcv_email { get; set; }
        public string amount { get; set; }
        public string validity { get; set; }
    }
}