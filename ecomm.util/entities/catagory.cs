﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ecomm.util.entities
{
    public class catagory:cat
    {
        public List<url> image_urls { get; set; }
        public List<url> video_urls { get; set; }
        public List<url> ad_urls { get; set; }
        public string active { get; set; }
    }
}
