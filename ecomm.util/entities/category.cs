﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class category:cate
    {
        public List<imgurl> image_urls { get; set; }
        public List<url> video_urls { get; set; }
        public List<adurl> ad_urls { get; set; }
        public string number_items { get; set; }
    }
}
