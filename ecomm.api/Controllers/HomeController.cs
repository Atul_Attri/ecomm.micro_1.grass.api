﻿using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ecomm.api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }



        public ActionResult feedback(grass_micro_feedback ogmf)
        {
            ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
            string result = cr.micro_feedback(ogmf);
            return View();
        }

    }
}
