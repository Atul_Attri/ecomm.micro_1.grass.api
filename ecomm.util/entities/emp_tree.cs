﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class airtel_emp_tree
    {

        public string user_id { get; set; }
        public string full_name { get; set; }
        public string mobile_no { get; set; }
        public string dist_drp_no { get; set; }
        public string disti_name { get; set; }
        public string tm_name { get; set; }
        public string zsm_name { get; set; }
        public string zone { get; set; }
        public string district { get; set; }
        public string district_type { get; set; }
    }
}
