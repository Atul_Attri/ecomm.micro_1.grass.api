﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using ecomm.dal;
using ecomm.util;
using ecomm.util.entities;
using Newtonsoft.Json;
// to be removed
using MongoDB.Bson;
using MongoDB.Driver;
using System.Data;
using MongoDB.Driver.Builders;


namespace ecomm.model.repository
{
    public class store_repository
    {

        #region get methods


        public discount_coupon validate_discount_code(string discount_code, string store, string user)
        {

            DataAccess da = new DataAccess();

            discount_coupon coupon = new discount_coupon();
            DataTable dtResult = new DataTable();

            try
            {
                dtResult = da.ExecuteDataTable("get_discount_coupon"
                                , da.Parameter("_discount_code", discount_code)
                                , da.Parameter("_store", store)
                                , da.Parameter("_user_id", user)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    coupon.discount_code = discount_code;
                    coupon.rule = convertToString(dtResult.Rows[0]["rule"].ToString());
                    coupon.content_url = convertToString(dtResult.Rows[0]["content_url"].ToString());
                }

                return coupon;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        private MongoCursor get_company_cat_exclusion(string company)
        {
            dal.DataAccess dal = new DataAccess();
            //string q = "{'ParentId': { $ne:'0'}}";
            string q = "{'company':'" + company + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "cat", "brand" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("company_exclusion", queryDoc, include_fields, exclude_fields);
            return cursor;
        }

        public List<menu> get_menu(string company)
        {
            // get company exclusion
            string excl_cat = "";
            string excl_brand = "";
            if (company != "")
            {
                MongoCursor cursor_exclusion = get_company_cat_exclusion(company);
                if (cursor_exclusion.Count() > 0)
                {
                    List<string> exclude_cat = new List<string>();
                    excl_cat = "[";
                    foreach (var c in cursor_exclusion)
                    {
                        for (int i = 0; i < c.ToBsonDocument()["cat"].AsBsonArray.Count; i++)
                        {
                            excl_cat = excl_cat + "'" + c.ToBsonDocument()["cat"].AsBsonArray[i].ToString() + "',";
                        }
                        excl_cat = excl_cat + "]";
                    }
                    List<string> exclude_brand = new List<string>();
                    excl_brand = "[";
                    foreach (var c in cursor_exclusion)
                    {
                        if (check_field(c.ToBsonDocument(), "brand"))
                        {
                            for (int i = 0; i < c.ToBsonDocument()["brand"].AsBsonArray.Count; i++)
                            {
                                excl_brand = excl_brand + "'" + c.ToBsonDocument()["brand"].AsBsonArray[i].ToString() + "',";
                            }
                        }
                        excl_brand = excl_brand + "]";
                    }

                }
            }
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //string q = "{'ParentId': { $ne:'0'}}";
            string q = "";
            if (excl_cat != "")
            {
                q = "{'active': 1, 'id' : {$nin :" + excl_cat + "}}}";
            }
            else
            {
                q = "{'active': 1}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "Name", "id", "ParentId" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
            var menu = new List<menu>();
            List<menu_item> menu_items = new List<menu_item>();
            foreach (var c in cursor)
            {
                menu_item m = new menu_item();
                m = JsonConvert.DeserializeObject<menu_item>(c.ToBsonDocument().ToString());
                m.link = c.ToBsonDocument()["id"].ToString();
                m.ParentId = c.ToBsonDocument()["ParentId"].ToString();
                var tmp = c.ToBsonDocument()["id"].ToString().Split('.');
                if (tmp.Length == 3)
                {
                    if (tmp[2] != "0")
                    {
                        m.level = 3;
                    }
                    else
                    {
                        m.level = 2;
                    }
                }
                else
                {
                    m.level = 1;
                }
                menu_items.Add(m);
            }

            foreach (menu_item m in menu_items)
            {
                switch (m.level)
                {
                    case 1:
                        menu menu_head = new menu();
                        menu_head.name = m.name.ToLower();
                        menu_head.id = m.link;
                        List<menu_item> menu_l2 = new List<menu_item>();
                        menu_l2 = menu_items.FindAll(delegate(menu_item m_in) { return m_in.ParentId.ToString() == m.link.ToString(); });
                        menu_l2.Remove(m);
                        List<menu_item> menu_l3 = new List<menu_item>();
                        foreach (menu_item m2 in menu_l2)
                        {
                            var menu_l3_tmp = (List<menu_item>)menu_items.FindAll(delegate(menu_item m_in) { return m_in.ParentId.ToString() == m2.link.ToString(); });
                            menu_l3.AddRange(menu_l3_tmp);
                        }

                        //menu_l3.Remove(m);
                        List<menu_item> sub_menu_items = new List<menu_item>();
                        menu_l2.AddRange(menu_l3);
                        menu_head.menu_items = menu_l2;
                        menu.Add(menu_head);
                        break;
                }

            }
            //return mongo_query_singleton (q, "cart", include_fields, exclude_fields);
            return menu;
        }

        public int get_menu_expiry_status(string last_download_date)
        {
            int expired = 1;
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                //string q = "{'ParentId': { $ne:'0'}}";
                string q = "";
                string[] include_fields = new string[] { "expired" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("expiry", include_fields, exclude_fields);

                foreach (var c in cursor)
                {
                    string last_menu_update_date = c.ToBsonDocument()["expired"].ToString();
                    DateTime dt_last_download_date = Convert.ToDateTime(last_download_date);
                    DateTime dt_last_menu_update_date = Convert.ToDateTime(last_menu_update_date);
                    if ((dt_last_download_date - dt_last_menu_update_date).TotalDays > 0)
                    {
                        expired = 0;
                    }
                    else
                    {
                        expired = 1;
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return expired;
        }
        public List<menu_item> get_level1_menu()
        {
            List<menu_item> menu_items = new List<menu_item>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                //string q = "{'ParentId': { $ne:'0'}}";
                string q = "{'active': 1, 'ParentId':'0'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "Name", "id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
                var menu = new List<menu>();

                foreach (var c in cursor)
                {
                    menu_item m = new menu_item();
                    m = JsonConvert.DeserializeObject<menu_item>(c.ToBsonDocument().ToString());
                    m.link = c.ToBsonDocument()["id"].ToString();
                    m.ParentId = "0";
                    if (m.name != "Airtel Special" && m.name != "Exide Specials")
                    {
                        menu_items.Add(m);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return menu_items;
        }


        public string[] get_cat_list()
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            MongoCursor cursor = dal.execute_mongo_db("category");
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToJson());
            }
            return list.ToArray();
        }


        public List<cat> get_cat_list_short()
        {
            var catagories = new List<cat>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                MongoCursor cursor = dal.execute_mongo_db("cat");


                foreach (var c in cursor)
                {
                    cat cat = new cat();
                    cat.Id = c.ToBsonDocument()["_id"].ToString();
                    cat.cat_name = c.ToBsonDocument()["cat_name"].ToString();
                    cat.parent_cat_id = c.ToBsonDocument()["parent_cat_id"].ToString();
                    catagories.Add(cat);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return catagories;
        }
        public List<catagory> get_active_cat_list()
        {
            var catagories = new List<catagory>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{ active : '1'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("cat", queryDoc);


                foreach (var c in cursor)
                {
                    catagory cat = new catagory();
                    cat.Id = c.ToBsonDocument()["_id"].ToString();
                    cat.active = c.ToBsonDocument()["active"].ToString();
                    cat.cat_name = c.ToBsonDocument()["cat_name"].ToString();
                    cat.description = c.ToBsonDocument()["description"].ToString();
                    cat.parent_cat_id = c.ToBsonDocument()["parent_cat_id"].ToString();
                    cat.image_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["image_urls"].ToString());
                    cat.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    cat.ad_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["ad_urls"].ToString());
                    catagories.Add(cat);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return catagories;
        }
        public List<cat> get_active_cat_list_short()
        {
            // evoke mongodb connection method in dal
            var catagories = new List<cat>();

            try
            {
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{ active : '1'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("cat", queryDoc);


                foreach (var c in cursor)
                {
                    cat cat = new cat();
                    cat.Id = c.ToBsonDocument()["_id"].ToString();
                    cat.cat_name = c.ToBsonDocument()["cat_name"].ToString();
                    cat.parent_cat_id = c.ToBsonDocument()["parent_cat_id"].ToString();
                    catagories.Add(cat);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return catagories;
        }
        public string[] get_cat_details(string cat_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{ active : 1, id:'" + cat_id + "'}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "Name", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();
        }
        public string[] get_cat_list_filters(string cat_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{ active : 1, id:'" + cat_id + "'}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "Name", "filters" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();
        }
        public List<filter> get_newarrivals_items_filters()
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                //string q = "{ 'active' : 1, 'price.list':{$gt:" + premium_threshhold + "}}";

                int DateRange = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["DateRange"].ToString());

                var EndDate = "'" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                var StartDate = "'" + System.DateTime.UtcNow.AddDays(-DateRange).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
                //string q = "{ $and: [{'active' : 1},{'create_date':{$lte:" + EndDate + "}},{'create_date':{$gte:" + StartDate + "}}]}";
                string q = "{'active' : 1, 'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")}}";

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }


        }

        public List<filter> get_premium_items_filters(int premium_threshhold)
        {

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'price.list':{$gt:" + premium_threshhold + "}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters", "Name" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    //filter cat_filter = new filter();

                    //List<string> cat_val_list = new List<string>();;
                    foreach (var c in cursor)
                    {
                        // build cat list
                        //cat_filter.values.Add(c.ToBsonDocument()["Name"].ToString());
                        //cat_val_list.Add(c.ToBsonDocument()["Name"].ToString());
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    //cat_filter = new filter() { name = "cat", values = cat_val_list };
                    //list.Add(cat_filter);
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }


        }
        public List<filter> get_special_item_filters_parent(string cat_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'cat_id.cat_id':{$gt:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }
        public List<filter> get_special_item_filters_specific(string cat_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'cat_id.cat_id':'" + cat_id + "'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }
        public string[] get_prod_list_by_cat_short(string cat_id, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            List<String> list = new List<string>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "stock", "have_child" };
                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);


                foreach (var c in cursor)
                {

                    list.Add(c.ToBsonDocument().ToString());
                    //foreach(var d in c.ToBsonDocument()["cat_id"].AsBsonArray[0].ToBsonDocument()["id"].ToString()
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");

            }
            return list.ToArray();
        }

        public string[] get_prod_list_by_cat_short(string cat_id)
        {
            List<String> list = new List<string>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q;
                //if (cat_id.Last() == '0')
                if (cat_id.EndsWith(".0"))
                {
                    string tmp = cat_id.Substring(0, cat_id.Length - 2);
                    //q = "{'active':1, 'cat_id.cat_id':/^" + tmp + "./}}";
                    q = "{'active':1,'cat_id.cat_id':/^" + tmp + "./}}";
                }
                else
                {
                    //q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                    q = "{'active':1, cat_id:{cat_id:'" + cat_id + "'}}";
                }
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = get_cat_short_include_fields();
                // new string[] {    "id", "brand", "price", 
                //"cat_id", "sku", "point", 
                //"Name", "description", 
                //"image_urls", "feature", 
                //"have_child", "Express" };

                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);


                foreach (var c in cursor)
                {

                    list.Add(c.ToBsonDocument().ToString());
                    //foreach(var d in c.ToBsonDocument()["cat_id"].AsBsonArray[0].ToBsonDocument()["id"].ToString()
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");

            }
            return list.ToArray();

        }
        public string[] get_prod_list_by_cat_short(string cat_id, int min, int max)
        {
            List<String> list = new List<string>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q;
                //if (cat_id.Last() == '0')
                if (min > max) // wrong case
                {
                    // reset to default and ignore
                    min = 0;
                    max = 0;
                }
                if ((min > 0) || (max > 0))
                {

                    if (cat_id.EndsWith(".0"))
                    {
                        string tmp = cat_id.Substring(0, cat_id.Length - 2);
                        //q = "{'active':1, 'cat_id.cat_id':/^" + tmp + "./}}";
                        q = "{'active':1,'cat_id.cat_id':/^" + tmp + "./ , 'price.list':{ $lte: " + max + ", $gte: " + min + "  }}";
                    }
                    else
                    {
                        //q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                        q = "{'active':1, cat_id:{cat_id:'" + cat_id + "'}, 'price.list':{ $lte: " + max + ", $gte: " + min + " }}";
                    }
                }
                else
                {
                    if (cat_id.EndsWith(".0"))
                    {
                        string tmp = cat_id.Substring(0, cat_id.Length - 2);
                        //q = "{'active':1, 'cat_id.cat_id':/^" + tmp + "./}}";
                        q = "{'active':1,'cat_id.cat_id':/^" + tmp + "./}";
                    }
                    else
                    {
                        //q = "{cat_id:{cat_id:'" + cat_id + "'}}";
                        q = "{'active':1, cat_id:{cat_id:'" + cat_id + "'}}";
                    }
                }

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = get_cat_short_include_fields();

                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);


                foreach (var c in cursor)
                {
                    if (c.ToBsonDocument()["have_child"] > 0)
                    {
                        c.ToBsonDocument()["stock"] = -9999;// hardcoded -negatve value to show products with children. system is prone to a situation of we get to -9999 stock - very unlikely
                    }
                    else
                    {
                        c.ToBsonDocument()["stock"] = c.ToBsonDocument()["stock"].ToInt32();
                    }

                    if (c.ToBsonDocument()["stock"].ToInt32() > 0 || c.ToBsonDocument()["stock"] == -9999)
                    {
                        list.Add(c.ToBsonDocument().ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list.ToArray();

        }




        public string[] get_breadcrumb(string type, string value)
        {
            string[] new_list = new string[] { "" };
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string[] include_fields;
                string[] exclude_fields = new string[] { "_id" };

                string l1, l2, qs;
                string[] s;
                List<string> distinct_cat = new List<string>();
                if (type == "")
                {
                    type = "cat";
                }
                type = type.ToLower();
                switch (type)
                {
                    case "cat":
                        // get parent cats also
                        s = value.Split('.');
                        switch (s.Length)
                        {
                            case 3:
                                l1 = s[0] + ".0";
                                l2 = s[0] + "." + s[1] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(l2);
                                distinct_cat.Add(value);
                                break;

                            case 2:
                                l1 = s[0] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(value);
                                break;
                            //case 1:
                            default:
                                break;
                        }

                        //distinct_cat = new List<string>(distinct_cat.Distinct());
                        qs = "{'id':{$in:[";
                        foreach (var c in distinct_cat)
                        {
                            if (qs == "{'id':{$in:[")
                            {
                                qs = qs + "'" + c + "'";
                            }
                            else
                            {
                                qs = qs + ",'" + c + "'";
                            }
                        }
                        qs = qs + "]}}";
                        include_fields = new string[] { "id", "Name", "description" };
                        exclude_fields = new string[] { "_id" };
                        new_list = mongo_query(qs, "category", include_fields, exclude_fields);
                        break;

                    case "prod":

                        // TODO - get parent cat and brand
                        // TODO - feed parent cat thru below loop
                        // concat brand at the end of the list

                        // get parent cats also

                        s = value.Split('.');
                        switch (s.Length)
                        {
                            case 3:
                                l1 = s[0] + ".0";
                                l2 = s[0] + "." + s[1] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(l2);
                                distinct_cat.Add(value);
                                break;

                            case 2:
                                l1 = s[0] + ".0";
                                distinct_cat.Add(l1);
                                distinct_cat.Add(value);
                                break;
                            //case 1:
                            default:
                                break;
                        }

                        //distinct_cat = new List<string>(distinct_cat.Distinct());
                        qs = "{'id':{$in:[";
                        foreach (var c in distinct_cat)
                        {
                            if (qs == "{'id':{$in:[")
                            {
                                qs = qs + "'" + c + "'";
                            }
                            else
                            {
                                qs = qs + ",'" + c + "'";
                            }
                        }
                        qs = qs + "]}}";
                        include_fields = new string[] { "id", "Name", "description" };
                        exclude_fields = new string[] { "_id" };
                        new_list = mongo_query(qs, "category", include_fields, exclude_fields);
                        break;


                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

            return new_list;

        }
        public string[] get_categories_by_brand(string brand)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'brand':'" + brand + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };

                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);

                List<String> list = new List<string>();
                foreach (var c in cursor)
                {

                    //list.Add(c.ToBsonDocument().ToString());
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list.Add(d["cat_id"].ToString());
                    }
                }
                List<string> distinct_cat = new List<string>(list.Distinct());//.ToArray(); // get disctinct

                // get parent cats also
                string l1, l2;
                string[] s;
                //var cat1 = distinct_cat;
                foreach (var c1 in distinct_cat.ToList())
                {
                    s = c1.Split('.');
                    switch (s.Length)
                    {
                        case 3:
                            l1 = s[0] + ".0";
                            l2 = s[0] + "." + s[1];
                            distinct_cat.Add(l1);
                            distinct_cat.Add(l2);
                            break;

                        case 2:
                            l1 = s[0] + ".0";
                            distinct_cat.Add(l1);
                            break;
                        //case 1:
                        default:
                            break;
                    }
                }
                distinct_cat = new List<string>(distinct_cat.Distinct());
                //{"id":{$in: ["1.1", "1.1.1"]}}
                string qs = "{'id':{$in:[";
                foreach (var c in distinct_cat)
                {
                    qs = qs + "'" + c + "'";
                }
                qs = qs + "]}}";
                include_fields = new string[] { "id", "Name", "description" };
                exclude_fields = new string[] { "_id" };
                var new_list = mongo_query(qs, "category", include_fields, exclude_fields);
                // now get cat names by id
                return new_list;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw;
            }
        }

        public string[] get_prod_list_by_brand(string brand)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'brand':'" + brand + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = get_cat_short_include_fields(); //new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }
        public string[] get_premium_prod_list(int premium_threshhold, int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{ 'active' : 1, 'price.list':{$lte: " + max + ", $gte:" + premium_threshhold + "}}";
            }
            else
            {
                q = "{ 'active' : 1, 'price.list':{$gte:" + premium_threshhold + "}}";
            }
            //q = "{ 'active' : 1, 'price.list':{$gte:" + premium_threshhold + "}}";

            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public string[] get_newarrivals_prod_list(int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();

            int DateRange = Convert.ToInt32(System.Configuration.ConfigurationSettings.AppSettings["DateRange"].ToString());
            var EndDate = "'" + DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
            var StartDate = "'" + System.DateTime.UtcNow.AddDays(-DateRange).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "'";
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{'active' : 1, 'price.list':{ $lte: " + max + ", $gte: " + min + "  },  'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")}}";

            }
            else
            {
                q = "{'active' : 1, 'create_date':{$lte:ISODate(" + EndDate + "), $gte:ISODate(" + StartDate + ")}}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public string[] get_search_prod_list(string s, int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //string q = "{'brand':'" + brand + "'}}";
            //string q = "{ 'active' : 1, 'price.list':{$gt:'" + premium_threshhold + "'}}";
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{ 'active' : 1, 'price.list':{ $lte: " + max + ", $gte: " + min + "  }, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
            }
            else
            {
                q = "{ 'active' : 1, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }
        public string[] get_search_prod_list(string s, string context, int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                if (context != "0")
                {

                    q = "{ 'active' : 1, 'price.list':{ $lte: " + max + ", $gte: " + min + "  }, 'cat_id.cat_id':/^" + context.Substring(0, context.Length - 1) + "/, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
                else
                {
                    q = "{ 'active' : 1,'price.list':{ $lte: " + max + ", $gte: " + min + "  },  'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
            }
            else
            {

                if (context != "0")
                {

                    q = "{ 'active' : 1, 'cat_id.cat_id':/^" + context.Substring(0, context.Length - 1) + "/, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
                else
                {
                    q = "{ 'active' : 1, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                }
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public List<filter> get_search_prod_list_filters(string s)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'description': /" + s + "/i} , $or: {'shortdesc':/" + s + "/i}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }


        public List<filter> get_express_items_filters()
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                // first get the cats that special items belong to
                string q = "{ 'active' : 1, 'Express':'1'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "cat_id.cat_id" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                List<String> list_cats = new List<string>();
                foreach (var c in cursor)
                {
                    foreach (BsonDocument d in c.ToBsonDocument()["cat_id"].AsBsonArray)
                    {
                        list_cats.Add(d["cat_id"].ToString());
                    }
                }

                // filter only unique strings in the list_cat
                var unique_cats_arr = (new HashSet<string>(list_cats.ToArray())).ToArray();

                if (unique_cats_arr.Length > 0)
                {
                    q = "{ 'active' : 1, 'id':{$in:['";
                    for (int i = 0; i < unique_cats_arr.Count(); i++)
                    {
                        if (i == 0)
                        {
                            q = q + unique_cats_arr[i] + "'";
                        }
                        else
                        {
                            q = q + ",'" + unique_cats_arr[i] + "'";
                        }
                    }
                    q = q + "]}}";
                    document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    queryDoc = new QueryDocument(document);
                    include_fields = new string[] { "filters" };
                    exclude_fields = new string[] { "_id" };
                    cursor = dal.execute_mongo_db("category", queryDoc, include_fields, exclude_fields);

                    List<filter> list = new List<filter>();
                    foreach (var c in cursor)
                    {
                        // skip empty filters
                        if (c.ToBsonDocument()["filters"].AsBsonArray.Count() != 0)
                        {
                            foreach (BsonDocument d in c.ToBsonDocument()["filters"].AsBsonArray)
                            {
                                filter f = new filter();
                                string[] arr;
                                f.name = d["name"].ToString();
                                string v = d["values"].ToString();
                                v = v.Substring(1, v.Length - 2);
                                arr = v.Split(',');
                                f.values = arr.ToList();
                                bool lfound = false;
                                foreach (filter fltr in list)
                                {
                                    if (fltr.name == f.name)
                                    {
                                        fltr.values = f.values.Concat(fltr.values).Distinct().ToList();
                                        lfound = true;

                                    }
                                }
                                if (!lfound)
                                {
                                    list.Add(f);
                                }
                            }
                        }
                    }
                    return list;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }


        public string[] get_express_prod_list(int min, int max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{ 'active' : 1, 'Express':'1', 'price.list':{ $lte: " + max + ", $gte: " + min + "  } }";
            }
            else
            {
                q = "{ 'active' : 1, 'Express':'1'}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }


        public string[] get_prod_list_by_range(long min, long max)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            if (min > max) // wrong case
            {
                // reset to default and ignore
                min = 0;
                max = 0;
            }
            string q;
            if ((min > 0) || (max > 0))
            {
                q = "{ 'active' : 1, 'price.list':{ $lte: " + max + ", $gte: " + min + "  } }";
            }
            else
            {
                q = "{ 'active' : 1}";
            }
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls", "feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }

        public string[] get_prod_list_by_brand(string brand, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'brand':'" + brand + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            //string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls","feature", "have_child" };
            string[] include_fields = get_cat_short_include_fields();
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
        }


        public string[] get_prod_details(string product_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'active':1, id:'" + product_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "parent_cat_id", "sku", "point", "Name", "description", "shortdesc", "feature", "image_urls", "video_urls", "image", "Express", "stock" };
            string[] exclude_fields = new string[] { "_id" };
            //return mongo_query(q, "product", include_fields, exclude_fields);
            List<string> list = new List<string>();
            List<string> child_list = new List<string>();
            list = mongo_query_list(q, "product", include_fields, exclude_fields);
            child_list = get_child_products(product_id);
            if (child_list.Count > 0)
            {
                foreach (string el in child_list)
                {
                    list.Add(el);
                }
            }
            return list.ToArray();
        }

        public string[] get_shipping_details(string cat_id)
        {
            // evoke mongodb connection method in dal

            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = "{'active':1, id:'" + cat_id + "'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "min_ship", "max_ship", "Shipping" };
                string[] exclude_fields = new string[] { "_id" };
                return mongo_query(q, "category", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        //public prod get_prod_details_for_cart(string product_id)
        //{
        //    // evoke mongodb connection method in dal

        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{id:'" + product_id + "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "price", "sku", "size" };
        //    string[] exclude_fields = new string[] { "_id" };
        //    MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
        //    prod p = new prod();
        //    foreach (var c in cursor)
        //    {
        //        p.id = c.ToBsonDocument()["id"].ToString();
        //        p.ch
        //        var ch = get_child_products(p.id);
        //        p.sku = c.ToBsonDocument()["sku"].ToString();
        //        p.price = JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString());
        //    }


        //    return list.ToArray();
        //}

        //public string[] get_child_products(string product_id)
        //{
        //    // evoke mongodb connection method in dal
        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{parent_prod:'" + product_id + "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "id", "size", "sku","stock" };
        //    string[] exclude_fields = new string[] { "_id" };
        //    string[] ascending_sort_fields = { "id" };
        //    string[] descending_sort_fields = { };
        //    return mongo_query(q, "product", include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
        //}
        public List<product> get_prod_list_by_cat(string cat_id)
        {
            var products = new List<product>();
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{ cat : {$elemMatch : {id:'51bb3c22782b4c22745a5100'}}}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                foreach (var c in cursor)
                {
                    product prod = new product();
                    //prod.Id = c.ToBsonDocument()["_id"].ToString();
                    //prod.prod_name = c.ToBsonDocument()["prod_name"].ToString();
                    prod.sku = c.ToBsonDocument()["sku"].ToString();
                    prod.description = c.ToBsonDocument()["description"].ToString();
                    prod.price = JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString());
                    prod.image_urls = JsonConvert.DeserializeObject<List<imgurl>>(c.ToBsonDocument()["image_urls"].ToString());
                    prod.video_urls = JsonConvert.DeserializeObject<List<url>>(c.ToBsonDocument()["video_urls"].ToString());
                    prod.ad_urls = JsonConvert.DeserializeObject<List<adurl>>(c.ToBsonDocument()["video_urls"].ToString());
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products;

        }
        // this method returns validated cart by checking stock, active flag and price
        public user_cart validate_cart(string email_id)
        {
            try
            {
                List<cart_item> to_be_deleted = new List<cart_item>();
                user_cart uc = get_cart(email_id);
                foreach (cart_item c in uc.cart_items)
                {
                    // get product info for the cart item
                    product p = get_product_details(c.id);
                    if (p.active == 0)
                    {
                        //delete item
                        //uc.cart_items.Remove(c);
                        to_be_deleted.Add(c);
                    }
                    else
                    {
                        if (c.selected_size == "ONESIZE")
                        {
                            //product p = get_product_details(c.id);
                            if (p.stock <= 0)
                            {
                                // delete item
                                //uc.cart_items.Remove(c);
                                to_be_deleted.Add(c);
                            }
                            else
                            {
                                if (c.quantity > p.stock)
                                {
                                    c.quantity = (int)p.stock; // set quantity = stock
                                }
                                c.mrp = p.price.mrp;
                                c.final_offer = p.price.final_offer;
                                c.discount = p.price.discount;
                                if (p.Express == "1")
                                {
                                    c.express = true;
                                }
                                else { c.express = false; }
                            }

                        }
                        else
                        {
                            product p1 = get_product_details(c.cart_item_id);

                            //product p = get_product_details(c.id);
                            if (p1.stock <= 0)
                            {
                                // delete item
                                //uc.cart_items.Remove(c);
                                to_be_deleted.Add(c);
                            }
                            else
                            {
                                if (c.quantity > p1.stock)
                                {
                                    c.quantity = (int)p1.stock; // set quantity = stock
                                }
                                if (p1.Express == "1")
                                {
                                    c.express = true;
                                }
                                else { c.express = false; }
                            }

                        }

                    }

                }
                foreach (cart_item c in to_be_deleted)
                {
                    uc.cart_items.Remove(c);
                }
                write_cart(uc);
                return uc;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }

        private product get_product_details(string id)
        {
            // evoke mongodb connection method in dal
            var products = new List<product>();
            try
            {
                dal.DataAccess dal = new DataAccess();

                //MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);


                string q = "{sku:'" + id + "'}";// qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "id", "parent_prod", "sku", "stock", "active", "price", "express" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
                foreach (var c in cursor)
                {

                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.active = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
                    prod.stock = check_field(c.ToBsonDocument(), "stock") ? convertToInt(c.ToBsonDocument()["stock"]) : 0;
                    prod.price = check_field(c.ToBsonDocument(), "price") ? JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString()) : null;
                    prod.Express = check_field(c.ToBsonDocument(), "Express") ? convertToString(c.ToBsonDocument()["Express"]) : null;
                    products.Add(prod);
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return products[0];
        }

        private user_cart get_cart(string email_id)
        {
            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{email_id:'" + email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);
                user_cart uc = new user_cart();
                foreach (var c in cursor)
                {
                    //uc = (user_cart) c.ToBsonDocument();
                    uc.email_id = c.ToBsonDocument()["email_id"].ToString();

                    foreach (BsonDocument b in c.ToBsonDocument()["cart_items"].AsBsonArray)
                    {
                        uc.cart_items.Add(
                         new cart_item()
                         {
                             id = b.ToBsonDocument()["_id"].ToString(),
                             cart_item_id = b.ToBsonDocument()["cart_item_id"].ToString(),
                             brand = b.ToBsonDocument()["brand"].ToString(),
                             discount = convertToDouble(b.ToBsonDocument()["discount"].ToString()),
                             express = b.ToBsonDocument()["express"].ToString() == "0" ? false : true,
                             final_offer = convertToDouble(b.ToBsonDocument()["final_offer"].ToString()),
                             mrp = convertToDouble(b.ToBsonDocument()["mrp"].ToString()),
                             quantity = convertToInt(b.ToBsonDocument()["quantity"].ToString()),
                             //sizes = JsonConvert.DeserializeObject<List<size>>(b.ToBsonDocument()["sizes"].ToString())
                             selected_size = convertToString(b.ToBsonDocument()["selected_size"].ToString())
                         }
                         );
                    }

                }

                return uc;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }
        }

        public List<cart_item> get_cart_by_email_for_order(string email_id)
        {
            var cart_items = new List<cart_item>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'email_id':'" + email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                //string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);


                foreach (var c in cursor)
                {
                    //cart_item cart = new cart_item();
                    cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

                    for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++)
                    {
                        cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
                    }

                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return cart_items;
        }

        public List<cart_item> get_cart_by_email(string email_id, string company)
        {
            var cart_items = new List<cart_item>();

            var modified_cart_items = new List<cart_item>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'email_id':'" + email_id + "' ,'company':'" + company + "' }";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                //string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);


                foreach (var c in cursor)
                {
                    //cart_item cart = new cart_item();
                    cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

                    for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++)
                    {
                        cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
                    }

                    for (int j = 0; j < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); j++)
                    {
                        int cur_prod_stock = get_prod_stock_qty(cart_items[j].cart_item_id.ToString());

                        string Product_ID = "";
                        var Prod_Id = cart_items[j].id.ToString().Split('.');
                        Product_ID = Prod_Id[0].ToString();

                        if (chk_prod_active(Product_ID) == 1)
                        {
                            if (cur_prod_stock > 0)
                            {
                                cart_item ci = new cart_item();
                                price op = new price();
                                op = get_prod_price(cart_items[j].id.ToString());
                                if (cart_items[j].quantity > cur_prod_stock)
                                {
                                    cart_items[j].quantity = cur_prod_stock;
                                }
                                cart_items[j].mrp = op.mrp;
                                cart_items[j].discount = op.discount;
                                cart_items[j].final_offer = op.final_offer;
                                ci = cart_items[j];
                                modified_cart_items.Add(ci);
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return modified_cart_items;
        }




        public List<mongo_order> send_will_ship_email(string email_id, string company)
        {
            var micro_order_list = new List<mongo_order>();

            helper_repository hr = new helper_repository();
            registration_repository rr = new registration_repository();
            checkoutrepository ocr = new checkoutrepository();


            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'status': 5}";

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "order_id", "user_id", "dispatch_date", "shipping_info", "courier_track_no", "courier_track_link", "courier_cost", "cart_data", "user_data", "selected_size", "selected_prod_name", "status" };
                string[] exclude_fields = new string[] { "_id" };


                MongoCursor cursor = dal.execute_mongo_db_sort("order", queryDoc, include_fields, exclude_fields, "create_ts");

                foreach (var c in cursor)
                {
                    string dispatch_date = "";
                    mongo_order omo = new mongo_order();
                    omo.order_id = check_field(c.ToBsonDocument(), "order_id") ? convertToDouble(c.ToBsonDocument()["order_id"]) : 0;
                    email_id = check_field(c.ToBsonDocument(), "user_id") ? convertToString(c.ToBsonDocument()["user_id"]) : null;
                    dispatch_date = check_field(c.ToBsonDocument(), "dispatch_date") ? convertToString(c.ToBsonDocument()["dispatch_date"]) : null;
                    omo.shipping_info = check_field(c.ToBsonDocument(), "shipping_info") ? convertToString(c.ToBsonDocument()["shipping_info"]) : null;
                    omo.courier_track_no = check_field(c.ToBsonDocument(), "courier_track_no") ? convertToString(c.ToBsonDocument()["courier_track_no"]) : null;
                    omo.courier_track_link = check_field(c.ToBsonDocument(), "courier_track_link") ? convertToString(c.ToBsonDocument()["courier_track_link"]) : null;
                    omo.courier_cost = check_field(c.ToBsonDocument(), "courier_cost") ? convertToDouble(c.ToBsonDocument()["courier_cost"]) : 0;


                    string strMsgBody = ocr.GetMicroShippingStatusBody(convertToString(omo.order_id), convertToString(dispatch_date), convertToString(omo.shipping_info), convertToString(omo.courier_track_no), convertToString(omo.courier_track_link), convertToDouble(omo.courier_cost), 5, ref email_id);

                    string bcclist = ConfigurationSettings.AppSettings["Management"].ToString() + "," + ConfigurationSettings.AppSettings["bcc_email"].ToString();
                    if (email_id.Length > 0 && strMsgBody.Length > 0)
                    {
                        hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Your Gift has been shipped by GRASS : Order # " + omo.order_id.ToString(), strMsgBody);

                    }


                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return micro_order_list;
        }


        public List<mongo_order> get_micro_order_list(string email_id, string company)
        {
            var micro_order_list = new List<mongo_order>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{}";

                if (email_id != null && email_id != "" && email_id != "null" && company != null && company != "" && company != "null")
                {
                    q = "{'user_id': /.*" + email_id + ".*/i ,'store': /.*" + company + ".*/i }";
                }
                else if (email_id != null && email_id != "" && email_id != "null" && (company == null || company == "" || company == "null"))
                {
                    q = "{'user_id': /.*" + email_id + ".*/i}";
                }
                else if (company != null && company != "" && company != "null" && (email_id == null || email_id == "" || email_id == "null"))
                {
                    q = "{'store': /.*" + company + ".*/i }";
                }


                //string q = "{}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "order_id", "dispatch_date", "shipping_info", "courier_track_no", "courier_track_link", "courier_cost", "cart_data", "user_data", "selected_size", "selected_prod_name", "status" };
                string[] exclude_fields = new string[] { "_id" };

                //  MongoCursor cursor = dal.execute_mongo_db("order", queryDoc);
                //MongoCursor cursor = dal.execute_mongo_db("order", queryDoc, include_fields, exclude_fields);

                MongoCursor cursor = dal.execute_mongo_db_sort("order", queryDoc, include_fields, exclude_fields, "create_ts");

                foreach (var c in cursor)
                {
                    mongo_order omo = new mongo_order();
                    omo.order_id = check_field(c.ToBsonDocument(), "order_id") ? convertToDouble(c.ToBsonDocument()["order_id"]) : 0;
                    omo.cart_data = JsonConvert.DeserializeObject<mongo_cart_data>(c.ToBsonDocument()["cart_data"].ToString());
                    omo.user_data = JsonConvert.DeserializeObject<mongo_user_data>(c.ToBsonDocument()["user_data"].ToString());
                    omo.selected_size = check_field(c.ToBsonDocument(), "selected_size") ? convertToString(c.ToBsonDocument()["selected_size"]) : null;
                    omo.selected_prod_name = check_field(c.ToBsonDocument(), "selected_prod_name") ? convertToString(c.ToBsonDocument()["selected_prod_name"]) : null;
                    omo.status = check_field(c.ToBsonDocument(), "status") ? convertToInt(c.ToBsonDocument()["status"]) : 0;
                    omo.dispatch_date = check_field(c.ToBsonDocument(), "dispatch_date") ? convertToString(c.ToBsonDocument()["dispatch_date"]) : null;
                    omo.shipping_info = check_field(c.ToBsonDocument(), "shipping_info") ? convertToString(c.ToBsonDocument()["shipping_info"]) : null;
                    omo.courier_track_no = check_field(c.ToBsonDocument(), "courier_track_no") ? convertToString(c.ToBsonDocument()["courier_track_no"]) : null;
                    omo.courier_track_link = check_field(c.ToBsonDocument(), "courier_track_link") ? convertToString(c.ToBsonDocument()["courier_track_link"]) : null;
                    omo.courier_cost = check_field(c.ToBsonDocument(), "courier_cost") ? convertToDouble(c.ToBsonDocument()["courier_cost"]) : 0;

                    micro_order_list.Add(omo);


                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return micro_order_list;
        }



        public int chk_prod_active(string Product_ID)
        {

            int IsActive = 0;

            dal.DataAccess dal = new DataAccess();
            string q = "{id:'" + Product_ID + "'}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "active" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
            foreach (var c in cursor)
            {
                IsActive = check_field(c.ToBsonDocument(), "active") ? convertToInt(c.ToBsonDocument()["active"]) : 0;
            }
            return IsActive;
        }
        public int get_prod_stock_qty(string Product_sku)
        {
            try
            {
                // evoke mongodb connection method in dal

                int Quantity = 0;

                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{sku:'" + Product_sku + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.stock = check_field(c.ToBsonDocument(), "stock") ? convertToInt(c.ToBsonDocument()["stock"]) : 0;
                    products.Add(prod);
                }

                Quantity = convertToInt(products[0].stock.ToString());

                return Quantity;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        public price get_prod_price(string Product_id)
        {
            price prod_price = new price();

            try
            {
                // evoke mongodb connection method in dal


                dal.DataAccess dal = new DataAccess();
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_id + "'}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

                var products = new List<product>();
                foreach (var c in cursor)
                {
                    product prod = new product();
                    prod.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                    prod.price = JsonConvert.DeserializeObject<price>(c.ToBsonDocument()["price"].ToString());
                    products.Add(prod);
                }

                prod_price = products[0].price;

                return prod_price;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return prod_price;
            }
        }

        //Added By Hassan To Get The Wish List
        public List<cart_item> get_wishlist_by_email(string email_id)
        {
            var cart_items = new List<cart_item>();

            var modified_cart_items = new List<cart_item>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'email_id':'" + email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                //string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("wishlist", queryDoc);


                foreach (var c in cursor)
                {
                    //cart_item cart = new cart_item();
                    cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

                    for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++)
                    {
                        cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
                    }

                    for (int j = 0; j < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); j++)
                    {
                        int cur_prod_stock = get_prod_stock_qty(cart_items[j].cart_item_id.ToString());

                        string Product_ID = "";
                        var Prod_Id = cart_items[j].id.ToString().Split('.');
                        Product_ID = Prod_Id[0].ToString();

                        if (chk_prod_active(Product_ID) == 1)
                        {

                            if (cur_prod_stock > 0)
                            {
                                cart_item ci = new cart_item();
                                price op = new price();
                                op = get_prod_price(cart_items[j].id.ToString());
                                if (cart_items[j].quantity > cur_prod_stock)
                                {
                                    cart_items[j].quantity = cur_prod_stock;
                                }
                                cart_items[j].mrp = op.mrp;
                                cart_items[j].discount = op.discount;
                                cart_items[j].final_offer = op.final_offer;
                                ci = cart_items[j];
                                modified_cart_items.Add(ci);
                            }
                        }
                    }
                }
                //return mongo_query_singleton (q, "cart", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return modified_cart_items;
        }


        //Added By Dipanjan To Get The Product By Brand
        public string[] get_prod_by_brand(string brand_name)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = "{'active':1,'brand':/^" + brand_name + "$/i}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = get_cat_short_include_fields(); //new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            //string[] ascending_sort_fields = {"price.list", "price.discount"};
            return mongo_query(q, "product", include_fields, exclude_fields);
        }


        //public List<cart_item> get_cart_by_email(string email_id)
        //{
        //    // evoke mongodb connection method in dal
        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{'email_id':'" + email_id+ "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "email_id", "cart_items" };
        //    //string[] exclude_fields = new string[] { "_id" };
        //    MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);

        //    var cart_items = new List<cart_item>();
        //    foreach (var c in cursor)
        //    {
        //        //cart_item cart = new cart_item();
        //        cart_items = JsonConvert.DeserializeObject<List<cart_item>>(c.ToBsonDocument()["cart_items"].ToString());

        //        for (int i = 0; i < c.ToBsonDocument()["cart_items"].AsBsonArray.Count(); i++ )
        //        {
        //            cart_items[i].id = c.ToBsonDocument()["cart_items"].AsBsonArray[i].ToBsonDocument()["_id"].ToString();
        //        }
        //        //cart.sizes = JsonConvert.DeserializeObject<List<size>>(c.ToBsonDocument()["sizes"].ToString());

        //        //cart.id = c.ToBsonDocument()["id"].ToString();
        //        //cart.brand = c.ToBsonDocument()["brand"].ToString();
        //        //cart.cart_item_id = c.ToBsonDocument()["cart_item_id"].ToString();
        //        //cart.image = c.ToBsonDocument()["image"].ToString();
        //        //cart.sku = c.ToBsonDocument()["sku"].ToString();
        //        //cart.name = c.ToBsonDocument()["name"].ToString();
        //        //cart.mrp = convertToDouble(c.ToBsonDocument()["name"].ToString());
        //        //cart.final_offer = convertToDouble(c.ToBsonDocument()["final_offer"].ToString());
        //        //cart.discount = convertToDouble(c.ToBsonDocument()["discount"].ToString());
        //        //cart.express = c.ToBsonDocument()["discount"].ToBoolean();
        //        //cart.sizes = JsonConvert.DeserializeObject<List<size>>(c.ToBsonDocument()["sizes"].ToString());
        //        //cart_items.Add(cart);
        //    }
        //    //return mongo_query_singleton (q, "cart", include_fields, exclude_fields);
        //    return cart_items;
        //}

        // mysql

        //public List<user_shipping> get_user_shipping_info(string user_id)
        //{

        //    DataAccess da = new DataAccess();
        //    DataTable dtResult = new DataTable();
        //    List<user_shipping> user_shipping_list = new List<user_shipping>();
        //    try
        //    {
        //        dtResult = da.ExecuteDataTable("get_shipping_by_user"
        //                        , da.Parameter("_user_id", user_id)
        //                        );




        //        if (dtResult != null && dtResult.Rows.Count > 0)
        //        {
        //            foreach (DataRow dr in dtResult.Rows)
        //            {
        //                user_shipping_list.Add(
        //                new user_shipping
        //                {
        //                    id = convertToInt(dr["id"].ToString()),
        //                    address = convertToString(dr["address"]),
        //                    city = convertToString(dr["city"]),
        //                    state = convertToString(dr["state"]),
        //                    pincode = convertToString(dr["pincode"]),
        //                    shipping_name = convertToString(dr["shipping_name"]),
        //                    mobile_number = convertToString(dr["mobile_number"]),
        //                    name = convertToString(dr["name"])
        //                }
        //               );

        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }
        //    return user_shipping_list;

        //}


        public List<mongo_shipping_address> get_user_shipping_info(string user_id, string store)
        {

            List<mongo_shipping_address> olus = new List<mongo_shipping_address>();

            try
            {

                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{'email_id' :'" + user_id + "' ,'company':'" + store + "'}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("user", queryDoc);


                foreach (var c in cursor)
                {

                    List<mongo_shipping_address> blank_ship = new List<mongo_shipping_address>();
                    olus = check_field(c.ToBsonDocument(), "shipping_data") ? JsonConvert.DeserializeObject<List<mongo_shipping_address>>(c.ToBsonDocument()["shipping_data"].ToString()) : blank_ship;

                }


            }

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return olus;

        }



        public List<point_details> get_user_points_credit(string user_id)
        {
            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<point_details> point_details_credit = new List<point_details>();
            try
            {
                dtResult = da.ExecuteDataTable("get_user_point_credit"
                                , da.Parameter("_user_id", user_id)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        point_details_credit.Add(
                        new point_details
                        {
                            user_id = convertToString(dr["user_id"]),
                            txn_amount = convertToDouble(dr["txn_amount"]),
                            txn_ts = convertToDate(dr["txn_ts"]),
                            txn_comment = convertToString(dr["txn_comment"]),
                            txn_type = "c"
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return point_details_credit;
        }
        public List<point_details> get_user_points_debit(string user_id)
        {
            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<point_details> point_details_debit = new List<point_details>();
            try
            {
                dtResult = da.ExecuteDataTable("get_user_point_debit"
                                , da.Parameter("_user_id", user_id)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        point_details_debit.Add(
                        new point_details
                        {
                            user_id = convertToString(dr["user_id"]),
                            txn_amount = convertToDouble(dr["txn_amount"]),
                            txn_ts = convertToDate(dr["txn_ts"]),
                            txn_comment = convertToString(dr["txn_comment"]),
                            txn_type = "d"
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return point_details_debit;
        }


        public long get_points_against_user(string company, string email_id)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();

            long Points = 0;
            try
            {

                dtResult = da.ExecuteDataTable("get_user_points"
                               , da.Parameter("_company", company)
                               , da.Parameter("_email_id", email_id)
                               );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        Points = convertToLong(dtResult.Rows[i]["PointsBalance"]);
                    }

                }
                return Points;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

        }


        public List<order> get_user_order_details(string user_id)
        {
            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<order> order_details = new List<order>();
            try
            {
                dtResult = da.ExecuteDataTable("get_user_order_details"
                                , da.Parameter("_user_id", user_id)
                                , da.Parameter("_flag", 0)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    foreach (DataRow dr in dtResult.Rows)
                    {
                        order_details.Add(
                        new order
                        {
                            user_id = convertToString(dr["user_id"]),
                            order_id = convertToLong(dr["order_id"]),
                            total_amount = convertToDecimal(dr["total_amount"]),
                            paid_amount = convertToDecimal(dr["paid_amount"]),
                            create_ts = convertToDate(dr["create_ts"]),
                            payment_info = Convert.ToString(dr["payment_info"]),
                        }
                       );

                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return order_details;
        }

        public int add_express_shipping(int order_id, int express_shipping_amount)
        {

            DataAccess da = new DataAccess();
            int o_id = da.ExecuteSP("add_express_shipping",
                            da.Parameter("_order_id", order_id),
                            da.Parameter("_express_shipping", express_shipping_amount)
                          );
            return o_id;
        }


        //public int write_ship_data(user_shipping s)
        public int write_ship_data(mongo_ship s)
        {
            try
            {

                //Check Previous Order 

                dal.DataAccess dal = new DataAccess();
                string qq = "{user_id:'" + s.user_id + "' , store:'" + s.store + "'}";
                BsonDocument docu_ment = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qq);
                QueryDocument query_Doc = new QueryDocument(docu_ment);
                MongoCursor cur_sor = dal.execute_mongo_db("order", query_Doc);

                var match = false;
                int return_order_info = 0;

                foreach (var cc in cur_sor)
                {

                    match = true;
                    break;
                }

                if (match == false)
                {

                    // 1. Get Cart info from MongoDB
                    List<cart_item> cart_items = new List<cart_item>();
                    //cart_items = get_cart_by_email(s.user_id);
                    cart_items = get_cart_by_email_for_order(s.user_id);

                    // 2. Save Shipping Data




                    string q = "{email_id:'" + s.user_id + "'}";
                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                    QueryDocument queryDoc = new QueryDocument(document);
                    string[] include_fields = new string[] { "shipping_data", "email_id" };
                    string[] exclude_fields = new string[] { "_id" };

                    MongoCursor cursor = dal.execute_mongo_db("user", queryDoc, include_fields, exclude_fields);
                    var Prev_Shipping_Address = new List<mongo_shipping_address>();
                    var New_Shipping_Address = new List<mongo_shipping_address>();
                    foreach (var outc in cursor)
                    {
                        List<mongo_shipping_address> blank_ship = new List<mongo_shipping_address>();
                        Prev_Shipping_Address = check_field(outc.ToBsonDocument(), "shipping_data") ? JsonConvert.DeserializeObject<List<mongo_shipping_address>>(outc.ToBsonDocument()["shipping_data"].ToString()) : blank_ship;
                        New_Shipping_Address = check_field(outc.ToBsonDocument(), "shipping_data") ? JsonConvert.DeserializeObject<List<mongo_shipping_address>>(outc.ToBsonDocument()["shipping_data"].ToString()) : blank_ship;
                    }


                    double nextshipinfoid = 0;

                    if (s.id == 0)
                    {
                        var ship_info = getNextSequence("shipping_info_id", "seq");
                        nextshipinfoid = convertToDouble(ship_info.seq);
                        s.shipping_address.id = nextshipinfoid;
                    }
                    else
                    {

                        for (int i = 0; i < New_Shipping_Address.Count(); i++)
                        {
                            if (New_Shipping_Address[i].id == s.id)
                            {
                                var onlyMatch = New_Shipping_Address.Single(ship => ship.id == s.id);
                                New_Shipping_Address.Remove(onlyMatch);
                            }
                        }

                    }

                    New_Shipping_Address.Add(s.shipping_address);

                    var whereclause = "";
                    whereclause = "{email_id:'" + s.user_id + "'}";
                    MongoDB.Driver.Builders.UpdateBuilder update = new MongoDB.Driver.Builders.UpdateBuilder();

                    var bsonShippingArray = new BsonArray();

                    for (int i = 0; i < Prev_Shipping_Address.Count(); i++)
                    {
                        bsonShippingArray.Add(Prev_Shipping_Address[i].ToBsonDocument());
                    }

                    BsonDocument bd_prod = s.ToBsonDocument();
                    update.PullAll("shipping_data", bsonShippingArray);
                    string result1 = dal.mongo_update("user", whereclause, bd_prod, update);

                    MongoDB.Driver.Builders.UpdateBuilder upd = new MongoDB.Driver.Builders.UpdateBuilder();

                    var bsonNewShippingArray = new BsonArray();

                    for (int i = 0; i < New_Shipping_Address.Count; i++)
                    {
                        bsonNewShippingArray.Add(New_Shipping_Address[i].ToBsonDocument());
                    }

                    upd.PushAll("shipping_data", bsonNewShippingArray);
                    string result2 = dal.mongo_update("user", whereclause, bd_prod, upd);

                    DataAccess da = new DataAccess();
                    //DataTable dt_ship_info = da.ExecuteDataTable("write_user_shipping",
                    //                da.Parameter("_id", s.id),
                    //                da.Parameter("_user_id", s.user_id),
                    //                da.Parameter("_shipping_name", s.shipping_name),
                    //                da.Parameter("_pincode", s.pincode),
                    //                da.Parameter("_address", s.address),
                    //                da.Parameter("_state", s.state),
                    //                da.Parameter("_city", s.city),
                    //                da.Parameter("_mobile_number", s.mobile_number),
                    //                da.Parameter("_name", s.name)
                    //              );
                    //s.id = convertToInt(dt_ship_info.Rows[0]["shipping_info_id"].ToString());
                    // 3. Save Cart details in mysql - this is when cart becomes close to order

                    cart_data c = new cart_data()
                    {
                        cart_id = Guid.NewGuid().ToString(),
                        cart_data_json = Newtonsoft.Json.JsonConvert.SerializeObject(cart_items),
                        user_id = s.user_id
                    };


                    //int retval = da.ExecuteSP("cart_data_write",
                    //             da.Parameter("_cart_data_id", c.cart_id),
                    //             da.Parameter("_cart_data", c.cart_data_json),
                    //             da.Parameter("_user_id", c.user_id)
                    //             );

                    // 3a. Get Shipping information
                    double shipping_charge = 0;
                    double min_order_for_free_shipping = 0;

                    string q1 = "{name:'" + s.store + "'}";
                    BsonDocument document1 = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q1);
                    QueryDocument queryDoc1 = new QueryDocument(document1);
                    string[] include_fields1 = new string[] { "shipping_data", "min_order_for_free_shipping" };
                    string[] exclude_fields1 = new string[] { "_id" };

                    MongoCursor cursor1 = dal.execute_mongo_db("user", queryDoc1, include_fields1, exclude_fields1);

                    foreach (var outc1 in cursor1)
                    {

                        shipping_charge = check_field(outc1.ToBsonDocument(), "shipping_charge") ? convertToDouble(outc1.ToBsonDocument()["shipping_charge"]) : 0;
                        min_order_for_free_shipping = check_field(outc1.ToBsonDocument(), "min_order_for_free_shipping") ? convertToDouble(outc1.ToBsonDocument()["min_order_for_free_shipping"]) : 0;
                    }

                    //DataTable dt_ship_charge = da.ExecuteDataTable("get_shipping_charges_by_user",
                    //                           da.Parameter("_email_id", c.user_id)
                    //                           );
                    //if ((dt_ship_charge != null) && (dt_ship_charge.Rows.Count > 0))
                    //{
                    //    shipping_charge = convertToDouble(dt_ship_charge.Rows[0]["shipping_charge"].ToString());
                    //    min_order_for_free_shipping = convertToDouble(dt_ship_charge.Rows[0]["min_order_for_free_shipping"].ToString());
                    //}
                    //else
                    //{
                    //    shipping_charge = 0;
                    //    min_order_for_free_shipping = 0;
                    //}

                    // 4. Save in Order Table 
                    // get cart total
                    double cart_total = 0;
                    foreach (cart_item cart in cart_items)
                    {
                        cart_total = cart_total + cart.final_offer * cart.quantity;
                    }
                    if (cart_total < min_order_for_free_shipping)
                    {
                        cart_total = cart_total + shipping_charge;
                    }

                    // create order object
                    user_order o = new user_order()
                    {
                        order_id = 0, // first time order getting generated
                        order_display_id = Guid.NewGuid().ToString(),
                        store = s.store,
                        cart_id = c.cart_id,
                        shipping_info_id = nextshipinfoid,
                        user_id = c.user_id,
                        discount_code = "default",
                        total_amount = 0,
                        points = 0,
                        paid_amount = 0,
                        status = 0, // order booked
                        payment_info = "",
                        rejection_reason = "",
                        create_ts = DateTime.Now,
                        modify_ts = DateTime.Now
                    };


                    //DataTable dt_order_info = da.ExecuteDataTable("order_Insert",
                    //             da.Parameter("_order_id", o.order_id),
                    //             da.Parameter("_cart_id", o.cart_id),
                    //             da.Parameter("_user_id", o.user_id),
                    //             da.Parameter("_discount_code", o.discount_code),
                    //             da.Parameter("_total_amount", o.total_amount),
                    //             da.Parameter("_points", o.points),
                    //             da.Parameter("_paid_amount", o.paid_amount),
                    //             da.Parameter("_status", o.status),
                    //             da.Parameter("_payment_info", o.payment_info),
                    //             da.Parameter("_rejection_reason", o.rejection_reason),
                    //             da.Parameter("_create_ts", o.create_ts),
                    //             da.Parameter("_modify_ts", o.modify_ts),
                    //             da.Parameter("_order_display_id", o.order_display_id),
                    //             da.Parameter("_store", o.store),
                    //             da.Parameter("_shipping_info_id", o.shipping_info_id)
                    //             );
                    //o.order_id = convertToInt(dt_order_info.Rows[0]["order_id"].ToString());


                    /********************************* Insert Order Record In Mongo db in This Code Block ***************************************/

                    mongo_order mo = new mongo_order();
                    // evoke mongodb connection method in dal

                    mongo_cart_data mcd = new mongo_cart_data();
                    mcd.cart_data_id = c.cart_id;
                    mcd.cart_data = c.cart_data_json;
                    mcd.user_id = c.user_id;

                    var order_next_id = getNextSequence("orderid", "seq");

                    mo._id = ObjectId.GenerateNewId().ToString();
                    mo.order_id = convertToDouble(order_next_id.seq.ToString());
                    o.order_id = convertToInt(order_next_id.seq.ToString());
                    mo.cart_id = c.cart_id;
                    mo.cart_data = mcd;
                    mo.user_id = o.user_id;
                    mo.user_data = s.user_data;

                    mo.discount_code = o.discount_code;
                    mo.total_amount = o.total_amount;
                    mo.points = o.points;
                    mo.paid_amount = convertToDouble(o.paid_amount);
                    mo.status = o.status;
                    mo.payment_info = o.payment_info;
                    mo.rejection_reason = o.rejection_reason;
                    mo.create_ts = o.create_ts;
                    mo.modify_ts = o.modify_ts;
                    mo.pay_sent_ts = DateTime.Now;
                    mo.pay_conf_ts = DateTime.Now;
                    mo.order_display_id = o.order_display_id;
                    mo.store = o.store;
                    mo.shipping_info_id = o.shipping_info_id;
                    mo.shipping_address = s.shipping_address;

                    mo.points_amount = 0;
                    mo.egift_vou_no = "";
                    mo.egift_vou_amt = 0;
                    mo.shipping_info = "";
                    mo.delayed_note = "";
                    mo.cancellation_note = "";
                    mo.courier_track_no = "";
                    mo.courier_track_link = "";
                    mo.discount_amount = 0;
                    mo.dispatch_date = null;
                    mo.courier_cost = 0;
                    mo.shipping_amount = 0;
                    mo.expiry_datetime = null;
                    mo.extended_reason = "";
                    mo.dispute_data = new dispute();
                    mo.dispute_data.dispute_ts = DateTime.Now.ToString("dd/MM/yyyy");
                    //   mo.dispute_data.dispute_ts = DateTime.Now.ToString("dd/MM/yyyy");




                    BsonDocument bd_mo = mo.ToBsonDocument();
                    string result = dal.mongo_write("order", bd_mo);

                    return_order_info = o.order_id;


                }
                else
                {

                    //"You have already placed an order for sample";
                    return_order_info = 0;

                }

                return return_order_info;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }


        public string come_from_edm_track(edm_track gmo)
        {
            try
            {

                //Check Previous Order 

                dal.DataAccess dal = new DataAccess();

                edm_track et = new edm_track();
                et._id = ObjectId.GenerateNewId().ToString();

                et.edm_id = gmo.edm_id;
                et.email_id = gmo.email_id;
                et.hitting_time = System.DateTime.Now;

                BsonDocument bd_mo = et.ToBsonDocument();
                return dal.mongo_write("edm_track", bd_mo);

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "error";
            }
        }

        public string come_from_mail_track(mail_track gmo)
        {
            try
            {

                //Check Previous Order 

                dal.DataAccess dal = new DataAccess();

                edm_track et = new edm_track();
                et._id = ObjectId.GenerateNewId().ToString();

               // et.edm_id = gmo.edm_id;
                et.email_id = gmo.email_id;
                et.hitting_time = System.DateTime.Now;

                BsonDocument bd_mo = et.ToBsonDocument();
                return dal.mongo_write("edm_track", bd_mo);

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "error";
            }
        }

        public int place_order(grass_micro_order gmo)
        {
            try
            {

                //Check Previous Order 

                dal.DataAccess dal = new DataAccess();
                string qq = "{user_id:'" + gmo.email + "' , store:'" + gmo.company + "'}";
                BsonDocument docu_ment = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qq);
                QueryDocument query_Doc = new QueryDocument(docu_ment);
                MongoCursor cur_sor = dal.execute_mongo_db("order", query_Doc);

                var match = false;
                int return_order_info = 0;

                var cart_gen_id = Guid.NewGuid().ToString();

                foreach (var cc in cur_sor)
                {

                    match = true;
                    break;
                }

                if (match == false)
                {
                    // create order object
                    user_order o = new user_order()
                    {
                        order_id = 0, // first time order getting generated
                        order_display_id = Guid.NewGuid().ToString(),
                        store = gmo.company,
                        cart_id = cart_gen_id,
                        shipping_info_id = 1,
                        user_id = gmo.email,
                        discount_code = "default",
                        total_amount = 0,
                        points = 0,
                        paid_amount = 0,
                        status = 2, // order booked
                        payment_info = "",
                        rejection_reason = "",
                        create_ts = DateTime.Now,
                        modify_ts = DateTime.Now
                    };


                    /********************************* Insert Order Record In Mongo db in This Code Block ***************************************/

                    mongo_order mo = new mongo_order();
                    // evoke mongodb connection method in dal

                    mongo_cart_data mcd = new mongo_cart_data();
                    mcd.cart_data_id = cart_gen_id;
                    mcd.cart_data = gmo.sel_product_string;
                    mcd.user_id = gmo.email;



                    mongo_user_data mud = new mongo_user_data();
                    mud.first_name = gmo.first_name;
                    mud.last_name = gmo.last_name;
                    mud.company = gmo.company;
                    mud.user_type = gmo.designation;
                    mud.email_id = gmo.email;
                    mud.mobile_no = gmo.phone;
                    mud.address = gmo.address_line1;
                    mud.street = gmo.address_line2;
                    mud.zipcode = gmo.zipcode;


                    mongo_shipping_address msa = new mongo_shipping_address();
                    msa._id = ObjectId.GenerateNewId().ToString();
                    msa.id = 1;
                    msa.user_id = gmo.email;
                    msa.shipping_name = gmo.first_name + ' ' + gmo.last_name;
                    msa.pincode = gmo.zipcode;
                    msa.address = gmo.address_line1;
                    msa.state = "";
                    msa.city = gmo.address_line2;
                    msa.mobile_number = gmo.phone;
                    msa.name = gmo.first_name + ' ' + gmo.last_name;


                    var order_next_id = getNextSequence("orderid", "seq");

                    mo._id = ObjectId.GenerateNewId().ToString();
                    mo.order_id = convertToDouble(order_next_id.seq.ToString());
                    o.order_id = convertToInt(order_next_id.seq.ToString());
                    mo.cart_id = cart_gen_id;
                    mo.cart_data = mcd;
                    mo.user_id = gmo.email;
                    mo.selected_size = gmo.selected_size;
                    mo.selected_prod_name = gmo.selected_product.Name;
                    mo.site_version = gmo.site_version;
                    mo.user_data = mud;

                    mo.discount_code = o.discount_code;
                    mo.total_amount = o.total_amount;
                    mo.points = o.points;
                    mo.paid_amount = convertToDouble(o.paid_amount);
                    mo.status = o.status;
                    mo.payment_info = o.payment_info;
                    mo.rejection_reason = o.rejection_reason;
                    mo.create_ts = o.create_ts;
                    mo.modify_ts = o.modify_ts;
                    mo.pay_sent_ts = DateTime.Now;
                    mo.pay_conf_ts = DateTime.Now;
                    mo.order_display_id = o.order_display_id;
                    mo.store = o.store;
                    mo.shipping_info_id = o.shipping_info_id;
                    mo.shipping_address = msa;

                    mo.points_amount = 0;
                    mo.egift_vou_no = "";
                    mo.egift_vou_amt = 0;
                    mo.shipping_info = "";
                    mo.delayed_note = "";
                    mo.cancellation_note = "";
                    mo.courier_track_no = "";
                    mo.courier_track_link = "";
                    mo.discount_amount = 0;
                    mo.dispatch_date = null;
                    mo.courier_cost = 0;
                    mo.shipping_amount = 0;
                    mo.expiry_datetime = null;
                    mo.extended_reason = "";
                    mo.dispute_data = new dispute();
                    mo.dispute_data.dispute_ts = DateTime.Now.ToString("dd/MM/yyyy");

                    BsonDocument bd_mo = mo.ToBsonDocument();
                    string result = dal.mongo_write("order", bd_mo);

                   send_order_email(mo, gmo);
                   


                    return_order_info = o.order_id;


                }
                else
                {

                    //"You have already placed an order for sample";
                    return_order_info = 0;

                }

                return return_order_info;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        public string add_user_details(grass_micro_client gmc)
        {
            try
            {

                dal.DataAccess dal = new DataAccess();
                gmc._id = ObjectId.GenerateNewId().ToString();
                gmc.create_ts = DateTime.Now;
                BsonDocument bd_gmc = gmc.ToBsonDocument();
                string result = dal.mongo_write("non_campaign_user", bd_gmc);
                return result;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "error";
            }
        }

        public int place_order_restrict(grass_camp_order gmo)
        {
            try
            {
                int return_order_info = 0;
            //    grass_camp_order gco = new grass_camp_order();  atul 24/6
                //Check Previous Order 

                dal.DataAccess dal = new DataAccess();

                DataAccess da = new DataAccess();
                // string enquire_data = "";
                // string status;
                // string id;
                List<grass_camp_order> lst = new List<grass_camp_order>();
                DataTable dt = da.ExecuteDataTable("get_grass_camp_data_by_email", da.Parameter("_company_email", gmo.email));
                if (dt != null && dt.Rows.Count > 0)
                {


                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // grass_camp_order gco = new grass_camp_order();
                        //gco.id = convertToLong(dt.Rows[i]["id"]);
                        //gco.name = convertToString(dt.Rows[i]["name"]);
                        //gco.landline = convertToString(dt.Rows[i]["landline"]);
                        //gco.city = convertToString(dt.Rows[i]["city"]);
                        //gco.company_name = convertToString(dt.Rows[i]["company_name"]);
                        //gco.company_email = convertToString(dt.Rows[i]["company_email"]);
                        //gco.designation = convertToString(dt.Rows[i]["designation"]); ;
                        //gco.address = convertToString(dt.Rows[i]["address"]); ;
                        //gco.pincode = convertToString(dt.Rows[i]["pincode"]); ;
                        ////oei.create_ts   =convertToString(dt.Rows[i]["create_ts"]);;
                        //gco.product_name = convertToString(dt.Rows[i]["product_name"]); ;
                        //gco.product_qty = convertToString(dt.Rows[i]["product_qty"]); ;
                        //gco.created_ts = Convert.ToDateTime(dt.Rows[i]["created_ts"]); ;

                        //lst.Add(gco);
                        gmo.id = convertToLong(dt.Rows[i]["id"]);
                        gmo.name = convertToString(dt.Rows[i]["name"]);
                        gmo.mobile = convertToString(dt.Rows[i]["mobile"]);
                        gmo.landline = convertToString(dt.Rows[i]["landline"]);
                        gmo.city = convertToString(dt.Rows[i]["city"]);
                        gmo.company_name = convertToString(dt.Rows[i]["company_name"]);
                        gmo.company_email = convertToString(dt.Rows[i]["company_email"]);
                        gmo.designation = convertToString(dt.Rows[i]["designation"]); ;
                        gmo.address = convertToString(dt.Rows[i]["address"]); ;
                        gmo.pincode = convertToString(dt.Rows[i]["pincode"]); ;
                        //oei.create_ts   =convertToString(dt.Rows[i]["create_ts"]);;
                        gmo.product_name = convertToString(dt.Rows[i]["product_name"]); ;
                        gmo.product_qty = convertToString(dt.Rows[i]["product_qty"]); ;
                        gmo.created_ts = Convert.ToDateTime(dt.Rows[i]["created_ts"]); ;

                        lst.Add(gmo);
                    }



                    //else
                    //{
                    //   // return "Your Details are not Present in our Database";
                    //    return 0;
                    //}
                    //     return lst;




                    //string qq = "{ EmailID: /^" + gmo.email + "$/i,edm_id: /^" + gmo.edm_id + "$/i }";
                    ////  string qq = "{EmailID:'" + gmo.email + "' , edm_id:'" + gmo.edm_id + "'}";
                    //BsonDocument docu_ment = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qq);
                    //QueryDocument query_Doc = new QueryDocument(docu_ment);
                    //MongoCursor cur_sor = dal.execute_mongo_db("CentralEDM", query_Doc);

                    var match = true;
                   

                    var cart_gen_id = Guid.NewGuid().ToString();

                    //foreach (var cc in cur_sor)
                    //{

                    //    match = true;
                    //    break;
                    //}

                    if (match == true)
                    {

                      //  string qqq1 = "{ user_id: /^" + gmo.email + "$/i,edm_id: /^" + gmo.edm_id + "$/i }";
                        string qqq1 = "{ user_id: /^" + gmo.email +"$/i }";
                        //  string qq = "{EmailID:'" + gmo.email + "' , edm_id:'" + gmo.edm_id + "'}";
                        BsonDocument docu_ment1 = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qqq1);
                        QueryDocument query_Doc1 = new QueryDocument(docu_ment1);
                        MongoCursor cur_sor1 = dal.execute_mongo_db("order", query_Doc1);

                        var match1 = false;


                        foreach (var cc1 in cur_sor1)
                        {
                            match1 = true;
                            break;
                        }

                        if (match1 == false)
                        {

                            // create order object
                            user_order o = new user_order()
                            {
                                order_id = 0, // first time order getting generated
                                order_display_id = Guid.NewGuid().ToString(),
                                //  store = gmo.company,
                                cart_id = cart_gen_id,
                                shipping_info_id = 1,
                                user_id = gmo.company_email,
                                discount_code = "default",
                                total_amount = 0,
                                points = 0,
                                paid_amount = 0,
                                status = 2, // order booked
                                payment_info = "",
                                rejection_reason = "",
                                create_ts = DateTime.Now,
                                modify_ts = DateTime.Now
                            };


                            /********************************* Insert Order Record In Mongo db in This Code Block ***************************************/

                            mongo_order mo = new mongo_order();
                            // evoke mongodb connection method in dal

                            mongo_cart_data mcd = new mongo_cart_data();
                            mcd.cart_data_id = cart_gen_id;
                            mcd.cart_data = gmo.sel_product_string;
                            mcd.user_id = gmo.company_email;



                            mongo_user_data mud = new mongo_user_data();
                            mud.first_name = gmo.name;
                            // mud.last_name = gmo.last_name;
                            mud.company = gmo.company_name;
                            mud.user_type = gmo.designation;
                            mud.email_id = gmo.company_email;
                            mud.mobile_no = gmo.mobile;
                            mud.address = gmo.address;
                            //   mud.street = gmo.address_line2;
                            mud.zipcode = gmo.pincode;


                            mongo_shipping_address msa = new mongo_shipping_address();
                            msa._id = ObjectId.GenerateNewId().ToString();
                            msa.id = 1;
                            msa.user_id = gmo.company_email;
                            msa.shipping_name = gmo.name;
                            msa.pincode = gmo.pincode;
                            msa.address = gmo.address;
                            msa.state = "";
                            msa.city = gmo.city;
                            msa.mobile_number = gmo.mobile;
                            msa.name = gmo.name;


                           var order_next_id = getNextSequence("orderid", "seq");  

                            mo._id = ObjectId.GenerateNewId().ToString();
                            mo.order_id = convertToDouble(order_next_id.seq.ToString());
                            o.order_id = convertToInt(order_next_id.seq.ToString());
                            mo.cart_id = cart_gen_id;
                            mo.cart_data = mcd;
                            mo.user_id = gmo.company_email;
                            mo.selected_size = gmo.selected_size;
                            mo.selected_prod_name = gmo.selected_product.Name;
                            mo.site_version = gmo.site_version;
                            mo.user_data = mud;
                            mo.edm_id = gmo.edm_id;

                            mo.discount_code = o.discount_code;
                            mo.total_amount = o.total_amount;
                            mo.points = o.points;
                            mo.paid_amount = convertToDouble(o.paid_amount);
                            mo.status = o.status;
                            mo.payment_info = o.payment_info;
                            mo.rejection_reason = o.rejection_reason;
                            mo.create_ts = o.create_ts;
                            mo.modify_ts = o.modify_ts;
                            mo.pay_sent_ts = DateTime.Now;
                            mo.pay_conf_ts = DateTime.Now;
                            mo.order_display_id = o.order_display_id;
                            mo.store = o.store;
                            mo.shipping_info_id = o.shipping_info_id;
                            mo.shipping_address = msa;

                            mo.points_amount = 0;
                            mo.egift_vou_no = "";
                            mo.egift_vou_amt = 0;
                            mo.shipping_info = "";
                            mo.delayed_note = "";
                            mo.cancellation_note = "";
                            mo.courier_track_no = "";
                            mo.courier_track_link = "";
                            mo.discount_amount = 0;
                            mo.dispatch_date = null;
                            mo.courier_cost = 0;
                            mo.shipping_amount = 0;
                            mo.expiry_datetime = null;
                            mo.extended_reason = "";
                            mo.dispute_data = new dispute();
                            mo.dispute_data.dispute_ts = DateTime.Now.ToString("dd/MM/yyyy");

                            BsonDocument bd_mo = mo.ToBsonDocument();
                            string result = dal.mongo_write("order", bd_mo);

                          // send_order_email(mo, gmo);
                            send_order_email_camp(mo, gmo);



                            return_order_info = o.order_id;

                        }
                        else
                        {
                            //"You have already placed an order for sample";
                            return_order_info = 0;
                        }




                    }
                }
                else
                {

                    //"You are not present in our db";
                    return_order_info = -1;

                }

                return return_order_info;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        public void send_order_email(mongo_order omo, grass_micro_order ogmo)
        {


            try
            {
                string email_id = "";
                string company = "";
                string strEmailBody = getMicroOrderBody(omo, ogmo);
               
                

                string strSubject = "Your Gift from Grass-The Brand for the brands : Order # " + convertToString(omo.order_id);
                email_id = ogmo.email.ToLower();
                helper_repository hr = new helper_repository();
                registration_repository rr = new registration_repository();
                string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                if (email_id.Length > 0 && strEmailBody.Length > 0)
                {
                    hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, strSubject, strEmailBody);
                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

        }
        public void send_order_email_camp(mongo_order omo, grass_camp_order ogmo)
        {


            try
            {
                string email_id = "";
                string company = "";
              //  string strEmailBody = getMicroOrderBody(omo, ogmo);
                string strEmailBody = getCampOrderBody(omo, ogmo);

                string strSubject = "Your Gift from Grass-The Brand for the brands : Order # " + convertToString(omo.order_id);
                email_id = ogmo.email.ToLower();
                helper_repository hr = new helper_repository();
                registration_repository rr = new registration_repository();
                string cclist = ConfigurationSettings.AppSettings["Management"].ToString();
                string bcclist = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                if (email_id.Length > 0 && strEmailBody.Length > 0)
                {
                    hr.SendMail(rr.getUserEMailAddress(email_id), cclist, bcclist, strSubject, strEmailBody);
                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }

        }

        public string getMicroOrderBody(mongo_order omo, grass_micro_order ogmo)
        {


            string mailBody = "";

            mailBody = @"
                        <html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= 'http://grass.shopinbulk.in/' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%'style='padding-left: 120px; font-size: x-large;'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'>Order Confirmation</td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Verdana, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FULL_NAME##,</em></strong></font>
                        </td>
                        <td width='5%'>&nbsp;</td>
                        </tr> 
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='center' valign='top'><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:30px; line-height:21px'>Thank you.
                        </font><br /><br /></td>
                        <td width='5%'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'>
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>          
                        The T-Shirt of your choice <strong>##PROD_NAME##</strong> , will be gift wrapped and sent to the shipping address below. 
                        <br /><br />
                        Here is the summary of your order: <strong style='color: #f1613b'>Order ID:</strong> ##ORDER_NO##</font></td>
                        <td width='5%'>&nbsp;</td>
                        </tr>						
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>  <tr>  <td colspan='4'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>  
						
					    <strong>1 Product Code: ##PROD_CODE## - ##PROD_NAME##  </td>     <td style='padding:0cm 0cm 0cm 0cm'></td>  </tr>  <tr>  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >  
						<div style='padding:0cm 0cm 0cm 8.0pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Image</span></p>  	<div style='margin-top:7.5pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'><img src='##PROD_IMG_LINK##' width='90px' height='120px'></span></p>  </div>  </div>  </td>  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >  
					
						<div style='padding:0cm 0cm 0cm 8.0pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size:</span></p>  <div style='margin-top:7.5pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>##PROD_SIZE##</span></p>  </div>   </div>   </td>    </tr>  <tr>      
                        <td bgcolor='#f1613b' ></td>
                        <td  bgcolor='#f1613b'  >
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                       
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'></span></p>
                        </div>
                        </div>
                        </td>

                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        <td style='padding: 10px;' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'><strong><em>Shipping Address </em></strong></font><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>   Name:  ##FULL_NAME##<br />   Company :##COMPANY## <br />   Designation :##DESIGNATION## <br />   Address :##ADDRESS_LINE_1## <br />   Mobile:##PHONE## <br />Zip Code :##ZIP_CODE## <br /><br />
                        Once verified and confirmed ,you will receive the order dispatch mail with further details.In the meanwhile if you have any queries or clarifications please connect with our customer support team at 91 9686202046 / 9972334590 or email us at <a  style='color: #f1613b'  href='mailto:customerfirst@grassapparels.in' target='_blank'>customerfirst@grassapparels.in</a>
                        <br />
                        <br />
                        You can continue shopping at <a style='color: #f1613b' href='http://grass.shopinbulk.in/' target='_blank'>www.grass.shopinbulk.in</a>
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Program Center</strong><br/>
                        <strong>GRASS Promo Apparels</strong>           
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2015 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in/' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in/</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";




            string strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";

            mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
            mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
            mailBody = mailBody.Replace("##PROD_CODE##", convertToString(ogmo.selected_product.sku));
            mailBody = mailBody.Replace("##PROD_NAME##", convertToString(ogmo.selected_product.Name));
            mailBody = mailBody.Replace("##PROD_IMG_LINK##", convertToString(ogmo.selected_product.image_urls[0].thumb_link));
            mailBody = mailBody.Replace("##ORDER_NO##", convertToString(omo.order_id));
            mailBody = mailBody.Replace("##PROD_SIZE##", convertToString(ogmo.selected_size));
            mailBody = mailBody.Replace("##FULL_NAME##", convertToString(ogmo.first_name + ' ' + ogmo.last_name));
            mailBody = mailBody.Replace("##DESIGNATION##", convertToString(ogmo.designation));
            mailBody = mailBody.Replace("##COMPANY##", convertToString(ogmo.company));
            mailBody = mailBody.Replace("##ADDRESS_LINE_1##", convertToString(ogmo.address_line1));
            mailBody = mailBody.Replace("##ADDRESS_LINE_2##", convertToString(ogmo.address_line2));
            mailBody = mailBody.Replace("##PHONE##", convertToString(ogmo.phone));
            mailBody = mailBody.Replace("##ZIP_CODE##", convertToString(ogmo.zipcode));


            //  mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
            return mailBody;


        }

        public string getCampOrderBody(mongo_order omo, grass_camp_order ogmo)
        {


            string mailBody = "";

            mailBody = @"
                        <html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= 'http://grass.shopinbulk.in/' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%'style='padding-left: 120px; font-size: x-large;'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'>Order Confirmation</td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Verdana, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FULL_NAME##,</em></strong></font>
                        </td>
                        <td width='5%'>&nbsp;</td>
                        </tr> 
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='center' valign='top'><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:30px; line-height:21px'>Thank you.
                        </font><br /><br /></td>
                        <td width='5%'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'>
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>          
                        The T-Shirt of your choice <strong>##PROD_NAME##</strong> , will be gift wrapped and sent to the shipping address below. 
                        <br /><br />
                        Here is the summary of your order: <strong style='color: #f1613b'>Order ID:</strong> ##ORDER_NO##</font></td>
                        <td width='5%'>&nbsp;</td>
                        </tr>						
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>  <tr>  <td colspan='4'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>  
						
					    <strong>1 Product Code: ##PROD_CODE## - ##PROD_NAME##  </td>     <td style='padding:0cm 0cm 0cm 0cm'></td>  </tr>  <tr>  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >  
						<div style='padding:0cm 0cm 0cm 8.0pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Image</span></p>  	<div style='margin-top:7.5pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'><img src='##PROD_IMG_LINK##' width='90px' height='120px'></span></p>  </div>  </div>  </td>  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >  
					
						<div style='padding:0cm 0cm 0cm 8.0pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size:</span></p>  <div style='margin-top:7.5pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>##PROD_SIZE##</span></p>  </div>   </div>   </td>    </tr>  <tr>      
                        <td bgcolor='#f1613b' ></td>
                        <td  bgcolor='#f1613b'  >
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                       
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'></span></p>
                        </div>
                        </div>
                        </td>

                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        <td style='padding: 10px;' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'><strong><em>Shipping Address </em></strong></font><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>   Name:  ##FULL_NAME##<br />   Company :##COMPANY## <br />   Designation :##DESIGNATION## <br />   Address :##ADDRESS_LINE_1## <br /> City :##City## <br />   Mobile:##PHONE## <br />Zip Code :##ZIP_CODE## <br /><br />
                        Once verified and confirmed ,you will receive the order dispatch mail with further details.In the meanwhile if you have any queries or clarifications please connect with our customer support team at 91 9686202046 / 9972334590 or email us at <a  style='color: #f1613b'  href='mailto:customerfirst@grassapparels.in' target='_blank'>customerfirst@grassapparels.in</a>
                        <br />
                        <br />
                        You can continue shopping at <a style='color: #f1613b' href='http://grass.shopinbulk.in/' target='_blank'>www.grass.shopinbulk.in</a>
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Program Center</strong><br/>
                        <strong>GRASS Promo Apparels</strong>           
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2015 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in/' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in/</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";




            string strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";

            mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
            mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
            mailBody = mailBody.Replace("##PROD_CODE##", convertToString(ogmo.selected_product.sku));
            mailBody = mailBody.Replace("##PROD_NAME##", convertToString(ogmo.selected_product.Name));
            mailBody = mailBody.Replace("##PROD_IMG_LINK##", convertToString(ogmo.selected_product.image_urls[0].thumb_link));
            mailBody = mailBody.Replace("##ORDER_NO##", convertToString(omo.order_id));
            mailBody = mailBody.Replace("##PROD_SIZE##", convertToString(ogmo.selected_size));
            mailBody = mailBody.Replace("##FULL_NAME##", convertToString(ogmo.name));
            mailBody = mailBody.Replace("##DESIGNATION##", convertToString(ogmo.designation));
            mailBody = mailBody.Replace("##COMPANY##", convertToString(ogmo.company_name));
            mailBody = mailBody.Replace("##ADDRESS_LINE_1##", convertToString(ogmo.address));
            mailBody = mailBody.Replace("##City##", convertToString(ogmo.city));
            mailBody = mailBody.Replace("##PHONE##", convertToString(ogmo.mobile));
            mailBody = mailBody.Replace("##ZIP_CODE##", convertToString(ogmo.pincode));


            //  mailBody = mailBody.Replace("img/emails/", ConfigurationSettings.AppSettings["email_img"].ToString() + "/img/emails/");
            return mailBody;


        }


        public countercollection getNextSequence(string seq_type, string track_field)
        {
            dal.DataAccess dal = new DataAccess();
            string q = "{'_id':'" + seq_type + "'}";
            QueryDocument queryDoc = createQueryDocument(q);
            UpdateBuilder u = new UpdateBuilder();
            u.Inc(track_field, 1);
            BsonDocument nextorderid = dal.mongo_find_and_modify("counters", queryDoc, u);
            var ordid = nextorderid.ToString();
            countercollection occ = JsonConvert.DeserializeObject<countercollection>(nextorderid.ToString());
            return occ;
        }

        private QueryDocument createQueryDocument(string s)
        {
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(s);
            QueryDocument queryDoc = new QueryDocument(document);
            return queryDoc;
        }

        #endregion



        #region Private Methods
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private float convertToFloat(object o)
        {
            try
            {
                if (o != null)
                {
                    return float.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private double convertToDouble(object o)
        {
            try
            {
                if (o != null)
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != "") && (o.ToString() != "BsonNull"))
                {
                    return o.ToString();
                }
                else if (o.ToString() == "BsonNull")
                {
                    return "";
                }
                else { return ""; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }
        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private string[] get_cat_short_include_fields()
        {
            string[] include_fields = new string[] {    "id", "stock","brand", "price", 
                                                        "cat_id", "sku", "point", 
                                                        "Name", "description", 
                                                        "image_urls", "feature", 
                                                        "have_child", "Express" };
            return include_fields;
        }
        public List<string> get_child_products(string product_id)
        {
            List<string> list = new List<string>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();
                string q = "{parent_prod:'" + product_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "id", "size", "sku", "stock" };
                string[] exclude_fields = new string[] { "_id" };
                string[] ascending_sort_fields = { "id" };
                string[] descending_sort_fields = { };
                list = mongo_query_list(q, "product", include_fields, exclude_fields);
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list;
        }
        //private List<childproduct> get_child_products(string product_id)
        //{
        //    // evoke mongodb connection method in dal
        //    dal.DataAccess dal = new DataAccess();
        //    string q = "{parent_prod:'" + product_id + "'}}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    string[] include_fields = new string[] { "id", "size", "sku", "stock" };
        //    string[] exclude_fields = new string[] { "_id" };
        //    MongoCursor cursor = dal.execute_mongo_db("product", queryDoc);

        //    var child_prod_list = new List<childproduct>();
        //    foreach (var c in cursor)
        //    {
        //        childproduct ch = new childproduct();
        //        ch.id = c.ToBsonDocument()["id"].ToString();
        //        ch.size = c.ToBsonDocument()["size"].ToString();
        //        ch.sku = c.ToBsonDocument()["sku"].ToString();
        //        ch.stock = convertToDouble( c.ToBsonDocument()["stock"].ToString());
        //        child_prod_list.Add(ch);
        //    }
        //    return child_prod_list;
        //}
        private string[] mongo_query(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            List<String> list = new List<string>();
            try
            {
                dal.DataAccess dal = new DataAccess();
                string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);

                //foreach (var c in cursor)
                //{

                //    list.Add(c.ToBsonDocument().ToString());
                //}
                bool l_prodlist_call = false;
                foreach (var c in cursor)
                {

                    for (int i = 0; i < c.ToBsonDocument().ElementCount; i++)
                    {
                        if (c.ToBsonDocument().ElementAt(i).Name == "have_child")
                        {
                            l_prodlist_call = true;
                            break;
                        }
                    }
                    if (l_prodlist_call)
                    {
                        if (c.ToBsonDocument()["have_child"] > 0)
                        {
                            c.ToBsonDocument()["stock"] = -9999;
                        }
                        else
                        {
                            c.ToBsonDocument()["stock"] = c.ToBsonDocument()["stock"].ToInt32();
                        }
                    }
                    list.Add(c.ToBsonDocument().ToString());
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return list.ToArray();
        }
        private string mongo_query_singleton(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {
                list.Add(c.ToBsonDocument().ToString());
            }
            return list[0];
        }
        private long mongo_query_count(string qs, string collection)
        {
            dal.DataAccess dal = new DataAccess();

            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(qs);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "_id" };
            string[] exclude_fields = new string[] { "" };
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc);
            int count = 0;
            if (cursor != null)
            {
                foreach (var c in cursor)
                {
                    count++;
                }
                return count;
            }
            else
            {
                return 0;
            }
        }
        private string[] mongo_query(string qs, string collection, string[] include_fields, string[] exclude_fields, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();
        }
        private List<string> mongo_query_list(string qs, string collection, string[] include_fields, string[] exclude_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {
                c.ToBsonDocument()["stock"] = c.ToBsonDocument()["stock"].ToString();

                list.Add(c.ToBsonDocument().ToString());
            }
            return list;
        }
        private List<string> mongo_query_list(string qs, string collection, string[] include_fields, string[] exclude_fields, string[] ascending_sort_fields, string[] descending_sort_fields)
        {
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            MongoCursor cursor = dal.execute_mongo_db(collection, queryDoc, include_fields, exclude_fields, ascending_sort_fields, descending_sort_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list;
        }
        public void del_cart(string email_id, string company)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //BsonDocument bd_deals = dl.ToBsonDocument();
            var whereclause = "";
            whereclause = "{email_id:'" + email_id + "' , company:'" + company + "' }";
            dal.mongo_remove("cart", whereclause);
        }
        private void insert_cart(user_cart cart)
        {
            // evoke mongodb connection method in dal
            //cart._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cart = cart.ToBsonDocument();
            string result = dal.mongo_write("cart", bd_cart);
        }
        private void insert_cart(bulk_cart cart)
        {
            // evoke mongodb connection method in dal
            //cart._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cart = cart.ToBsonDocument();
            string result = dal.mongo_write("cart", bd_cart);
        }

        // Added By Hassan
        public void del_wishlist(string email_id)
        {
            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            //BsonDocument bd_deals = dl.ToBsonDocument();
            var whereclause = "";
            whereclause = "{email_id:'" + email_id + "'}";
            dal.mongo_remove("wishlist", whereclause);
        }
        // Added By Hassan
        private void insert_wishlist(user_cart cart)
        {
            // evoke mongodb connection method in dal
            //cart._id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cart = cart.ToBsonDocument();
            string result = dal.mongo_write("wishlist", bd_cart);
        }

        // Added By Hassan
        public bool wishlist_exists(string email_id)
        {
            string q = "{email_id:'" + email_id + "'}}";
            if (mongo_query_count(q, "wishlist") > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }



        public bool cart_exists(string email_id, string company)
        {
            string q = "{email_id:'" + email_id + "' , company:'" + company + "'}";
            if (mongo_query_count(q, "cart") > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion

        #region post methodsval
        public string write_cart(user_cart cart)
        {
            try
            {
                // evoke mongodb connection method in dal
                cart._id = ObjectId.GenerateNewId().ToString();
                string email = cart.email_id;
                string company = cart.company;
                dal.DataAccess dal = new DataAccess();
                if (cart_exists(email, company))
                {
                    // frst delete
                    del_cart(email, company);
                    // then insert
                    insert_cart(cart);

                }
                else
                {
                    // insert
                    insert_cart(cart);
                }
                return "success";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return ex.Message.ToString();
            }

        }


        public string add_cart(bulk_cart cart)
        {
            try
            {
                // evoke mongodb connection method in dal
                cart._id = ObjectId.GenerateNewId().ToString();
                string email = cart.email_id;
                string company = cart.company;
                dal.DataAccess dal = new DataAccess();
                if (cart_exists(email, company))
                {
                    // frst delete
                    del_cart(email, company);
                    // then insert
                    insert_cart(cart);

                }
                else
                {
                    // insert
                    insert_cart(cart);
                }
                return "success";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return ex.Message.ToString();
            }

        }

        // Added By Hassan To Write Wish List 
        public string write_wishlist(user_cart cart)
        {
            try
            {
                // evoke mongodb connection method in dal
                cart._id = ObjectId.GenerateNewId().ToString();
                string email = cart.email_id;
                dal.DataAccess dal = new DataAccess();
                if (wishlist_exists(email))
                {
                    // frst delete
                    del_wishlist(email);
                    // then insert
                    insert_wishlist(cart);

                }
                else
                {
                    // insert
                    insert_wishlist(cart);
                }
                return "success";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return ex.Message.ToString();
            }

        }

        public user_cart reprice_cart(user_cart cart)
        {
            try
            {
                // get cart
                //var  c = new user_cart();
                //c = get_cart(cart.email_id);


                dal.DataAccess dal = new DataAccess();
                string q = "{email_id:'" + cart.email_id + "'}}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "email_id", "cart_items" };
                string[] exclude_fields = new string[] { "_id" };
                MongoCursor cursor = dal.execute_mongo_db("cart", queryDoc);
                user_cart uc = new user_cart();
                foreach (var c in cursor)
                {
                    //uc = (user_cart) c.ToBsonDocument();
                    uc.email_id = c.ToBsonDocument()["email_id"].ToString();

                    foreach (BsonDocument b in c.ToBsonDocument()["cart_items"].AsBsonArray)
                    {
                        uc.cart_items.Add(
                             new cart_item()
                             {
                                 cart_item_id = b.ToBsonDocument()["cart_item_id"].ToString(),
                                 brand = b.ToBsonDocument()["brand"].ToString(),
                                 discount = convertToDouble(b.ToBsonDocument()["discount"].ToString()),
                                 express = b.ToBsonDocument()["express"].ToString() == "0" ? false : true,
                                 final_offer = convertToDouble(b.ToBsonDocument()["final_offer"].ToString()),
                                 image = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<imgurl>(b.ToBsonDocument()["image"].AsBsonDocument),
                                 mrp = convertToDouble(b.ToBsonDocument()["mrp"].ToString()),
                                 quantity = convertToInt(b.ToBsonDocument()["quantity"].ToString()),
                                 selected_size = convertToString(b.ToBsonDocument()["selected_size"]),
                                 sku = b.ToBsonDocument()["sku"].ToString(),
                                 name = b.ToBsonDocument()["name"].ToString(),
                                 sizes = b.ToBsonDocument()["sizes"].ToString() == "BsonNull" ? null : MongoDB.Bson.Serialization.BsonSerializer.Deserialize<List<size>>(b.ToBsonDocument()["sizes"].ToJson()),
                             });
                    }

                }

                //                return uc;//



                //replace size and price node


                return null;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }

        }
        public string insert_catagory(catagory cat)
        {
            // evoke mongodb connection method in dal
            cat.Id = ObjectId.GenerateNewId().ToString();
            dal.DataAccess dal = new DataAccess();
            BsonDocument bd_cat = cat.ToBsonDocument();
            string result = dal.mongo_write("cat", bd_cat);

            return result;
        }

        public string[] post_prod_list_by_qs(string qs)
        {

            // evoke mongodb connection method in dal
            dal.DataAccess dal = new DataAccess();
            string q = qs;//"{cat_id:{id:'" + cat_id + "'}}";
            BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
            QueryDocument queryDoc = new QueryDocument(document);
            string[] include_fields = new string[] { "id", "brand", "price", "cat_id", "sku", "point", "Name", "description", "image_urls" };
            string[] exclude_fields = new string[] { "_id" };
            MongoCursor cursor = dal.execute_mongo_db("product", queryDoc, include_fields, exclude_fields);
            List<String> list = new List<string>();
            foreach (var c in cursor)
            {

                list.Add(c.ToBsonDocument().ToString());
            }
            return list.ToArray();

        }

        public void prod_view_insert(prod_view pv)
        {
            pv._id = ObjectId.GenerateNewId().ToString();
            pv.view_ts = DateTime.Now;
            dal.DataAccess dal = new DataAccess();
            var result = dal.mongo_write("prod_view", pv.ToBsonDocument());
        }


        #endregion

    }
}
