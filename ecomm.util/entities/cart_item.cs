﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class cart_item
    {
        public string cart_item_id { get; set; }
        //public string _id { get; set; }
        public string id { get; set; }
        public string sku { get; set; }
        public string brand { get; set; }
        public int quantity { get; set; }
        public double mrp { get; set; }
        public double final_offer { get; set; }
        public string name { get; set; }
        public imgurl image { get; set; }
        public double discount { get; set; }
        public bool express { get; set; }
        public List<size> sizes { get; set; }
        public string selected_size { get; set; }
        

    }
}
