﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net.Mail;
using System.IO;
using System.Net;
using System.Web;


namespace ecomm.model.repository
{
    public class helper_repository
    {
        public void LogError(Exception ex)
        {
            
            string strLine="";
            try
            {
                System.IO.StreamWriter sw = new System.IO.StreamWriter(DateTime.Now.ToString() + ".log", true);

                strLine = "Msg: " + ex.Message + " , Source: " + ex.InnerException.Source + " , Trace : " + ex.StackTrace;
                sw.WriteLine(strLine);
                sw.Close();

            }
            catch 
            {
                ;
            }

        }
        public int SendMail(string toList, string ccList, string subject, string body)
        {

            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string strsmtpclient = System.Configuration.ConfigurationSettings.AppSettings["SmtpClient"].ToString();
            int port = Convert.ToInt16(ConfigurationSettings.AppSettings["SmtpPort"].ToString());
            string mailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
            string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();

            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(mailFrom);
                message.From = fromAddress;
                message.To.Add(toList);
                if (ccList != null && ccList != string.Empty)
                    message.Bcc.Add(ccList);
                    //message.CC.Add(ccList);
                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                // We use gmail as our smtp client
                smtpClient.Host = strsmtpclient;
                smtpClient.Port = port;
                smtpClient.EnableSsl = false;
              //  smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(
                    mailFrom, mailpwd);

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }

        public int SendMail(string toList, string ccList,string bccList, string subject, string body)
        {

            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string strsmtpclient = System.Configuration.ConfigurationSettings.AppSettings["SmtpClient"].ToString();
            int port = Convert.ToInt16(ConfigurationSettings.AppSettings["SmtpPort"].ToString());
            string mailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
            string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
            string strReplyTo = System.Configuration.ConfigurationSettings.AppSettings["MailReplyTo"].ToString();
            string strReplyDisplay = System.Configuration.ConfigurationSettings.AppSettings["MailReplyDisplay"].ToString();
            string strMailFromDisplay = System.Configuration.ConfigurationSettings.AppSettings["MailFromDisplay"].ToString();

            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(mailFrom, strMailFromDisplay);
                message.From = fromAddress;
                message.ReplyTo = new MailAddress(strReplyTo, strReplyDisplay);
                message.To.Add(toList);
                if (bccList != null && bccList != string.Empty)
                    message.Bcc.Add(bccList);
                //message.CC.Add(ccList);

                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                // We use gmail as our smtp client
                smtpClient.Host = strsmtpclient;
                smtpClient.Port = port;
               // smtpClient.EnableSsl = true;
               smtpClient.EnableSsl = false;
               smtpClient.UseDefaultCredentials = true;
           //    smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(
                    mailFrom, mailpwd);

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }

        public int SendEMail(string toList, string ccList, string bccList, string subject, string body)
        {

            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string strsmtpclient = System.Configuration.ConfigurationSettings.AppSettings["SmtpClient"].ToString();
            int port = Convert.ToInt16(ConfigurationSettings.AppSettings["SmtpPort"].ToString());
            string mailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
            string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();
            string strReplyTo = System.Configuration.ConfigurationSettings.AppSettings["MailReplyTo"].ToString();
            string strReplyDisplay = System.Configuration.ConfigurationSettings.AppSettings["MailReplyDisplay"].ToString();
            string strMailFromDisplay = System.Configuration.ConfigurationSettings.AppSettings["MailFromDisplay"].ToString();

            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(mailFrom, strMailFromDisplay);
                message.From = fromAddress;
                message.ReplyTo = new MailAddress(strReplyTo, strReplyDisplay);
                message.To.Add(toList);

                if (ccList != null && ccList != string.Empty)
                    message.CC.Add(ccList);

                if (bccList != null && bccList != string.Empty)
                    message.Bcc.Add(bccList);

                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                // We use gmail as our smtp client
                smtpClient.Host = strsmtpclient;
                smtpClient.Port = port;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(
                    mailFrom, mailpwd);

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }



        public int SendMailWithAttachment(string toList, string ccList, string bccList, string subject, string body,string fileName)
        {

            MailMessage message = new MailMessage();
            SmtpClient smtpClient = new SmtpClient();
            string strsmtpclient = System.Configuration.ConfigurationSettings.AppSettings["SmtpClient"].ToString();
            int port = Convert.ToInt16(ConfigurationSettings.AppSettings["SmtpPort"].ToString());
            string mailFrom = System.Configuration.ConfigurationSettings.AppSettings["MailFrom"].ToString();
            string mailpwd = System.Configuration.ConfigurationSettings.AppSettings["MailPwd"].ToString();

            string msg = string.Empty;
            try
            {
                MailAddress fromAddress = new MailAddress(mailFrom);
                message.From = fromAddress;
                message.To.Add(toList);
                if (ccList != null && ccList != string.Empty)
                    message.CC.Add(ccList);

                if (bccList != null && bccList != string.Empty)
                    message.CC.Add(bccList);

                if (fileName.Trim().Length > 0)
                {
                    message.Attachments.Add(new Attachment(fileName));
                }

                message.Subject = subject;
                message.IsBodyHtml = true;
                message.Body = body;
                // We use gmail as our smtp client
                smtpClient.Host = strsmtpclient;
                smtpClient.Port = port;
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = true;
                smtpClient.Credentials = new System.Net.NetworkCredential(
                    mailFrom, mailpwd);

                smtpClient.Send(message);
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 1;
        }
        #region
        /// <summary> 
        /// Userid = username; spwd = password, snumber = mobile number(multiple mobile number can be sent with a comma seperator),
        /// sSID : WebSMS(hard coded for now), sMessage =Message to be sent.
        /// Return Value is "The Message Id : 919886352744-fcb2a64747d94867b1f606652e70adf0" 
        /// Need to store this id.
        /// </summary> 

        public string SendSMS(string sUserID, string sPwd, string sNumber, string sSID, string sMessage)
        {
            string sURL = "http://smslane.com/vendorsms/pushsms.aspx?user=" + sUserID + "&password=" + sPwd + "&msisdn=" + sNumber + "&sid=" + sSID + "&msg=" + sMessage + "&fl=0&n%gwid=2" ;
            return GetResponse(sURL);
        }
        #endregion
        #region
        /// <summary> 
        /// Userid = username; spwd = password, MessageID = message response stored when the message was sent!
        /// Return Value is ""         
        /// </summary>
        public string SMSDeliveryReport(string sUserID, string sPwd, string MessageID)
        {
            string sURL = "http://www.smslane.com/vendorsms/CheckDelivery.aspx?user=" + sUserID + "&password=" + sPwd + "&MessageID=" + MessageID;
            return GetResponse(sURL);
        }
        #endregion
        #region
        /// <summary> 
        /// Userid = username; spwd = password, snumber = mobile number(multiple mobile number can be sent with a comma seperator),
        /// sSID : WebSMS(hard coded for now), sMessage =Message to be sent.
        /// Return Value is "The Message Id : 919886352744-fcb2a64747d94867b1f606652e70adf0" 
        /// Need to store this id.
        /// </summary> 

        public string SendSMS(string sUserID, string sPwd, string sNumber, string sSID, string sMessage,string sTransactional)
        {
            string sURL = "http://smslane.com/vendorsms/pushsms.aspx?user=" + sUserID + "&password=" + sPwd + "&msisdn=" + sNumber + "&sid=" + sSID + "&msg=" + sMessage + "&fl=0&gwid=2";
            return GetResponse(sURL);
        }
        #endregion

        public string SMSCreditBalance(string sUserID, string sPwd)
        {
            string sURL = "http://www.smslane.com/vendorsms/CheckBalance.aspx?user=" + sUserID + "&password=" + sPwd;
            return GetResponse(sURL);
        }
        private string GetResponse(string sURL)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(sURL);
            request.MaximumAutomaticRedirections = 4;
            request.Credentials = CredentialCache.DefaultCredentials;
            try
            {
                HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);
                string sResponse = readStream.ReadToEnd();
                response.Close();
                readStream.Close();
                return sResponse;
            }
            catch
            {
                return "";
            }
        }

        public void WriteToFile(string fileContent)
        {
            string message = "";
            string path = "c:\\Errorlog\\errorlog.txt";
            try
            {
                if (!File.Exists(path))
                {
                    FileStream fs1 = File.Create(path);
                    fs1.Close();
                }
                using (StreamWriter fs = new StreamWriter(path, true))
                {
                    fs.WriteLine("------------------------------------------------");
                    fs.WriteLine(fileContent);
                }
            }
            catch (Exception ex)
            {
                message = ex.InnerException.ToString();
               
            }
            
        } 
    }
}
