﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class airtel_monthly_tracker
    {
        public int id { get; set; }
        public int company_id { get; set; }
        public string displayname { get; set; }
        public string user_id { get; set; }
        public int cycle_id { get; set; }
        public string cycle_name { get; set; }


        public int lso_count_current_month { get; set; }
        public int sso_count_current_month { get; set; }
        public int total_fr_current_month { get; set; }
        public int high_value_fr_percent_ach_last_month_LM { get; set; }
        public int high_value_fr_current_month { get; set; }
        public int high_value_fr_tgt_for_current_month { get; set; }
        public int high_value_fr_percent_ach_for_current_month { get; set; }
        public int high_value_fr_tgt_vs_ach { get; set; }
        public int high_value_fr_fse_on_tgt_vs_ach { get; set; }
        public int high_value_fr_fse_on_LM_vs_mtd { get; set; }
        public int quality_activations_percent_ach_LM { get; set; }
        public int quality_activations_lm { get; set; }
        public int total_activations_lm { get; set; }
        public int quality_activation_tgt { get; set; }
        public int quality_activation_percent_ach_lm { get; set; }
        public int qulity_activation_tgt_vs_ach { get; set; }
        public int quality_acitvation_fse_on_tgt_vs_ach { get; set; }
        public int quality_acitvation_fse_on_LM_vs_mtd { get; set; }
        public int mamo_tert_percent_ach_LM { get; set; }
        public int mamo_tert_current_month { get; set; }
        public int mamo_tert_tgt { get; set; }
        public int mamo_tert_percent_ach_current_month { get; set; }
        public int percent_mamo_tert_tgt_vs_ach { get; set; }
        public int fse_on_mamo_tert_tgt_vs_ach { get; set; }
        public int fse_on_mamo_ter_lm_vs_mtd { get; set; }
        public int total_score { get; set; }
        public string qualified { get; set; }
        public int incentive
        {
            get;
            set;
        }
    }
}
