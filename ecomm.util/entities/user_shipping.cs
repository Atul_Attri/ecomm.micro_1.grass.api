﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.IO;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
    public class user_shipping
    {
        public int id { get; set; }
        public string user_id { get; set; }
        public string shipping_name { get; set; }
        public string pincode { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string mobile_number { get; set; }
        public string name { get; set; }
        public string store { get; set; }
    }

    public class mongo_user_discount_code
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public double id { get; set; }
        public string user_id { get; set; }
        public string discount_code { get; set; }
    }

    public class mongo_discount_codes
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string discount_code { get; set; }
        public string content_url { get; set; }
        public mongo_discount_rule rule { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string create_ts { get; set; }
        public string modify_ts { get; set; }
        public string create_by { get; set; }
        public string modify_by { get; set; }
        public string store { get; set; }
    }

    public class mongo_discount_rule
    {
        public string discount_type { get; set; }
        public string discount_value { get; set; }
        public double min_value { get; set; }
        public double max_discount { get; set; }
    }

    public class mongo_user_points
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string user_id { get; set; }
        public double txn_amount { get; set; }
        public string txn_type { get; set; }
        public DateTime txn_ts { get; set; }
        public string txn_comment { get; set; }
        public double txn_status { get; set; }
        public double validity { get; set; }
        public DateTime? expiry_date { get; set; }
        public string program_type { get; set; }
        public string store { get; set; }
    }



    public class mongo_user
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string office_no { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string password { get; set; }
        public double status { get; set; }
        public DateTime registration_date { get; set; }
        public string user_type { get; set; }
        public string company { get; set; }
        public string location { get; set; }
        public string address { get; set; }
        public string email_address { get; set; }
        public DateTime dob { get; set; }
        public DateTime doj { get; set; }
        public string qualification { get; set; }
        public string branch_name { get; set; }
        public string region { get; set; }
        public List<mongo_shipping_address> shipping_data { get; set; }

        public double total_point { get; set; }
        public string token_id { get; set; }
        public string message { get; set; }
        public double max_point_range { get; set; }
        public double min_point_range { get; set; }

    }



    public class mongo_user_info
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string office_no { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string password { get; set; }
        public double status { get; set; }
        public DateTime registration_date { get; set; }
        public string user_type { get; set; }
        public string company { get; set; }
        public string location { get; set; }
        public string address { get; set; }
        public string email_address { get; set; }
        public DateTime dob { get; set; }
        public DateTime doj { get; set; }
        public string qualification { get; set; }
        public string branch_name { get; set; }
        public string region { get; set; }
        public List<mongo_shipping_address> shipping_data { get; set; }



    }



    public class mongo_cart_data
    {
        public string cart_data_id { get; set; }
        public string cart_data { get; set; }
        public string user_id { get; set; }

    }

    public class countercollection
    {
        public string _id { get; set; }
        public string seq { get; set; }
    }

    public class mongo_point_output
    {
        public string txn_type { get; set; }
        public double total_point { get; set; }
        public double total_count { get; set; }
    }

    public class mongo_user_data
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string email_id { get; set; }
        public string mobile_no { get; set; }
        public string office_no { get; set; }
        public string street { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string country { get; set; }
        public string zipcode { get; set; }
        public string status { get; set; }
        public string registration_date { get; set; }
        public string user_type { get; set; }
        public string company { get; set; }
        public string location { get; set; }
        public string address { get; set; }
        public string email_address { get; set; }
        public string dob { get; set; }
        public string doj { get; set; }
        public string qualification { get; set; }
        public string branch_name { get; set; }
        public string region { get; set; }
    }

    public class mongo_shipping_address
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public double id { get; set; }
        public string user_id { get; set; }
        public string shipping_name { get; set; }
        public string pincode { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string mobile_number { get; set; }
        public string name { get; set; }

    }


    public class mongo_company
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public double id { get; set; }
        public string name { get; set; }
        public string displayname { get; set; }
        public string contactname { get; set; }
        public string phonenumber { get; set; }
        public string address { get; set; }
        public string emailid { get; set; }
        public string logo { get; set; }
        public string company_type { get; set; }
        public double point_range_min { get; set; }
        public double point_range_max { get; set; }
        public string root_url_1 { get; set; }
        public string root_url_2 { get; set; }
        public double shipping_charge { get; set; }
        public double min_order_for_free_shipping { get; set; }
        public double sales_tracking { get; set; }
        public string track_cycle { get; set; }
        public double validity { get; set; }
        public double inr_point_ratio { get; set; }

    }

    public class mongo_order
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public double order_id { get; set; }
        public string cart_id { get; set; }
        public mongo_cart_data cart_data { get; set; }
        public string user_id { get; set; }
        public mongo_user_data user_data { get; set; }
        public string discount_code { get; set; }
        public double total_amount { get; set; }
        public double points { get; set; }
        public double paid_amount { get; set; }
        public int status { get; set; }
        public string payment_info { get; set; }
        public string rejection_reason { get; set; }
        public DateTime create_ts { get; set; }
        public DateTime modify_ts { get; set; }
        public DateTime pay_sent_ts { get; set; }
        public DateTime pay_conf_ts { get; set; }
        public string order_display_id { get; set; }
        public string store { get; set; }
        public double shipping_info_id { get; set; }
        public mongo_shipping_address shipping_address { get; set; }
        public double points_amount { get; set; }
        public string egift_vou_no { get; set; }
        public double egift_vou_amt { get; set; }
        public string shipping_info { get; set; }
        public string delayed_note { get; set; }
        public string cancellation_note { get; set; }
        public string courier_track_no { get; set; }
        public string courier_track_link { get; set; }
        public double discount_amount { get; set; }
        public string dispatch_date { get; set; }
        public double courier_cost { get; set; }
        public double shipping_amount { get; set; }
        public DateTime? expiry_datetime { get; set; }
        public string extended_reason { get; set; }
        public dispute dispute_data { get; set; }


        public string selected_size { get; set; }
        public string selected_prod_name { get; set; }
        public string site_version { get; set; }

        public string edm_id { get; set; }

    }

    public class dispute
    {

        public double dispute_id { get; set; }
        public double order_id { get; set; }
        public string dispute_notes { get; set; }
        public double dispute_flag { get; set; }
        public string dispute_ts { get; set; }

    }

    public class mongo_ship
    {

        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public double order_id { get; set; }
        public string cart_id { get; set; }
        public mongo_cart_data cart_data { get; set; }
        public string user_id { get; set; }
        public mongo_user_data user_data { get; set; }
        public string discount_code { get; set; }
        public double total_amount { get; set; }
        public double points { get; set; }
        public double paid_amount { get; set; }
        public int status { get; set; }
        public string payment_info { get; set; }
        public string rejection_reason { get; set; }
        public DateTime create_ts { get; set; }
        public DateTime modify_ts { get; set; }
        public DateTime pay_sent_ts { get; set; }
        public DateTime pay_conf_ts { get; set; }
        public string order_display_id { get; set; }
        public string store { get; set; }
        public double shipping_info_id { get; set; }
        public mongo_shipping_address shipping_address { get; set; }
        public List<mongo_shipping_address> user_shipping_list { get; set; }
        public string points_amount { get; set; }
        public string egift_vou_no { get; set; }
        public string egift_vou_amt { get; set; }
        public string shipping_info { get; set; }
        public string delayed_note { get; set; }
        public string cancellation_note { get; set; }
        public string courier_track_no { get; set; }
        public string courier_track_link { get; set; }
        public string discount_amount { get; set; }
        public DateTime? dispatch_date { get; set; }
        public string courier_cost { get; set; }
        public string shipping_amount { get; set; }
        public DateTime? expiry_datetime { get; set; }
        public string extended_reason { get; set; }

        /***************** For Old Collection ******************/

        public double id { get; set; }
        public string shipping_name { get; set; }
        public string pincode { get; set; }
        public string address { get; set; }
        public string state { get; set; }
        public string city { get; set; }
        public string mobile_number { get; set; }
        public string name { get; set; }

    }


    public class mongo_user_security
    {

        public double Id { get; set; }
        public string user_name { get; set; }
        public double company_id { get; set; }
        public string password { get; set; }
        public string user_type { get; set; }
        public string message { get; set; }


    }

    public class mongo_user_rights
    {

        public Int64 user_right_id { get; set; }
        public string user_right_desc { get; set; }
        public long USER_ID { get; set; }
    }

    public class mongo_user_assign_rigths
    {
        public List<long> RIGHTS_IDS { get; set; }
        public long USER_ID { get; set; }
        public string created_by { get; set; }


    }




    public class grass_micro_order
    {

        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
        public string designation { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string zipcode { get; set; }
        public grass_micro_product selected_product { get; set; }
        public string selected_size { get; set; }
        public string sel_product_string { get; set; }
        public string site_version { get; set; }
        public string edm_id { get; set; }


    }
    public class grass_camp_order
    {

        public long id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string mobile { get; set; }
        public string landline { get; set; }
        public string city { get; set; }
        public string company_name { get; set; }
        public string company_email { get; set; }
        public string designation { get; set; }
        public string address { get; set; }
        public string pincode { get; set; }
        public string product_name { get; set; }
        public string product_id { get; set; }
        public string product_qty { get; set; }
        public string comments { get; set; }
        public DateTime created_ts { get; set; }
        public grass_micro_product selected_product { get; set; }
        public string selected_size { get; set; }
        public string sel_product_string { get; set; }
        public string site_version { get; set; }
        public string edm_id { get; set; }


    }


    public class grass_micro_client
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string company { get; set; }
        public string designation { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address_line1 { get; set; }
        public string address_line2 { get; set; }
        public string zipcode { get; set; }
        public string site_version { get; set; }
        public string edm_id { get; set; }
        public DateTime create_ts { get; set; }

    }




    public class grass_micro_product
    {
        public string id { get; set; }
        public string sku { get; set; }
        public string Name { get; set; }
        public string brand { get; set; }
        public string description { get; set; }
        public string shortdesc { get; set; }
        public List<imgurl> image_urls { get; set; }
        public List<String> size { get; set; }

    }


    public class edm_track
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string edm_id { get; set; }
        public string email_id { get; set; }
        public DateTime hitting_time { get; set; }
        
    }

    public class mail_track
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; }
        public string edm_id { get; set; }
        public string email_id { get; set; }
        public DateTime hitting_time { get; set; }

    }



}
