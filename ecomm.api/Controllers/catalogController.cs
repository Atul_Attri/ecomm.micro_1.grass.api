﻿﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AttributeRouting;
using AttributeRouting.Web.Http;
using ecomm.util.entities;
using Newtonsoft.Json;
namespace ecomm.api.Controllers
{
    public class catalogController : ApiController
    {
        [GET("store/ping")]
        public string get_ping()
        {
            return "Store is Up";
        }
        //[GET("store/cart?email={email_id}")]
        //public List<cart_item> get_cart_by_email(string email_id)
        //{
        //    ecomm.model.repository.store_repository s = new model.repository.store_repository();
        //    return s.get_cart_by_email(email_id);
        //}
        [GET("store/menu/expired?last_download_date={last_download_date}")]
        public int get_menu_expiry_status(string last_download_date)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_menu_expiry_status(last_download_date);
        }
        [GET("store/menu/{company}")]
        public List<menu> get_menu(string company = "")
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_menu(company);
        }
        [GET("store/level1menu")]
        public List<menu_item> get_level1_menu()
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_level1_menu();
        }


        [GET("store/cat_list")]
        public string[] get_cat_list()
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_cat_list();
        }

        [GET("store/cat_filters?cat_id={cat_id}")]
        public string[] get_cat_list_filters(string cat_id)
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_cat_list_filters(cat_id);
        }
        [GET("store/premium_items_filters?prem={prem}")]
        public List<filter> get_premium_items_filters(int prem)
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_premium_items_filters(prem);
        }

        [GET("store/newarrivals_items_filters")]
        public List<filter> get_newarrivals_items_filters()
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_newarrivals_items_filters();
        }

        [GET("store/express_items_filters")]
        public List<filter> get_express_items_filters()
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_express_items_filters();
        }


        [GET("store/special_item_filters_parent?cat_id={cat_id}")]
        public List<filter> get_special_item_filters_parent(string cat_id)
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_special_item_filters_parent(cat_id);
        }
        [GET("store/special_item_filters_specific?cat_id={cat_id}")]
        public List<filter> get_special_item_filters_specific(string cat_id)
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_special_item_filters_specific(cat_id);
        }
        [GET("store/active_cat_list")]
        public List<catagory> get_active_cat_list()
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_active_cat_list();
        }
        [GET("store/cat_list_short")]
        public List<cat> get_cat_list_short()
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_cat_list_short();
        }
        [GET("store/active_cat_list_short")]
        public List<cat> get_active_cat_list_short()
        {
            //List<catalog> cats = new List<catalog>();
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_active_cat_list_short();
        }

        [GET("store/cat_details?cat_id={cat_id}")]
        public string[] get_cat_details(string cat_id)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_cat_details(cat_id);
        }
        [HttpPost]
        [POST("store/prod_list_by_qs/")]
        public string[] post_prod_list_by_qs(dynamic data)
        {

            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.post_prod_list_by_qs(Convert.ToString(data));

        }

        [GET("store/prod_list_by_cat_short?cat_id={cat_id}, {min}, {max}")]
        public string[] get_prod_list_by_cat_short(string cat_id, int? min = 0, int? max = 0)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_prod_list_by_cat_short(cat_id, (int)min, (int)max);
        }

        // not being used now
        [GET("store/prod_list_by_cat_short_sorted?cat_id={cat_id}&asc={asc}&desc={desc}")]
        public string[] get_prod_list_by_cat_short_sorted(string cat_id, string asc, string desc)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            string[] asc_flds, desc_flds;
            asc_flds = null;
            desc_flds = null;
            if (asc != null)
            {
                asc_flds = asc.Split('|');
            }
            if (desc != null)
            {
                desc_flds = desc.Split('|');
            }
            return s.get_prod_list_by_cat_short(cat_id, asc_flds, desc_flds);
        }

        [GET("store/search?srch={srch}&context={context}&min={min}&max={max}")]
        public string[] get_search(string srch, string context, int? min = 0, int? max = 0)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_search_prod_list(srch, context, (int)min, (int)max);
        }
        [GET("store/search_filters?srch={srch}")]
        public List<filter> get_search_filters(string srch)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_search_prod_list_filters(srch);
        }
        [GET("store/premium_prod_list/{prem}?min={min}&max={max}")]
        public string[] get_premium_prod_list(int prem, int? min = 0, int? max = 0)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_premium_prod_list(prem, (int)min, (int)max);
        }

        [GET("store/newarrivals_prod_list?min={min}&max={max}")]
        public string[] get_newarrivals_prod_list(int? min = 0, int? max = 0)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_newarrivals_prod_list((int)min, (int)max);
        }

        [GET("store/express_prod_list?min={min}&max={max}")]
        public string[] get_express_prod_list(int? min = 0, int? max = 0)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_express_prod_list((int)min, (int)max);
        }

        // not used right now
        // *************************************************************
        [GET("store/prod_list_by_brand/{brand}")]
        public string[] get_prod_list_by_brand(string brand)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_prod_list_by_brand(brand);

        }
        [GET("store/prod_list_by_brand_sorted/{brand}?asc={asc}&desc={desc}")]
        public string[] get_prod_list_by_brand_sorted(string brand, string asc, string desc)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            string[] asc_flds, desc_flds;
            asc_flds = null;
            desc_flds = null;
            if (asc != null)
            {
                asc_flds = asc.Split('|');
            }
            if (desc != null)
            {
                desc_flds = desc.Split('|');
            }
            return s.get_prod_list_by_brand(brand, asc_flds, desc_flds);

        }
        [GET("store/categories_by_brand/{brand}")]
        public string[] get_categories_by_brand(string brand)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_categories_by_brand(brand);

        }
        [GET("store/breadcrumb?type={type}&value={value}")]
        public string[] get_breadcrumb(string type, string value)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_breadcrumb(type, value);

        }
        [GET("store/prod_list_by_cat/{cat_id}")]
        public List<product> get_prod_list_by_cat(string cat_id)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_prod_list_by_cat(cat_id);

        }
        //[GET("store/prod_list_by_cat/{cat_id}?asc={asc}&desc={desc}")]
        //public List<product> get_prod_list_by_cat(string cat_id, string asc, string desc)
        //{
        //    ecomm.model.repository.store_repository s = new model.repository.store_repository();
        //    string[] asc_flds, desc_flds;
        //    asc_flds = null;
        //    desc_flds = null;
        //    if (asc != null)
        //    {
        //        asc_flds = asc.Split('|');
        //    }
        //    if (desc != null)
        //    {
        //        desc_flds = desc.Split('|');
        //    }
        //    return s.get_prod_list_by_cat(cat_id, asc_flds, desc_flds);
        //    //return s.get_prod_list_by_cat(cat_id);

        //}
        // *************************************************************
        [GET("store/prod_details?prod_id={prod_id}")]
        public string[] get_prod_details(string prod_id)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_prod_details(prod_id);
        }

        [GET("store/shipping_details?cat_id={cat_id}")]
        public string[] get_shipping_details(string cat_id)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_shipping_details(cat_id);
        }

        [GET("store/child_products?prod_id={prod_id}")]
        public string[] get_child_products(string prod_id)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_child_products(prod_id).ToArray();
        }
        //[GET("store/user_shipping?email={email}")]
        //public List<user_shipping> get_user_shipping_info(string email)
        //{
        //    ecomm.model.repository.store_repository s = new model.repository.store_repository();
        //    return s.get_user_shipping_info(email);
        //}

        [GET("store/user_shipping?email={email}&store={store}")]
        public List<mongo_shipping_address> get_user_shipping_info(string email, string store)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_user_shipping_info(email, store);
        }

        //[GET("store/get_user_shipping_info?u={email_id}")]
        //public List<user_shipping> get_user_shipping_info(string email_id)
        //{
        //    ecomm.model.repository.store_repository s = new model.repository.store_repository();
        //    return s.get_user_shipping(email_id);
        //}

        [GET("store/cart?email={email_id}&store={store}")]
        public List<cart_item> get_cart_by_email(string email_id, string store)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_cart_by_email(email_id, store);
        }


        //Added By Hassan To Get The Wish List

        [GET("store/wishlist?email={email_id}")]
        public List<cart_item> get_wishlist_by_email(string email_id)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_wishlist_by_email(email_id);
        }


        // Added By Dipanjan To Get Product By Brand
        [GET("store/get_prod_by_brand?brand_name={brand_name}")]
        public string[] get_prod_by_brand(string brand_name)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_prod_by_brand(brand_name);
        }



        [GET("store/points_credit?email={email}")]
        public List<point_details> get_user_points_credit(string email)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_user_points_credit(email);
        }
        [GET("store/points_debit?email={email}")]
        public List<point_details> get_user_points_debit(string email)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_user_points_debit(email);
        }

        [GET("store/get_points_against_user?company={company}&user_id={user_id}")]
        public long get_points_against_user(string company, string user_id)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_points_against_user(company, user_id);

        }



        [GET("store/coupon/validate?coupon={coupon}&store={store}&user={user}")]
        public discount_coupon get_validate_discount_coupon(string coupon, string store, string user)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.validate_discount_code(coupon, store, user);
        }

        [GET("store/order_details?email={email}")]
        public List<order> get_user_order_details(string email)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_user_order_details(email);
        }

        [GET("store/validate_cart?email={email}")]
        public user_cart get_validate_cart(string email)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.validate_cart(email);
        }


        //[GET("store/user_shipping?u={user_id}")]
        //public List<user_shipping> get_user_shipping(string user_id)
        //{
        //    ecomm.model.repository.store_repository s = new model.repository.store_repository();
        //    return s.get_user_shipping(user_id);
        //}



        [GET("store/prod_list/range?min={min}&max={max}")]
        public string[] get_prod_list_by_range(long? min = 0, long? max = 0)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_prod_list_by_range((long)min, (long)max);
        }


        [POST("store/admin/insert_catagory/")]
        public string insert_catagory(catagory cat)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.insert_catagory(cat);

        }

        [POST("store/cart/write_cart/")]
        public string write_cart(user_cart cart)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.write_cart(cart);
        }

        [POST("store/cart/add_cart/")]
        public string add_cart(bulk_cart bc)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.add_cart(bc);
        }


        // Added By Hassan To Write Wish List

        [POST("store/wishlist/write_wishlist/")]
        public string write_wishlist(user_cart cart)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.write_wishlist(cart);
        }


        [POST("store/cart/reprice_cart/")]
        public user_cart reprice_cart(user_cart cart)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.reprice_cart(cart);
        }

        //[POST("store/checkout/write_ship_data/")]
        //public int write_ship_data(user_shipping data)
        //{
        //    ecomm.model.repository.store_repository sr = new model.repository.store_repository();
        //    return sr.write_ship_data(data);
        //}


        [POST("store/checkout/write_ship_data/")]
        public int write_ship_data(mongo_ship data)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.write_ship_data(data);
        }




        [POST("/come/from/edm/track/")]
        public string come_from_edm_track(edm_track oet)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.come_from_edm_track(oet);

        }

        [POST("/come/from/mail/track/")]               //atul 20/6/2015
        public string come_from_mail_track(mail_track oet)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.come_from_mail_track(oet);

        }



        [POST("grass/micro/add/client/data")]
        public string add_user_details(grass_micro_client gmc)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();

            return sr.add_user_details(gmc);

        }

        [POST("grass/micro/place_order/")]
        public int place_order(grass_camp_order gmo)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            // return sr.place_order(gmo);

            return sr.place_order_restrict(gmo);

        }


       // [POST("grass/micro/place_order/restrict")]
       // public int place_order_restrict(grass_micro_order gmo)
       // {
       //     ecomm.model.repository.store_repository sr = new model.repository.store_repository();
       ////     return sr.place_order_restrict(gmo);

       // }



        [POST("grass/micro/feedback/")]
        public string grass_micro_feedback(grass_micro_feedback ogmf)
        {
            ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
            return cr.micro_feedback(ogmf);

        }


        [GET("grass/micro/order/list?email={email}&store={store}")]
        public List<mongo_order> get_micro_order_list(string email, string store)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.get_micro_order_list(email, store);

        }


        [GET("grass/micro/order/send_will_ship_email?email={email}&store={store}")]
        public List<mongo_order> send_will_ship_email(string email, string store)
        {
            ecomm.model.repository.store_repository s = new model.repository.store_repository();
            return s.send_will_ship_email(email, store);
        }


        [POST("/grass/micro/update/order/")]
        public string micro_update_order(ordadmin o)
        {
            ecomm.model.repository.checkoutrepository cr = new ecomm.model.repository.checkoutrepository();
            return cr.micro_update_order(o);

        }






        [GET("store/checkout/add_express/{order_id}/{express_shipping_amount}")] // todo to be convertedto POST Method
        public int get_add_express(int order_id, int express_shipping_amount)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            return sr.add_express_shipping(order_id, express_shipping_amount);
        }

        [POST("/audit/prod_view/insert")]
        public void post_prod_view_insert(prod_view pv)
        {
            ecomm.model.repository.store_repository sr = new model.repository.store_repository();
            sr.prod_view_insert(pv);
        }
    }

}
