﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace ecomm.util.entities
{
   public class store_config
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string _id { get; set; } 
        public string store { get; set; }
        public List<Exclu> exclusion { get; set; }
        public List<Disc> discount { get; set; }
        
    }
}
