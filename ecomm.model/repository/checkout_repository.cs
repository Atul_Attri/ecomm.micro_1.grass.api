﻿using ecomm.dal;
using ecomm.util.entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using Newtonsoft.Json;
using System.Configuration;
using System.Security.Cryptography;
using ecomm.util;

using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

//using System.Web.UI.WebControls;

namespace ecomm.model.repository
{
    public class checkoutrepository
    {

        private bool check_field(BsonDocument doc, string field_name)
        {
            if (doc.Contains(field_name))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private DateTime convertToDate(object o)
        {
            try
            {
                if ((o != null) && (o != ""))
                {
                    //string f = "mm/dd/yyyy";
                    return DateTime.Parse(o.ToString());
                    //return DateTime.ParseExact(o.ToString(), "MM-dd-yy", System.Globalization.CultureInfo.InvariantCulture);
                }
                else { return DateTime.Parse("1/1/1800"); }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return DateTime.Parse("1/1/1800");
            }
        }
        private int convertToInt(object o)
        {
            try
            {
                if (o != null)
                {
                    return Int32.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private decimal convertToDecimal(object o)
        {
            try
            {
                if (o != null)
                {
                    return decimal.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }
        private long convertToLong(object o)
        {
            try
            {
                if (o != null)
                {
                    return long.Parse(o.ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private double convertToDouble(object o)
        {
            try
            {
                if (o != null && o.ToString() != "")
                {
                    return double.Parse(Math.Round(decimal.Parse(o.ToString()), 3).ToString());
                }
                else { return 0; }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return 0;
            }
        }

        private string convertToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else
                {
                    return "";
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private string ConvToString(object o)
        {
            try
            {
                if ((o != null) && (o.ToString() != ""))
                {
                    return o.ToString().Trim();
                }
                else if (o == null)
                {
                    return null;
                }
                else if (o.ToString().Trim() == "")
                {
                    return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return null;
            }
        }

        public string AddCartInfo(List<ecomm.util.entities.cart> crtlist)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = Guid.NewGuid().ToString();
            int i = 0;
            try
            {
                foreach (ecomm.util.entities.cart crt in crtlist)
                {
                    i = da.ExecuteSP("cart_items_Insert", ref oc,
                        da.Parameter("_cart_id", guid)
                        , da.Parameter("_company_id", 111) // To be removed
                        , da.Parameter("_user_id", "annectos") // To be removed
                        , da.Parameter("_status", 1)
                        , da.Parameter("_token_id", "") // To be removed
                        , da.Parameter("_product_id", crt.id)
                        , da.Parameter("_quantity", crt.quantity)
                        , da.Parameter("_unit_price", crt.mrp)
                        , da.Parameter("_mrp", crt.mrp)
                        , da.Parameter("_list_price", crt.final_offer)
                        , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                        , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                        );
                }
                if (i == 1)
                {
                    return guid.ToString();
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }


        /// <summary>
        /// Add Shipping Information
        /// </summary>
        /// <param name="ship"></param>
        /// <returns></returns>
        public string AddShippingInfo(ecomm.util.entities.shipping ship)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string cart_id = Guid.NewGuid().ToString();
            int i = 0;
            try
            {
                i = da.ExecuteSP("shipping_info_Insert", ref oc,
                    da.Parameter("_cart_id", cart_id)
                    , da.Parameter("_order_id", ship.order_id)
                    , da.Parameter("_user_id", "annectos")
                    , da.Parameter("_shipping_name", ship.name)
                    , da.Parameter("_pincode", ship.pincode)
                    , da.Parameter("_address", ship.address)
                    , da.Parameter("_state", ship.state)
                    , da.Parameter("_city", ship.city)
                    , da.Parameter("_country", ship.country)
                    , da.Parameter("_landmark", ship.landmark)
                    , da.Parameter("_mobile_number", ship.mobile_number)
                    );

                if (i == 1)
                {
                    return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        /// <summary>
        /// Add Order Booking Information
        /// </summary>
        /// <param name="ord"></param>
        /// <returns></returns>
        public string AddOrderBooking(ecomm.util.entities.order ord)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            int i = 0;
            try
            {
                i = da.ExecuteSP("order_insert", ref oc,
                    da.Parameter("_order_id", ord.order_id)
                    , da.Parameter("_cart_id", ord.cart_id)
                    , da.Parameter("_user_id", ord.user_id)
                    , da.Parameter("_discount_code", ord.discount_code)
                    , da.Parameter("_total_amount", ord.total_amount)
                    , da.Parameter("_points", ord.points)
                    , da.Parameter("_paid_amount", ord.paid_amount)
                    , da.Parameter("_status", ord.status)
                    , da.Parameter("_payment_info", ord.payment_info)
                    , da.Parameter("_rejection_reason", ord.rejection_reason)
                    , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_pay_sent_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_pay_conf_ts", ord.pay_conf_ts)
                    );

                if (i == 1)
                {
                    return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        public MongoCursor getOrderDetails(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {
            MongoCursor cursor = null;
            try
            {
                dal.DataAccess dal = new DataAccess();
                string mongo_order_query = "";
                if (!OrderStore.Equals("null"))
                {
                    mongo_order_query = "store:'" + OrderStore + "'";
                }
                if (OrderType != -1)
                {
                    if (mongo_order_query.Length > 0)
                    {
                        mongo_order_query = mongo_order_query + ",";
                    }
                    mongo_order_query = mongo_order_query + "status:" + OrderType + "";
                }
                if (!OrderToDt.Equals("null") && !OrderFrmDt.Equals("null"))
                {
                    if (mongo_order_query.Length > 0)
                    {
                        mongo_order_query = mongo_order_query + ",";
                    }
                    mongo_order_query = mongo_order_query + "create_ts: { $gte: " + "ISODate( '" + convertToDate(OrderFrmDt).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "' )" + ",$lte: " + "ISODate( '" + convertToDate(OrderToDt).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "' )" + " } ";
                }
                else if (!OrderFrmDt.Equals("null") && OrderToDt.Equals("null"))
                {
                    if (mongo_order_query.Length > 0)
                    {
                        mongo_order_query = mongo_order_query + ",";
                    }
                    mongo_order_query = "create_ts: { $gte: " + "ISODate( '" + convertToDate(OrderFrmDt).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "' )" + " }";
                }
                else if (OrderFrmDt.Equals("null") && !OrderToDt.Equals("null"))
                {
                    if (mongo_order_query.Length > 0)
                    {
                        mongo_order_query = mongo_order_query + ",";
                    }
                    mongo_order_query = "create_ts: { $lte: " + "ISODate( '" + convertToDate(OrderToDt).ToString("yyyy-MM-ddTHH:mm:ss.fffZ") + "' )" + " } ";
                }

                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{" + mongo_order_query + "}");
                QueryDocument queryDoc = new QueryDocument(document);
                cursor = dal.execute_mongo_db("order", queryDoc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cursor;
        }

        public List<ordadmin> getOrderHistory(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {

            //DataAccess da = new DataAccess();
            login_msg return_msg = new login_msg();
            List<ordadmin> ol = new List<ordadmin>();
            //DataTable dtResult = new DataTable();

            try
            {
                MongoCursor cursor = getOrderDetails(OrderStore, OrderType, OrderFrmDt, OrderToDt);

                foreach (var c in cursor)
                {
                    ordadmin ordr = new ordadmin();
                    ordr.order_id = check_field(c.ToBsonDocument(), "order_id") ? convertToLong(c.ToBsonDocument()["order_id"]) : 0;
                    ordr.order_display_id = check_field(c.ToBsonDocument(), "order_display_id") ? convertToString(c.ToBsonDocument()["order_display_id"]) : null;
                    ordr.total_amount = check_field(c.ToBsonDocument(), "total_amount") ? convertToDecimal(c.ToBsonDocument()["total_amount"]) : 0;
                    ordr.store = check_field(c.ToBsonDocument(), "store") ? convertToString(c.ToBsonDocument()["store"]) : null;
                    int ord_stat = check_field(c.ToBsonDocument(), "status") ? convertToInt(c.ToBsonDocument()["status"]) : 0;
                    if (ord_stat == 1)
                    {
                        ordr.Order_Status = "Pending";
                    }
                    else if (ord_stat == 2)
                    {
                        ordr.Order_Status = "Payment Successful";
                    }
                    else if (ord_stat == 3)
                    {
                        ordr.Order_Status = "Payment Failed";
                    }
                    else if (ord_stat == 4)
                    {
                        ordr.Order_Status = "Will Ship";
                    }
                    else if (ord_stat == 5)
                    {
                        ordr.Order_Status = "Shipped";
                    }
                    else if (ord_stat == 6)
                    {
                        ordr.Order_Status = "Order Delayed";
                    }
                    else if (ord_stat == 7)
                    {
                        ordr.Order_Status = "Order Cancelled";
                    }
                    else if (ord_stat == 13)
                    {
                        ordr.Order_Status = "Order Disputed";
                    }
                    else
                    {
                        ordr.Order_Status = "";
                    }

                    //var user_data = check_field(c.ToBsonDocument(), "user_data") ? JsonConvert.DeserializeObject<mongo_user_data>(c.ToBsonDocument()["user_data"].ToString()) : null;
                    //var user_shipping_data = check_field(c.ToBsonDocument(), "shipping_address") ? JsonConvert.DeserializeObject<mongo_shipping_address>(c.ToBsonDocument()["shipping_address"].ToString()) : null;

                    ordr.BillToName = convertToString(c.ToBsonDocument()["user_data"]["first_name"]) + " " + convertToString(c.ToBsonDocument()["user_data"]["last_name"]);
                    ordr.ShipToName = convertToString(c.ToBsonDocument()["shipping_address"]["name"]);
                    ordr.order_date = check_field(c.ToBsonDocument(), "create_ts") ? convertToString(c.ToBsonDocument()["create_ts"]) : System.DateTime.Now.ToLongDateString();


                    ol.Add(ordr);

                }
                /*string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("populate_orders_details"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_order_frm_date", OrderFrmDt)
                                , da.Parameter("_order_to_date", OrderToDt)
                                );




                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        ordadmin ordr = new ordadmin();
                        ordr.order_id = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());
                        ordr.order_display_id = dtResult.Rows[i]["order_display_id"].ToString();
                        ordr.total_amount = convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        ordr.store = dtResult.Rows[i]["store"].ToString();
                        ordr.Order_Status = dtResult.Rows[i]["Order_Status"].ToString();
                        ordr.BillToName = dtResult.Rows[i]["BillToName"].ToString();
                        ordr.ShipToName = dtResult.Rows[i]["ShipToName"].ToString();
                        ordr.order_date = dtResult.Rows[i]["order_date"].ToString();


                        ol.Add(ordr);
                    }
                }*/
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ol;

        }

        public string Order_Export_To_Excel(string OrderStore, int OrderType, string OrderFrmDt, string OrderToDt)
        {

            //DataAccess da = new DataAccess();
            //DataTable dtResult = new DataTable();
            string FileName = Guid.NewGuid().ToString();
            try
            {
                /*
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("populate_orders_details"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_order_frm_date", OrderFrmDt)
                                , da.Parameter("_order_to_date", OrderToDt)
                                );*/
                MongoCursor cursor = getOrderDetails(OrderStore, OrderType, OrderFrmDt, OrderToDt);
                if (cursor != null && cursor.Size() > 0)
                {
                    string strFileName = "";
                    strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
                    sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                    foreach (var c in cursor)
                    {
                        string strLine = "";
                        var Order_Status = "";
                        int ord_stat = check_field(c.ToBsonDocument(), "status") ? convertToInt(c.ToBsonDocument()["status"]) : 0;
                        if (ord_stat == 1)
                        {
                            Order_Status = "Pending";
                        }
                        else if (ord_stat == 2)
                        {
                            Order_Status = "Payment Successful";
                        }
                        else if (ord_stat == 3)
                        {
                            Order_Status = "Payment Failed";
                        }
                        else if (ord_stat == 4)
                        {
                            Order_Status = "Will Ship";
                        }
                        else if (ord_stat == 5)
                        {
                            Order_Status = "Shipped";
                        }
                        else if (ord_stat == 6)
                        {
                            Order_Status = "Order Delayed";
                        }
                        else if (ord_stat == 7)
                        {
                            Order_Status = "Order Cancelled";
                        }
                        else if (ord_stat == 13)
                        {
                            Order_Status = "Order Disputed";
                        }
                        else
                        {
                            Order_Status = "";
                        }

                        strLine = Convert.ToInt64(convertToString(c.ToBsonDocument()["order_id"])) + "," + convertToString(c.ToBsonDocument()["store"]) + ","
                            + convertToDate(c.ToBsonDocument()["order_date"]).ToShortDateString() + "," + convertToString(c.ToBsonDocument()["user_data"]["first_name"])
                            + " " + convertToString(c.ToBsonDocument()["user_data"]["last_name"]) + ","
                            + Order_Status + "," + convertToDecimal(c.ToBsonDocument()["total_amount"]);
                        sw.WriteLine(strLine);
                    }
                    sw.Close();
                }
                /*if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    string strFileName = "";
                    strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
                    sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string strLine = "";
                        strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        sw.WriteLine(strLine);
                    }
                    sw.Close();
                }*/
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return FileName;

        }

        public MongoCursor getOrderRecord_Track(string OrderStore, int OrderType)
        {
            MongoCursor cursor = null;
            try
            {
                dal.DataAccess dal = new DataAccess();
                string mongo_order_query = "";
                if (!OrderStore.Equals("null"))
                {
                    mongo_order_query = "store:'" + OrderStore + "'";
                }
                if (OrderType != -1)
                {
                    if (mongo_order_query.Length > 0)
                    {
                        mongo_order_query = mongo_order_query + ",";
                    }
                    mongo_order_query = mongo_order_query + "status:" + OrderType + "";
                }


                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{" + mongo_order_query + "}");
                QueryDocument queryDoc = new QueryDocument(document);
                cursor = dal.execute_mongo_db("order", queryDoc);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cursor;
        }

        public List<ordadmin> getOrderTrackRecord(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            string FileName = Guid.NewGuid().ToString();
            try
            {



                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        ordadmin ordr = new ordadmin();
                        ordr.order_id = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());
                        ordr.order_display_id = dtResult.Rows[i]["order_display_id"].ToString();
                        ordr.total_amount = convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        ordr.store = dtResult.Rows[i]["store"].ToString();
                        ordr.Order_Status = dtResult.Rows[i]["Order_Status"].ToString();
                        ordr.BillToName = dtResult.Rows[i]["BillToName"].ToString();
                        ordr.ShipToName = dtResult.Rows[i]["ShipToName"].ToString();
                        ordr.order_date = dtResult.Rows[i]["order_date"].ToString();


                        ol.Add(ordr);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ol;

        }

        public string getSendTrackReport(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            string FileName = Guid.NewGuid().ToString();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );

                string toemail = "";
                string emailsubject = "";
                string strStoreName = "annectos";

                if (OrderStore == null || OrderStore == "null")
                {
                    emailsubject = "Order Track Report For All Store";
                }
                else
                {
                    emailsubject = "Order Track Report For : " + OrderStore;
                }

                if (dtResult != null && dtResult.Rows.Count > 0)
                {

                    string strFileName = "";
                    strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                    System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                    sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string strLine = "";
                        strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                        sw.WriteLine(strLine);
                    }
                    sw.Close();


                    helper_repository hr = new helper_repository();
                    toemail = "sharvani@annectos.in";
                    string cclist = System.Configuration.ConfigurationSettings.AppSettings["bcc_email"].ToString();
                    string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();


                    payment_repository pr = new payment_repository();
                    string strMsgBody = pr.GetOrderTrackBody(strStoreName);

                    hr.SendMailWithAttachment(toemail, cclist, bcclist, emailsubject, strMsgBody, strFileName);

                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return "Report Send";

        }


        public List<ordadmin> getWillShipTrackReport(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();

            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            DataTable dtReport = new DataTable();
            payment_repository pr = new payment_repository();

            string GenerateOrderIds = "";

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.min_ship = check_field(c.ToBsonDocument(), "min_ship") ? convertToInt(c.ToBsonDocument()["min_ship"]) : 0;
                        cat.max_ship = check_field(c.ToBsonDocument(), "max_ship") ? convertToInt(c.ToBsonDocument()["max_ship"]) : 0;
                        Category_List.Add(cat);
                    }

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        //DataTable dtOrdDtls = new DataTable();
                        int min_ship_days = 0;
                        int max_ship_days = 0;

                        string OrdWiseStoreName = convertToString(dtResult.Rows[i]["store"]);
                        long OrderID = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());

                        List<mongo_order> dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = convertToString(dtOrdDtls[j].cart_data);
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());



                                foreach (cart_item ord in ordlist)
                                {
                                    string Product_ID = "";

                                    var Prod_Id = ord.id.ToString().Split('.');

                                    Product_ID = Prod_Id[0].ToString();

                                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                    QueryDocument queryDoc = new QueryDocument(document);
                                    MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                    foreach (var c1 in cursor1)
                                    {
                                        string Category_Id = "";
                                        Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                        if (Category_Id != "" && Category_Id != "0")
                                        {

                                            for (var m = 0; m < Category_List.Count(); m++)
                                            {

                                                if (Category_List[m].id.ToString() == Category_Id)
                                                {
                                                    int Temp_Min_Ship = 0;
                                                    int Temp_Max_Ship = 0;

                                                    Temp_Min_Ship = convertToInt(Category_List[m].min_ship);
                                                    Temp_Max_Ship = convertToInt(Category_List[m].max_ship);

                                                    if (min_ship_days == 0)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    else if (Temp_Min_Ship < min_ship_days)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    if (max_ship_days == 0)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                    else if (Temp_Max_Ship < max_ship_days)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        List<OutCollection> oc = new List<OutCollection>();
                        oc = GetOrderWillShipRep(OrderID, OrdWiseStoreName, OrderType, min_ship_days);
                        if (oc[0].strParamValue.ToString() == "Order Status Needs To Change")
                        {

                            if (GenerateOrderIds == "")
                            {
                                GenerateOrderIds = OrderID.ToString();
                            }
                            else
                            {
                                GenerateOrderIds = GenerateOrderIds + "," + OrderID.ToString();
                            }
                        }
                    }


                    GenerateOrderIds = "(" + GenerateOrderIds + ")";


                    string MysqlQuery = "";

                    if (OrderStore == null || OrderStore == "null")
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where  a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";

                    }
                    else
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where a.store='" + OrderStore + "' and a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";

                    }
                    dtReport = da.ExecuteSQL(MysqlQuery);

                    if (dtReport != null && dtReport.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtReport.Rows.Count; i++)
                        {
                            ordadmin ordr = new ordadmin();
                            ordr.order_id = Convert.ToInt64(dtReport.Rows[i]["order_id"].ToString());
                            ordr.order_display_id = dtReport.Rows[i]["order_display_id"].ToString();
                            ordr.total_amount = convertToDecimal(dtReport.Rows[i]["total_amount"].ToString());
                            ordr.store = dtReport.Rows[i]["store"].ToString();
                            ordr.Order_Status = dtReport.Rows[i]["Order_Status"].ToString();
                            ordr.BillToName = dtReport.Rows[i]["BillToName"].ToString();
                            ordr.ShipToName = dtReport.Rows[i]["ShipToName"].ToString();
                            ordr.order_date = dtReport.Rows[i]["order_date"].ToString();
                            ol.Add(ordr);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ol;

        }


        public string getWillShipReport(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();

            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            DataTable dtReport = new DataTable();
            payment_repository pr = new payment_repository();

            string GenerateOrderIds = "";

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.min_ship = check_field(c.ToBsonDocument(), "min_ship") ? convertToInt(c.ToBsonDocument()["min_ship"]) : 0;
                        cat.max_ship = check_field(c.ToBsonDocument(), "max_ship") ? convertToInt(c.ToBsonDocument()["max_ship"]) : 0;
                        Category_List.Add(cat);
                    }

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        //DataTable dtOrdDtls = new DataTable();
                        int min_ship_days = 0;
                        int max_ship_days = 0;

                        string OrdWiseStoreName = convertToString(dtResult.Rows[i]["store"]);
                        long OrderID = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());

                        List<mongo_order> dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = convertToString(dtOrdDtls[j].cart_data);
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());



                                foreach (cart_item ord in ordlist)
                                {
                                    string Product_ID = "";

                                    var Prod_Id = ord.id.ToString().Split('.');

                                    Product_ID = Prod_Id[0].ToString();

                                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                    QueryDocument queryDoc = new QueryDocument(document);
                                    MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                    foreach (var c1 in cursor1)
                                    {
                                        string Category_Id = "";
                                        Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                        if (Category_Id != "" && Category_Id != "0")
                                        {

                                            for (var m = 0; m < Category_List.Count(); m++)
                                            {

                                                if (Category_List[m].id.ToString() == Category_Id)
                                                {
                                                    int Temp_Min_Ship = 0;
                                                    int Temp_Max_Ship = 0;

                                                    Temp_Min_Ship = convertToInt(Category_List[m].min_ship);
                                                    Temp_Max_Ship = convertToInt(Category_List[m].max_ship);

                                                    if (min_ship_days == 0)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    else if (Temp_Min_Ship < min_ship_days)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    if (max_ship_days == 0)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                    else if (Temp_Max_Ship < max_ship_days)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        List<OutCollection> oc = new List<OutCollection>();
                        oc = GetOrderWillShipRep(OrderID, OrdWiseStoreName, OrderType, min_ship_days);
                        if (oc[0].strParamValue.ToString() == "Order Status Needs To Change")
                        {

                            if (GenerateOrderIds == "")
                            {
                                GenerateOrderIds = OrderID.ToString();
                            }
                            else
                            {
                                GenerateOrderIds = GenerateOrderIds + "," + OrderID.ToString();
                            }
                        }
                    }


                    GenerateOrderIds = "(" + GenerateOrderIds + ")";


                    string MysqlQuery = "";
                    string toemail = "";
                    string emailsubject = "";
                    string strStoreName = "annectos";
                    string FileName = Guid.NewGuid().ToString();


                    if (OrderStore == null || OrderStore == "null")
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where  a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Min Ship Days For All Store";
                    }
                    else
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where a.store='" + OrderStore + "' and a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Min Ship Days For : " + OrderStore;
                    }
                    dtReport = da.ExecuteSQL(MysqlQuery);


                    if (dtReport != null && dtReport.Rows.Count > 0)
                    {
                        string strFileName = "";
                        strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                        sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            string strLine = "";
                            strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                            sw.WriteLine(strLine);
                        }
                        sw.Close();


                        helper_repository hr = new helper_repository();
                        toemail = "sharvani@annectos.in";
                        string cclist = System.Configuration.ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();


                        payment_repository prr = new payment_repository();
                        string strMsgBody = prr.GetWillShipOrderTrackBody(strStoreName);

                        hr.SendMailWithAttachment(toemail, cclist, bcclist, emailsubject, strMsgBody, strFileName);

                    }

                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return "Report Send";

        }

        public string getWillShipReportAfterMaxShip(string OrderStore, int OrderType)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();

            List<ordadmin> ol = new List<ordadmin>();
            DataTable dtResult = new DataTable();
            DataTable dtReport = new DataTable();
            payment_repository pr = new payment_repository();

            string GenerateOrderIds = "";

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_order_track_record"
                                , da.Parameter("_order_store", OrderStore)
                                , da.Parameter("_order_type", OrderType)
                                , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                                );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.min_ship = check_field(c.ToBsonDocument(), "min_ship") ? convertToInt(c.ToBsonDocument()["min_ship"]) : 0;
                        cat.max_ship = check_field(c.ToBsonDocument(), "max_ship") ? convertToInt(c.ToBsonDocument()["max_ship"]) : 0;
                        Category_List.Add(cat);
                    }

                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {

                        //DataTable dtOrdDtls = new DataTable();
                        int min_ship_days = 0;
                        int max_ship_days = 0;

                        string OrdWiseStoreName = convertToString(dtResult.Rows[i]["store"]);
                        long OrderID = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString());

                        List<mongo_order> dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = convertToString(dtOrdDtls[j].cart_data);
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());



                                foreach (cart_item ord in ordlist)
                                {
                                    string Product_ID = "";

                                    var Prod_Id = ord.id.ToString().Split('.');

                                    Product_ID = Prod_Id[0].ToString();

                                    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                    QueryDocument queryDoc = new QueryDocument(document);
                                    MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                    foreach (var c1 in cursor1)
                                    {
                                        string Category_Id = "";
                                        Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                        if (Category_Id != "" && Category_Id != "0")
                                        {

                                            for (var m = 0; m < Category_List.Count(); m++)
                                            {

                                                if (Category_List[m].id.ToString() == Category_Id)
                                                {
                                                    int Temp_Min_Ship = 0;
                                                    int Temp_Max_Ship = 0;

                                                    Temp_Min_Ship = convertToInt(Category_List[m].min_ship);
                                                    Temp_Max_Ship = convertToInt(Category_List[m].max_ship);

                                                    if (min_ship_days == 0)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    else if (Temp_Min_Ship < min_ship_days)
                                                    {
                                                        min_ship_days = Temp_Min_Ship;
                                                    }
                                                    if (max_ship_days == 0)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                    else if (Temp_Max_Ship < max_ship_days)
                                                    {
                                                        max_ship_days = Temp_Max_Ship;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }

                            }
                        }

                        List<OutCollection> oc = new List<OutCollection>();
                        oc = GetOrderWillShipRep(OrderID, OrdWiseStoreName, OrderType, max_ship_days);
                        if (oc[0].strParamValue.ToString() == "Order Status Needs To Change")
                        {

                            if (GenerateOrderIds == "")
                            {
                                GenerateOrderIds = OrderID.ToString();
                            }
                            else
                            {
                                GenerateOrderIds = GenerateOrderIds + "," + OrderID.ToString();
                            }
                        }
                    }


                    GenerateOrderIds = "(" + GenerateOrderIds + ")";


                    string MysqlQuery = "";
                    string toemail = "";
                    string emailsubject = "";
                    string strStoreName = "annectos";
                    string FileName = Guid.NewGuid().ToString();


                    if (OrderStore == null || OrderStore == "null")
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where  a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Max Ship Days For All Store";
                    }
                    else
                    {
                        MysqlQuery = "SELECT a.order_id,a.order_display_id,a.store,a.total_amount,a.create_ts as order_date,CONCAT(b.first_name,' ',b.last_name ) AS BillToName,c.name AS ShipToName,'Will Ship'  AS Order_Status FROM  `order` a left outer join `user_registration` b on a.user_id=b.email_id left outer join `user_shipping` c on a.shipping_info_id=c.id where a.store='" + OrderStore + "' and a.status=" + OrderType + " and order_id in " + GenerateOrderIds + "order by a.order_id desc";
                        emailsubject = "Will Ship Order Track Report After Max Ship Days For : " + OrderStore;
                    }
                    dtReport = da.ExecuteSQL(MysqlQuery);


                    if (dtReport != null && dtReport.Rows.Count > 0)
                    {
                        string strFileName = "";
                        strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + FileName + ".csv";
                        System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                        sw.WriteLine("Order #,Store Company,Date,Customer Name,Status,Total Purchase Amount");
                        for (int i = 0; i < dtResult.Rows.Count; i++)
                        {
                            string strLine = "";
                            strLine = Convert.ToInt64(dtResult.Rows[i]["order_id"].ToString()) + "," + dtResult.Rows[i]["store"].ToString() + "," + dtResult.Rows[i]["order_date"].ToString() + "," + dtResult.Rows[i]["BillToName"].ToString() + "," + dtResult.Rows[i]["Order_Status"].ToString() + "," + convertToDecimal(dtResult.Rows[i]["total_amount"].ToString());
                            sw.WriteLine(strLine);
                        }
                        sw.Close();


                        helper_repository hr = new helper_repository();
                        toemail = "sharvani@annectos.in";
                        string cclist = System.Configuration.ConfigurationSettings.AppSettings["bcc_email"].ToString();
                        string bcclist = System.Configuration.ConfigurationSettings.AppSettings["Management"].ToString();


                        payment_repository prr = new payment_repository();
                        string strMsgBody = prr.GetWillShipOrderTrackBody(strStoreName);

                        hr.SendMailWithAttachment(toemail, cclist, bcclist, emailsubject, strMsgBody, strFileName);

                    }

                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return "Report Send";

        }



        public List<OutCollection> GetOrderWillShipRep(Int64 OrderID, string store_name, int orderstatus, int min_shp)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            try
            {
                int i = da.ExecuteSP("get_order_status_report", ref oc
                      , da.Parameter("_order_id", OrderID)
                      , da.Parameter("_order_store", store_name)
                      , da.Parameter("_order_type", orderstatus)
                      , da.Parameter("_current_time", System.DateTime.Now.ToLocalTime())
                      , da.Parameter("_min_ship", min_shp)
                      , da.Parameter("_outmsg", String.Empty, true)
                      );

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");

            }

            return oc;

        }


        public string UserPin(string UserID)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            string User_Pin = "";
            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_pin"
                                , da.Parameter("_user_id", UserID)
                                , da.Parameter("_pin", null)
                                , da.Parameter("_action_flag", 1)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        User_Pin = convertToString(dtResult.Rows[i]["PIN"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return User_Pin;

        }

        public string ChangeUserPin(string UserID)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            string User_Pin = "";
            try
            {
                string[] strout = new string[0];
                User_Pin = GetUniquePin();
                dtResult = da.ExecuteDataTable("get_user_pin"
                                , da.Parameter("_user_id", UserID)
                                , da.Parameter("_pin", User_Pin)
                                , da.Parameter("_action_flag", 2)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        User_Pin = convertToString(dtResult.Rows[i]["PIN"]);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return User_Pin;

        }

        public List<OutCollection> Upload_Cust_points(string email_id, string points)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string FileName = Guid.NewGuid().ToString();
            try
            {


                int i = da.ExecuteSP("cust_points_insert", ref oc
                   , da.Parameter("_user_id", email_id)
                    , da.Parameter("_txn_amount", convertToDecimal(points))
                    , da.Parameter("_txn_type", "c")
                    , da.Parameter("_txn_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_txn_comment", "Uploaded Points")
                    , da.Parameter("_txn_status", 1)
                    , da.Parameter("_outmsg", String.Empty, true));

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> Upload_Cust_Data(string strore, string FirstName, string LastName, string EmailId, string PointsLoaded, string PointsRedeemed, string ShippingAddress)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            try
            {


                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                int i = da.ExecuteSP("upld_user_data", ref oc
                   , da.Parameter("_first_name", FirstName)
                   , da.Parameter("_last_name", LastName)
                   , da.Parameter("_gender", 0)
                   , da.Parameter("_email_id", EmailId)
                   , da.Parameter("_mobile_no", null)
                   , da.Parameter("_office_no", null)
                   , da.Parameter("_street", ShippingAddress)
                   , da.Parameter("_city", null)
                   , da.Parameter("_state", null)
                   , da.Parameter("_country", null)
                   , da.Parameter("_zipcode", null)
                   , da.Parameter("_password", Pwd)
                   , da.Parameter("_status", 9)
                   , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                   , da.Parameter("_company", strore)
                   , da.Parameter("_points_loaded", convertToDecimal(PointsLoaded))
                   , da.Parameter("_points_redeemed", convertToDecimal(PointsRedeemed))
                   , da.Parameter("_guid", guid)
                   , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
                   , da.Parameter("_action_flag", 1)
                   , da.Parameter("_outmsg", String.Empty, true));


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }
        public List<OutCollection> Upload_Sales_Target(sales_target st)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";



            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                Pin = GetUniquePin();
                int i = da.ExecuteSP("import_sales_target", ref oc
               , da.Parameter("_first_name", convertToString(st.first_name))
               , da.Parameter("_last_name", convertToString(st.last_name))
               , da.Parameter("_mobile_no", convertToString(st.mobile_no))
               , da.Parameter("_code", convertToString(st.code))
               , da.Parameter("_email_id", convertToString(st.email_id))
               , da.Parameter("_address", convertToString(st.street))
               , da.Parameter("_city", convertToString(st.city))
               , da.Parameter("_state", convertToString(st.state))
               , da.Parameter("_zipcode", convertToString(st.zipcode))
               , da.Parameter("_password", Pwd)
               , da.Parameter("_status", 9)
               , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
               , da.Parameter("_company", convertToString(st.company))
               , da.Parameter("_guid", guid)
               , da.Parameter("_expiry_date", System.DateTime.Today.AddHours(24).ToLocalTime())
               , da.Parameter("_mother_maiden_name", ConvToString(st.Mother_Maiden_Name))
               , da.Parameter("_first_school", ConvToString(st.First_School))
               , da.Parameter("_native_place", ConvToString(st.Native_Place))
               , da.Parameter("_retailer", ConvToString(st.Retailer))
               , da.Parameter("_date_of_birth", ConvToString(st.Date_Of_Birth))
               , da.Parameter("_target", convertToString(st.Target))
               , da.Parameter("_achievement", convertToString(st.Achievement))
               , da.Parameter("_points", convertToString(st.Points))
               , da.Parameter("_start_date", convertToString(st.Start_Date))
               , da.Parameter("_end_date", convertToString(st.End_Date))
               , da.Parameter("_pin", Pin)
               , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }


        public List<OutCollection> Upload_Cust_Points(string EmailId, string PointsLoaded)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {

                int i = da.ExecuteSP("upload_customer_incentive", ref oc
                   , da.Parameter("_email_id", EmailId)
                   , da.Parameter("_points_loaded", convertToDecimal(PointsLoaded))
                   , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> ImportLavaData(Lava_Data ld)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";

            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                Pin = GetUniquePin();
                int i = da.ExecuteSP("import_lava_data", ref oc
               , da.Parameter("_first_name", convertToString(ld.FirstName))
               , da.Parameter("_last_name", convertToString(ld.LastName))
               , da.Parameter("_dob", convertToDate(ld.DateOfBirth))
               , da.Parameter("_doj", convertToDate(ld.DateOfJoiningLAVA))
               , da.Parameter("_qualification", convertToString(ld.Qualifications))
               , da.Parameter("_email_id", convertToString(ld.DSEContactNo))
               , da.Parameter("_email_address", convertToString(ld.DSEEMailId))
               , da.Parameter("_address", convertToString(ld.DSEPostalAddress))
               , da.Parameter("_achieve_vol", convertToLong(ld.DecValueAchivement))
               , da.Parameter("_points", convertToLong(ld.DSEPoints))
               , da.Parameter("_source", "excel_upload")
               , da.Parameter("_branch_name", convertToString(ld.BranchName))
               , da.Parameter("_lava_status", convertToString(ld.Status))
               , da.Parameter("_password", Pwd)
               , da.Parameter("_status", 9)
               , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
               , da.Parameter("_company_id", convertToLong(ld.CompanyId))
               , da.Parameter("_company", "")
               , da.Parameter("_guid", guid)
               , da.Parameter("_sales_cycle_id", convertToLong(ld.SelesCycleId))
               , da.Parameter("_outmsg", String.Empty, true));





            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<OutCollection> ImportXoloData(Xolo_Data ld)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            string guid = "";
            string Pwd = "";
            string Pin = "";

            try
            {

                guid = Guid.NewGuid().ToString();
                Pwd = GetUniquePassword();
                Pin = GetUniquePin();
                int i = da.ExecuteSP("import_xolo_data", ref oc
                     , da.Parameter("_first_name", convertToString(ld.First_Name))
                     , da.Parameter("_last_name", convertToString(ld.Last_Name))
                     , da.Parameter("_dob", convertToDate(ld.DOB))
                     , da.Parameter("_doj", convertToDate(ld.DOJ))
                     , da.Parameter("_qualification", convertToString(ld.Qualifiations))
                     , da.Parameter("_email_id", convertToString(ld.DSE_Contact_Number))
                     , da.Parameter("_email_address", convertToString(ld.FOS_DSE_Mail_Id))
                     , da.Parameter("_address", convertToString(ld.Address))
                     , da.Parameter("_achieve_vol", 500000)
                     , da.Parameter("_points", 0)
                     , da.Parameter("_source", "excel_upload")
                     , da.Parameter("_branch_name", convertToString(ld.Branch))
                     , da.Parameter("_lava_status", "")
                     , da.Parameter("_password", Pwd)
                     , da.Parameter("_status", 9)
                     , da.Parameter("_registration_date", System.DateTime.Now.ToLocalTime())
                     , da.Parameter("_company_id", convertToLong(ld.CompanyId))
                     , da.Parameter("_company", "")
                     , da.Parameter("_guid", guid)
                     , da.Parameter("_sales_cycle_id", convertToLong(ld.SelesCycleId))
                     , da.Parameter("_Emp_Id", convertToString(ld.Emp_Id))
                     , da.Parameter("_BU", convertToString(ld.BU))
                     , da.Parameter("_Outlet_Code", convertToString(ld.Outlet_Code))
                     , da.Parameter("_Outlet_Name", convertToString(ld.Outlet_Name))
                     , da.Parameter("_Fos_Dse_Code", convertToString(ld.FOS_DSE_CODE))
                     , da.Parameter("_TM_Name", convertToString(ld.TM_Name))
                     , da.Parameter("_TM_Code", convertToString(ld.TM_Code))
                     , da.Parameter("_city", convertToString(ld.City))
                     , da.Parameter("_District", convertToString(ld.District))
                     , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }


        public List<OutCollection> Upload_Sales_Target_MonthWise(sales_target st)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {
                int i = da.ExecuteSP("import_sales_target_monthly", ref oc
               , da.Parameter("_mobile_no", convertToString(st.mobile_no))
               , da.Parameter("_email_id", convertToString(st.email_id))
               , da.Parameter("_target", convertToString(st.Target))
               , da.Parameter("_achievement", convertToString(st.Achievement))
               , da.Parameter("_points", convertToString(st.Points))
               , da.Parameter("_start_date", convertToString(st.Start_Date))
               , da.Parameter("_end_date", convertToString(st.End_Date))
               , da.Parameter("_outmsg", String.Empty, true));
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }
        public List<OutCollection> Upload_Sales_Target_Monthly_Lava(sales_target_monthly_lava stm)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {
                int i = da.ExecuteSP("import_sales_target_monthly_lava", ref oc
                   , da.Parameter("_company_id", null)
                   , da.Parameter("_user_id", convertToString(stm.Email))
                   , da.Parameter("_sales_cycle_id", 0)
                   , da.Parameter("_sku", convertToString(stm.Sku))
                   , da.Parameter("_target_val", convertToInt(stm.TargetValue))
                   , da.Parameter("_achieve_val", convertToInt(stm.AchieveValue))
                   , da.Parameter("_target_vol", convertToInt(stm.TargetVol))
                   , da.Parameter("_achieve_vol", convertToInt(stm.AchieveVol))
                   , da.Parameter("_point", convertToInt(stm.Points))
                   , da.Parameter("_start_date", convertToString(stm.StartDate))
                   , da.Parameter("_end_date", convertToString(stm.EndDate))
                   , da.Parameter("_source", "excel_upload")
                   , da.Parameter("_create_ts", System.DateTime.Now.ToLocalTime())
                   , da.Parameter("_outmsg", String.Empty, true));

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }
        private string GetUniquePassword()
        {
            try
            {
                int maxSize = 7;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }
        private string GetUniquePin()
        {
            try
            {
                int maxSize = 5;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        //public string update_order(ordadmin o)
        //{

        //    DataAccess da = new DataAccess();
        //    dal.DataAccess dal = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();
        //    string guid = "";
        //    string MobNo = "";
        //    string strMobileNo = "";
        //    try
        //    {

        //        int i = da.ExecuteSP("update_order_info", ref oc
        //            , da.Parameter("_order_id", o.order_id)
        //            , da.Parameter("_status", o.status)
        //            , da.Parameter("_shipping_info", o.shipping_info)
        //            , da.Parameter("_courier_track_no", o.courier_track_no)
        //            , da.Parameter("_courier_track_link", o.courier_track_link)
        //            , da.Parameter("_dispute_notes", o.dispute_notes)
        //            , da.Parameter("_delayed_note", o.delayed_note)
        //            , da.Parameter("_cancellation_note", o.cancellation_note)
        //            , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
        //            , da.Parameter("_shipping_date", convertToDate(o.shipping_date))
        //            , da.Parameter("_courier_cost", convertToDouble(o.courier_cost))
        //            , da.Parameter("_outmsg", String.Empty, true));

        //        if (i == 1)
        //        {
        //            registration_repository rr = new registration_repository();

        //            //For SMS
        //            helper_repository hp = new helper_repository();
        //            string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
        //            string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
        //            string sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG2"].ToString();
        //            string sms_ssid = ConfigurationSettings.AppSettings["SMSTRNID"].ToString();
        //            // End
        //            if (o.status.ToString() == "5")
        //            {
        //                if (convertToInt(o.send_email) == 1)
        //                {

        //                    string email_id = "";
        //                    payment_repository pr = new payment_repository();
        //                    string strMsgBody = pr.GetShippingStatusBody(o.order_id.ToString(), convertToString(o.shipping_date), convertToString(o.shipping_info), convertToString(o.courier_track_no), convertToString(o.courier_track_link), convertToDouble(o.courier_cost), 5, ref email_id);
        //                    helper_repository hr = new helper_repository();

        //                    string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
        //                    if (email_id.Length > 0 && strMsgBody.Length > 0)
        //                    {
        //                        hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Shipment: Annectos", strMsgBody);

        //                        sms_message = sms_message.Replace("##OrderNo##", o.order_id.ToString());
        //                        sms_message = sms_message.Replace("##Courier##", o.shipping_info.ToString());
        //                        sms_message = sms_message.Replace("##TrackNo##", o.courier_track_no.ToString());

        //                        strMobileNo = rr.getUserMobileNo(email_id);
        //                        if (rr.CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
        //                        {
        //                            string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message, "TRNS");
        //                        }
        //                    }

        //                    return "send emnail";
        //                }

        //            }
        //            else if (o.status.ToString() == "6")
        //            {
        //                string email_id = "";
        //                payment_repository pr = new payment_repository();
        //                string strMsgBody = pr.GetDelayStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), convertToString(o.Delay_Days), 6, ref email_id);
        //                helper_repository hr = new helper_repository();
        //                string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();

        //                if (email_id.Length > 0 && strMsgBody.Length > 0)
        //                {
        //                    hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Delayed Information: Annectos", strMsgBody);
        //                }

        //                return "send emnail";

        //            }
        //            else if (o.status.ToString() == "7")
        //            {
        //                string email_id = "";
        //                payment_repository pr = new payment_repository();
        //                string strMsgBody = pr.GetCancelStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), 7, ref email_id);
        //                helper_repository hr = new helper_repository();
        //                string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();


        //                long OrderID = convertToLong(o.order_id);
        //                //DataTable dtOrdDtls = new DataTable();
        //                List<mongo_order> dtOrdDtls = pr.GetDataFromOrder(OrderID);

        //                if (dtOrdDtls != null || dtOrdDtls.Count > 0)
        //                {
        //                    for (int j = 0; j < dtOrdDtls.Count; j++)
        //                    {

        //                        string cart_data = "";
        //                        cart_data = convertToString(dtOrdDtls[j].cart_data);
        //                        List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());

        //                        ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
        //                        foreach (cart_item ordupdt in ordlist)
        //                        {
        //                            cr.update_stock_qty(ordupdt.id, Convert.ToDouble(ordupdt.quantity), "Add");
        //                        }

        //                    }
        //                }

        //                if (email_id.Length > 0 && strMsgBody.Length > 0)
        //                {
        //                    hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Cancelled Information: Annectos", strMsgBody);
        //                }

        //                return "send emnail";

        //            }

        //            return "Success";
        //        }
        //        else
        //            return "Failed";
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        return "Failed"; ;
        //    }

        //}

        public string update_order(ordadmin o)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string MobNo = "";
            string strMobileNo = "";
            string date = DateTime.Now.ToString("dd/MM/yyyy");
            try
            {

                //int i = da.ExecuteSP("update_order_info", ref oc
                //    , da.Parameter("_order_id", o.order_id)
                //    , da.Parameter("_status", o.status)
                //    , da.Parameter("_shipping_info", o.shipping_info)
                //    , da.Parameter("_courier_track_no", o.courier_track_no)
                //    , da.Parameter("_courier_track_link", o.courier_track_link)
                //    , da.Parameter("_dispute_notes", o.dispute_notes)
                //    , da.Parameter("_delayed_note", o.delayed_note)
                //    , da.Parameter("_cancellation_note", o.cancellation_note)
                //    , da.Parameter("_modify_ts", System.DateTime.Now.ToLocalTime())
                //    , da.Parameter("_shipping_date", convertToDate(o.shipping_date))
                //    , da.Parameter("_courier_cost", convertToDouble(o.courier_cost))
                //    , da.Parameter("_outmsg", String.Empty, true));


                dispute dis = new dispute();
                dis = o.dispute_data;
                dis.dispute_ts = DateTime.Now.ToString("dd/MM/yyyy");
                dis.order_id = o.order_id;


                BsonDocument bd_prod = o.ToBsonDocument();
                var whereclause = "{order_id:" + convertToInt(o.order_id) + "}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();

                if (convertToInt(o.status) == 5)
                {
                    update = MongoDB.Driver.Builders.Update

                     .Set("shipping_info", convertToString(o.shipping_info))
                     .Set("status", convertToInt(o.status))
                     .Set("courier_track_no", convertToString(o.courier_track_no))
                     .Set("courier_track_link", convertToString(o.courier_track_link))
                     .Set("courier_cost", convertToString(o.courier_cost))
                     .Set("dispatch_date", convertToString(o.shipping_date))
                     .Set("modify_ts", date);

                }
                else if (convertToInt(o.status) == 6)
                {
                    update = MongoDB.Driver.Builders.Update
                   .Set("delayed_note", convertToString(o.delayed_note))
                   .Set("delayed_days", convertToString(o.Delay_Days))
                   .Set("status", convertToInt(o.status))
                   .Set("modify_ts", date);

                }

                else if (convertToInt(o.status) == 7)
                {
                    update = MongoDB.Driver.Builders.Update
                    .Set("cancellation_note", convertToString(o.cancellation_note))
                    .Set("status", convertToInt(o.status))
                    .Set("modify_ts", date);

                }

                else if (convertToInt(o.status) == 13)
                {

                    update = MongoDB.Driver.Builders.Update
                    .Set("status", convertToInt(o.status))
                    .SetWrapped("dispute_data", dis)
                    .Set("modify_ts", date);

                }


                string result = dal.mongo_update("order", whereclause, bd_prod, update);

                //  return result;

                if (result != "")


                //if (i == 1)
                {
                    registration_repository rr = new registration_repository();

                    //For SMS
                    helper_repository hp = new helper_repository();
                    string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                    string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                    string sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG2"].ToString();
                    string sms_ssid = ConfigurationSettings.AppSettings["SMSTRNID"].ToString();
                    // End
                    if (o.status.ToString() == "5")
                    {
                        if (convertToInt(o.send_email) == 1)
                        {

                            string email_id = "";
                            payment_repository pr = new payment_repository();
                            string strMsgBody = pr.GetShippingStatusBody(o.order_id.ToString(), convertToString(o.shipping_date), convertToString(o.shipping_info), convertToString(o.courier_track_no), convertToString(o.courier_track_link), convertToDouble(o.courier_cost), 5, ref email_id);
                            helper_repository hr = new helper_repository();

                            string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();
                            if (email_id.Length > 0 && strMsgBody.Length > 0)
                            {
                                hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Shipment: Annectos", strMsgBody);

                                sms_message = sms_message.Replace("##OrderNo##", o.order_id.ToString());
                                sms_message = sms_message.Replace("##Courier##", o.shipping_info.ToString());
                                sms_message = sms_message.Replace("##TrackNo##", o.courier_track_no.ToString());

                                strMobileNo = rr.getUserMobileNo(email_id);
                                if (rr.CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                                {
                                    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message, "TRNS");
                                }
                            }

                            return "send emnail";
                        }

                    }
                    else if (o.status.ToString() == "6")
                    {
                        string email_id = "";
                        payment_repository pr = new payment_repository();
                        string strMsgBody = pr.GetDelayStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), convertToString(o.Delay_Days), 6, ref email_id);
                        helper_repository hr = new helper_repository();
                        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();

                        if (email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Delayed Information: Annectos", strMsgBody);
                        }

                        return "send emnail";

                    }
                    else if (o.status.ToString() == "7")
                    {
                        string email_id = "";
                        payment_repository pr = new payment_repository();
                        string strMsgBody = pr.GetCancelStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), 7, ref email_id);
                        helper_repository hr = new helper_repository();
                        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();


                        long OrderID = convertToLong(o.order_id);
                        //DataTable dtOrdDtls = new DataTable();
                        List<mongo_order> dtOrdDtls = pr.GetDataFromOrder(OrderID);

                        if (dtOrdDtls != null || dtOrdDtls.Count > 0)
                        {
                            for (int j = 0; j < dtOrdDtls.Count; j++)
                            {

                                string cart_data = "";
                                cart_data = convertToString(dtOrdDtls[j].cart_data);
                                List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());

                                ecomm.model.repository.category_repository cr = new ecomm.model.repository.category_repository();
                                foreach (cart_item ordupdt in ordlist)
                                {
                                    cr.update_stock_qty(ordupdt.id, Convert.ToDouble(ordupdt.quantity), "Add");
                                }

                            }
                        }

                        if (email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Cancelled Information: Annectos", strMsgBody);
                        }

                        return "send emnail";

                    }

                    return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }

        public string micro_update_order(ordadmin o)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            string guid = "";
            string MobNo = "";
            string strMobileNo = "";
            string date = DateTime.Now.ToString("dd/MM/yyyy");
            try
            {
                //dispute dis = new dispute();
                //dis = o.dispute_data;
                //dis.dispute_ts = DateTime.Now.ToString("dd/MM/yyyy");
                //dis.order_id = o.order_id;


                BsonDocument bd_prod = o.ToBsonDocument();
                var whereclause = "{order_id:" + convertToInt(o.order_id) + "}";
                MongoDB.Driver.Builders.UpdateBuilder update = new UpdateBuilder();

                if (convertToInt(o.status) == 5)
                {
                    update = MongoDB.Driver.Builders.Update

                     .Set("shipping_info", convertToString(o.shipping_info))
                     .Set("status", convertToInt(o.status))
                     .Set("courier_track_no", convertToString(o.courier_track_no))
                     .Set("courier_track_link", convertToString(o.courier_track_link))
                     .Set("courier_cost", convertToString(o.courier_cost))
                     .Set("dispatch_date", convertToString(o.shipping_date))
                     .Set("modify_ts", DateTime.Now);

                }
                else if (convertToInt(o.status) == 6)
                {
                    update = MongoDB.Driver.Builders.Update
                   .Set("delayed_note", convertToString(o.delayed_note))
                   .Set("delayed_days", convertToString(o.Delay_Days))
                   .Set("status", convertToInt(o.status))
                   .Set("modify_ts", DateTime.Now);

                }

                else if (convertToInt(o.status) == 7)
                {
                    update = MongoDB.Driver.Builders.Update
                    .Set("cancellation_note", convertToString(o.cancellation_note))
                    .Set("status", convertToInt(o.status))
                    .Set("modify_ts", DateTime.Now);

                }

                else if (convertToInt(o.status) == 13)
                {

                    update = MongoDB.Driver.Builders.Update
                    .Set("status", convertToInt(o.status))
                        //.SetWrapped("dispute_data", dis)
                    .Set("modify_ts", DateTime.Now);

                }


                string result = dal.mongo_update("order", whereclause, bd_prod, update);

                //  return result;

                if (result != "")


                //if (i == 1)
                {
                    registration_repository rr = new registration_repository();

                    //For SMS
                    helper_repository hp = new helper_repository();
                    string sms_user_id = ConfigurationSettings.AppSettings["SMSNONPROMOLOGIN"].ToString();
                    string sms_pwd = ConfigurationSettings.AppSettings["SMSNONPROMOPWD"].ToString();
                    string sms_message = ConfigurationSettings.AppSettings["SMS_USER_TRNS_MSG2"].ToString();
                    string sms_ssid = ConfigurationSettings.AppSettings["SMSTRNID"].ToString();
                    // End
                    if (o.status.ToString() == "5")
                    {
                        if (convertToInt(o.send_email) == 1)
                        {

                            string email_id = "";

                            string strMsgBody = GetMicroShippingStatusBody(o.order_id.ToString(), convertToString(o.shipping_date), convertToString(o.shipping_info), convertToString(o.courier_track_no), convertToString(o.courier_track_link), convertToDouble(o.courier_cost), 5, ref email_id);
                            helper_repository hr = new helper_repository();

                            string bcclist = ConfigurationSettings.AppSettings["Management"].ToString() + "," + ConfigurationSettings.AppSettings["bcc_email"].ToString();
                            
                            if (email_id.Length > 0 && strMsgBody.Length > 0)
                            {
                                hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Your Gift has been shipped by GRASS : Order # " + o.order_id.ToString(), strMsgBody);

                                //sms_message = sms_message.Replace("##OrderNo##", o.order_id.ToString());
                                //sms_message = sms_message.Replace("##Courier##", o.shipping_info.ToString());
                                //sms_message = sms_message.Replace("##TrackNo##", o.courier_track_no.ToString());

                                //strMobileNo = rr.getUserMobileNo(email_id);
                                //if (rr.CheckMobileNo(strMobileNo, ref MobNo) != "Failure")
                                //{
                                //    string resp = hp.SendSMS(sms_user_id, sms_pwd, MobNo, sms_ssid, sms_message, "TRNS");
                                //}
                            }

                            return "send emnail";
                        }

                    }
                    else if (o.status.ToString() == "6")
                    {
                        string email_id = "";
                        payment_repository pr = new payment_repository();
                        string strMsgBody = pr.GetDelayStatusBody(o.order_id.ToString(), convertToString(o.shipping_info), convertToString(o.Delay_Days), 6, ref email_id);
                        helper_repository hr = new helper_repository();
                        string bcclist = ConfigurationSettings.AppSettings["Management"].ToString();

                        if (email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Order Delayed Information: Annectos", strMsgBody);
                        }

                        return "send emnail";

                    }
                    else if (o.status.ToString() == "7")
                    {

                        string email_id = "";
                        string strMsgBody = GetMicroCancelStatusBody(convertToString(o.order_id), convertToString(o.cancellation_note), 7, ref email_id);
                        helper_repository hr = new helper_repository();
                        email_id = ConfigurationSettings.AppSettings["Management"].ToString();
                        string bcclist = ConfigurationSettings.AppSettings["bcc_email"].ToString();
                       
                        if (email_id.Length > 0 && strMsgBody.Length > 0)
                        {
                            hr.SendMail(rr.getUserEMailAddress(email_id), bcclist, "Your Gift has been cancelled by GRASS : Order # " + o.order_id.ToString(), strMsgBody);
                        }

                        return "send email";

                    }

                    return "Success";
                }
                else
                    return "Failed";
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }

        }


        public string GetMicroShippingStatusBody(string OrderID, string shipdate, string CourServProv, string CourTrackNo, string CourTrackLink, double dCourierCost, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;

                //mongo_cart_data cart_data = new mongo_cart_data();
                double shipmentvalue = 0;
                double discount = 0;
                double finalamt = 0;
                double paidamt = 0;
                string vouchers = "";
                double points = 0;
                double pointamt = 0;
                double voucheramt = 0;
                double PromoOffer = 0;
                string discount_rule = "";
                string discount_code = "";
                string strShippingInfo = "";
                string strStoreName = "";
                string strLogo = "";

                string strMobileNo = "";
                string ShippingName = "";
                string ShippingAddress1 = "";
                string ShippingAddress2 = "";
                string ShippingCompany = "";
                string ShippingDesignation = "";
                string ShippingPin = "";
                string shippingAmount = "";
                string ShippingThreshold = "0";// ConfigurationSettings.AppSettings["shipping_threshold"].ToString();
                string strCompanyUrl = "";
                shippingAmount = "0";// ConfigurationSettings.AppSettings["shipping_amount"].ToString();

                List<mongo_order> dt = GetMicroDataFromOrder(Convert.ToInt64(OrderID));
                email_id = convertToString(dt[0].user_id);

                //var cart_data = JsonConvert.DeserializeObject<mongo_cart_data>(dt[0].cart_data.ToString());
                //var user_data = JsonConvert.DeserializeObject<mongo_user_data>(dt[0].user_data.ToString());
                var prod_data = JsonConvert.DeserializeObject<grass_micro_product>(dt[0].cart_data.cart_data.ToString());



                ShippingName = convertToString(dt[0].user_data.first_name + " " + dt[0].user_data.last_name);
                ShippingCompany = convertToString(dt[0].user_data.company);
                ShippingDesignation = convertToString(dt[0].user_data.user_type);
                ShippingAddress1 = convertToString(dt[0].user_data.address);
                ShippingAddress2 = convertToString(dt[0].user_data.street);
                ShippingPin = convertToString(dt[0].user_data.zipcode);
                strMobileNo = convertToString(dt[0].user_data.mobile_no);


                mailBody = @"<html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= 'http://grass.shopinbulk.in/' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%' style='padding-left: 120px; font-size: x-large;'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'>Order Shipment</td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FULL_NAME##,</em></strong></font><br /><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>We would like to inform you that your order has been shipped from our warehouse. You will receive your order over the next few days.
                        <br />
                       
                        <td width='5%'>&nbsp;</td>
                        </tr>      
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
        	            <td style='padding: 10px;'>
        		        <table style='border-collapse: collapse' border='0' cellspacing='0' cellpadding='0' width='100%' style='width:100.0%'>
                        <tbody>  <tr>  <td colspan='4'  style= 'font-family:Helvetica, Arial, sans-serif; padding-bottom:10px; padding-top:10px;   font-size:12px; text-transform:uppercase'>  
						
					    <strong>1 Product Code: ##PROD_CODE## - ##PROD_NAME##  </td>     <td style='padding:0cm 0cm 0cm 0cm'></td>  </tr>  <tr>  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >  
						<div style='padding:0cm 0cm 0cm 8.0pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Image</span></p>  	<div style='margin-top:7.5pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:red'><img src='##PROD_IMG_LINK##' width='90px' height='120px'></span></p>  </div>  </div>  </td>  <td style='border: 1px solid #ccc; font-family:Helvetica, Arial, sans-serif;  padding: 5px;' >  
					
						<div style='padding:0cm 0cm 0cm 8.0pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#666666'>Size:</span></p>  <div style='margin-top:7.5pt'>  <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#333333'>##PROD_SIZE##</span></p>  </div>   </div>   </td>    </tr>  <tr>      
                        <td bgcolor='#f1613b' ></td>
                        <td  bgcolor='#f1613b'  >
                        <div style='padding-top: 2px; padding-bottom: 2px;' >
                       
                        <div >
                        <p class='MsoNormal' align='center' style='text-align:center'><span style='font-size:10.0pt;color:#ffffff'></span></p>
                        </div>
                        </div>
                        </td>

                        </tr>
                        </tbody>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>  
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        <td style='padding: 10px;' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'><strong><em>Shipping Details:  </em></strong></font><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>   Shipping Date :  ##SHIPPING_DATE##<br />   Courier Service Provider :##COURIER_SVC_PROVIDER## <br />   Courier Track No :##COURIER_TRACK_NO## <br />   Courier Track Link :##COURIER_TRACK_LINK## <br /> <br />
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
						<tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        <td style='padding: 10px;' align='left' valign='top'><font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'><strong><em>Shipping Address: </em></strong></font><br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>   Name:  ##FULL_NAME##<br />   Company :##COMPANY## <br />   Designation :##DESIGNATION## <br />   Address :##ADDRESS_LINE_1## ##ADDRESS_LINE_2##<br />   Mobile:##PHONE## <br /> Zip Code :##ZIP_CODE## <br /><br />
                       
                        </font></td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Program Center</strong><br/>
                        <strong>GRASS Promo Apparels</strong>         
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2015 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in/' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in/</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html>";

                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";

                mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
                mailBody = mailBody.Replace("##PROD_CODE##", convertToString(prod_data.sku));
                mailBody = mailBody.Replace("##PROD_NAME##", convertToString(dt[0].selected_prod_name));
                mailBody = mailBody.Replace("##PROD_IMG_LINK##", convertToString(prod_data.image_urls[0].thumb_link));
                mailBody = mailBody.Replace("##ORDER_NO##", convertToString(OrderID));
                mailBody = mailBody.Replace("##PROD_SIZE##", convertToString(dt[0].selected_size));
                mailBody = mailBody.Replace("##FULL_NAME##", convertToString(ShippingName));
                mailBody = mailBody.Replace("##DESIGNATION##", convertToString(ShippingDesignation));
                mailBody = mailBody.Replace("##COMPANY##", convertToString(ShippingCompany));
                mailBody = mailBody.Replace("##ADDRESS_LINE_1##", convertToString(ShippingAddress1));
                mailBody = mailBody.Replace("##ADDRESS_LINE_2##", convertToString(ShippingAddress2));
                mailBody = mailBody.Replace("##PHONE##", convertToString(strMobileNo));
                mailBody = mailBody.Replace("##ZIP_CODE##", convertToString(ShippingPin));
                mailBody = mailBody.Replace("##SHIPPING_DATE##", convertToString(shipdate));
                mailBody = mailBody.Replace("##COURIER_SVC_PROVIDER##", convertToString(CourServProv));
                mailBody = mailBody.Replace("##COURIER_TRACK_NO##", convertToString(CourTrackNo));
                mailBody = mailBody.Replace("##COURIER_TRACK_LINK##", convertToString(CourTrackLink));


                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string GetMicroCancelStatusBody(string OrderID, string cancelnote, int Status, ref string email_id)
        {

            try
            {
                string mailBody = string.Empty;
                string strLogo = "";
                string ShippingName = "";
                List<mongo_order> dt = GetMicroDataFromOrder(Convert.ToInt64(OrderID));
                email_id = convertToString(dt[0].user_id);

                ShippingName = convertToString(dt[0].user_data.first_name + " " + dt[0].user_data.last_name);

                mailBody = @"<html>
                        <body bgcolor='#8d8e90'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0' bgcolor='#8d8e90'>
                        <tr>
                        <td><table width='800' border='0' cellspacing='0' cellpadding='0' bgcolor='#FFFFFF' align='center'>
                        <tr>
                        <td style='border-bottom:5px solid #f1613b; padding: 10px;'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>              
                        <td width='144'><a href= 'http://grass.shopinbulk.in/' target='_blank'><img src='http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png'  border='0' alt=''/></a></td>
                        <td width='393'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td height='46' align='right' valign='middle'><table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='67%'style='padding-left: 120px; font-size: x-large;'><font style='font-family:'Myriad Pro', Helvetica, Arial, sans-serif; color:#68696a; font-size:18px; text-transform:uppercase'>Order Cancellation</td>                         
                        <td width='4%'>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr>                  
                        </table></td>
                        </tr>
                        </table></td>
                        </tr>
                        <tr>
                        <td align='center'>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'><font style='font-family: Verdana, 'Times New Roman', Times, serif; color:#010101; font-size:24px'><strong><em>Hi ##FULL_NAME##,</em></strong></font>
                        </td>
                        <td width='5%'>&nbsp;</td>
                        
                        <tr>
                        <td width='5%'>&nbsp;</td>
                        <td width='90%' align='left' valign='top'>
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>          
                        <br />Your order number : <strong style='color: #f1613b'>##ORDER_NO##</strong> has been canceled. <br /> 
                        <br />
                        Reason : ##CANCEL_NOTE##</td>
                        <td width='5%'>&nbsp;</td>
                        </tr>						
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                       
                        </tr>
                        
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'></td>
                        </tr>
                        
                        <tr>
                        <td>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>             
                        <td style='padding: 10px;' align='left' valign='top'>
                        <br />
                        <font style='font-family: Verdana, Geneva, sans-serif; color:#666766; font-size:13px; line-height:21px'>  

                        Once verified and confirmed ,you will receive the order dispatch mail with further details.In the meanwhile if you have any queries or clarifications please connect with our customer support team at 91 9686202046 / 9972334590 or email us at <a  style='color: #f1613b'  href='mailto:customerfirst@grassapparels.in' target='_blank'>customerfirst@grassapparels.in</a>
                        <br />
                        <br />
                        You can continue shopping at <a style='color: #f1613b' href='http://grass.shopinbulk.in/' target='_blank'>www.grass.shopinbulk.in</a></font> 
                        </td>             
                        </tr>             
                        </table></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td style='border-bottom: 5px solid #f1613b;'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>     
                        <td style='padding: 10px;' align='left' valign='top'>
                        <font style='font-family: Georgia, 'Times New Roman', Times, serif; color:#010101; font-size:14px'>
                        <strong>Regards</strong> ,<br/>
                        <strong>Program Center</strong><br/>
                        <strong>GRASS Promo Apparels</strong>           
                        </font>
                        </td>
                        </tr>
                        </table>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>               
                        <tr>
                        <td align='center'><font style='font-family:Helvetica, Arial, sans-serif; color:#231f20; font-size:12px'>Copyright &copy; 2015 Annectos.All rights reserved.</font></td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        <tr>
                        <td align='center'>
                        <font style=' font-family:Helvetica, Arial, sans-serif; color:#f1613b; font-size:12px; text-transform:uppercase'><a href='http://grass.shopinbulk.in/' style='color:#f1613b; text-decoration:none'target='_blank' >http://grass.shopinbulk.in/</a></font>
                        </td>
                        </tr>
                        <tr>
                        <td>&nbsp;</td>
                        </tr>
                        </table></td>
                        </tr> 
                        </table>
                        </body>
                        </html> ";

                strLogo = "http://cdn-new-annectos.s3.amazonaws.com/images/brandlogo/n/grassbulk.png";

                mailBody = mailBody.Replace("##GRASS_LINK##", "http://grass.shopinbulk.in");
                mailBody = mailBody.Replace("##GRASS_LOGO##", strLogo);
                mailBody = mailBody.Replace("##ORDER_NO##", convertToString(OrderID));
                mailBody = mailBody.Replace("##FULL_NAME##", convertToString(ShippingName));
                mailBody = mailBody.Replace("##CANCEL_NOTE##", convertToString(cancelnote));


                return mailBody;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }


        public List<mongo_order> GetMicroDataFromOrder(double order_id)
        {

            var micro_order = new List<mongo_order>();

            try
            {
                // evoke mongodb connection method in dal
                dal.DataAccess dal = new DataAccess();

                string q = "{order_id:" + order_id + "}";

                //string q = "{}";
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
                QueryDocument queryDoc = new QueryDocument(document);
                string[] include_fields = new string[] { "order_id", "cart_data", "user_id", "user_data", "selected_size", "selected_prod_name", "status" };
                string[] exclude_fields = new string[] { "_id" };
                //  MongoCursor cursor = dal.execute_mongo_db("order", queryDoc);
                MongoCursor cursor = dal.execute_mongo_db("order", queryDoc, include_fields, exclude_fields);

                foreach (var c in cursor)
                {
                    mongo_order omo = new mongo_order();
                    omo.order_id = check_field(c.ToBsonDocument(), "order_id") ? convertToDouble(c.ToBsonDocument()["order_id"]) : 0;
                    omo.cart_data = JsonConvert.DeserializeObject<mongo_cart_data>(c.ToBsonDocument()["cart_data"].ToString());
                    omo.user_data = JsonConvert.DeserializeObject<mongo_user_data>(c.ToBsonDocument()["user_data"].ToString());
                    omo.user_id = check_field(c.ToBsonDocument(), "user_id") ? convertToString(c.ToBsonDocument()["user_id"]) : null;
                    omo.selected_size = check_field(c.ToBsonDocument(), "selected_size") ? convertToString(c.ToBsonDocument()["selected_size"]) : null;
                    omo.selected_prod_name = check_field(c.ToBsonDocument(), "selected_prod_name") ? convertToString(c.ToBsonDocument()["selected_prod_name"]) : null;
                    omo.status = check_field(c.ToBsonDocument(), "status") ? convertToInt(c.ToBsonDocument()["status"]) : 0;
                    micro_order.Add(omo);
                }


            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
            }
            return micro_order;

        }

        //public List<user_security> getUser_For_Rights(string company)
        //{

        //    DataAccess da = new DataAccess();
        //    List<user_security> lus = new List<user_security>();
        //    DataTable dtResult = new DataTable();

        //    try
        //    {
        //        string[] strout = new string[0];

        //        dtResult = da.ExecuteDataTable("get_user_for_rights"
        //                        , da.Parameter("_company", company));

        //        if (dtResult != null && dtResult.Rows.Count > 0)
        //        {
        //            for (int i = 0; i < dtResult.Rows.Count; i++)
        //            {
        //                user_security ous = new user_security();
        //                ous.Id = convertToLong(dtResult.Rows[i]["Id"].ToString());
        //                ous.user_name = dtResult.Rows[i]["user_name"].ToString();
        //                ous.password = dtResult.Rows[i]["password"].ToString();
        //                ous.company_id = convertToLong(dtResult.Rows[i]["company_id"].ToString());
        //                ous.user_type = dtResult.Rows[i]["user_type"].ToString();
        //                lus.Add(ous);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }
        //    return lus;

        //}


        public List<mongo_user_security> getUser_For_Rights(string company)
        {


            DataAccess da = new DataAccess();

            MongoCursor cursor = da.execute_mongo_db("user_security");

            List<mongo_user_security> user_security = new List<mongo_user_security>();

            try
            {


                foreach (var c in cursor)
                {
                    mongo_user_security mongo_user = new mongo_user_security();

                    mongo_user.Id = check_field(c.ToBsonDocument(), "Id") ? convertToInt(c.ToBsonDocument()["Id"]) : 0;
                    mongo_user.user_name = check_field(c.ToBsonDocument(), "user_name") ? convertToString(c.ToBsonDocument()["user_name"]) : null;
                    mongo_user.company_id = check_field(c.ToBsonDocument(), "company_id") ? convertToInt(c.ToBsonDocument()["company_id"]) : 0;
                    mongo_user.password = check_field(c.ToBsonDocument(), "password") ? convertToString(c.ToBsonDocument()["password"]) : null;
                    mongo_user.user_type = check_field(c.ToBsonDocument(), "user_type") ? convertToString(c.ToBsonDocument()["user_type"]) : null;
                    user_security.Add(mongo_user);
                }

            }

            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return user_security;

        }



        public List<promo> get_promo_list(string promo_id)
        {

            DataAccess da = new DataAccess();
            List<promo> lp = new List<promo>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_promotion_details"
                                , da.Parameter("_promo_id", promo_id));

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        promo op = new promo();
                        op.promo_id = convertToString(dtResult.Rows[i]["promo_id"]);
                        op.promo_name = convertToString(dtResult.Rows[i]["promo_name"]);
                        op.company = convertToString(dtResult.Rows[i]["company"].ToString());
                        op.min_purchase = convertToDecimal(dtResult.Rows[i]["min_purchase"]);
                        op.disc_type = convertToString(dtResult.Rows[i]["disc_type"]);
                        op.max_discount = convertToDecimal(dtResult.Rows[i]["max_discount"].ToString());
                        op.start_date = convertToString(dtResult.Rows[i]["Start_Dt"]);
                        op.end_date = convertToString(dtResult.Rows[i]["End_Dt"]);
                        lp.Add(op);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lp;

        }

        public string save_promo(promo op)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            if (op.promo_id == null || op.promo_id == "0")
            {
                string promo_id = GetUniquePromoID();
                op.promo_id = promo_id;
            }


            int i = 0;
            try
            {
                i = da.ExecuteSP("ins_upd_promo_info", ref oc
                   , da.Parameter("_promo_id", op.promo_id)
                   , da.Parameter("_promo_name", op.promo_name)
                   , da.Parameter("_company", op.company)
                   , da.Parameter("_min_purchase", op.min_purchase)
                   , da.Parameter("_disc_type", op.disc_type)
                   , da.Parameter("_max_discount", op.max_discount)
                   , da.Parameter("_start_date", op.start_date)
                   , da.Parameter("_end_date", op.end_date)
                   , da.Parameter("_outmsg", String.Empty, true));


                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        public string transfer_promo(promo op)
        {
            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            int i = 0;
            try
            {
                i = da.ExecuteSP("transfer_points_to_store", ref oc
                   , da.Parameter("_promo_id", op.promo_id)
                   , da.Parameter("_company", op.company)
                   , da.Parameter("_outmsg", String.Empty, true));


                if (i == 1)
                {
                    return oc[0].strParamValue.ToString();
                }
                else
                    return oc[0].strParamValue.ToString();
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "Failed"; ;
            }
        }

        private string GetUniquePromoID()
        {
            try
            {
                int maxSize = 8;
                char[] chars = new char[62];
                string a;
                a = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
                chars = a.ToCharArray();
                RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider();
                int size = maxSize;
                byte[] data = new byte[size];
                crypto.GetNonZeroBytes(data);
                //int value = BitConverter.ToInt32(data, 0);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < data.Length; i++)
                {
                    sb.Append(data[i].ToString("X2"));
                }
                return sb.ToString();

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                return "";
            }
        }

        public string ProcessOrderStatusAfterPG(paymentinfostatus pis)
        {

            try
            {
                DataAccess da = new DataAccess();
                string strPayInfo = "";
                string strRejectInfo = "";
                Int64 OrderID = pis.order_id;
                string PaidAmt = "";
                string remarks = "";
                int status = pis.status;
                ordadmin ordr = new ordadmin();
                store_repository sr = new store_repository();

                if (OrderID > 0)
                {

                    if (status == 2)
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == 3)
                    {
                        strRejectInfo = remarks;
                    }
                    else if (status == 4)
                    {
                        strPayInfo = remarks;
                    }
                    else if (status == 9)
                    {
                        strRejectInfo = remarks;
                    }

                    int i = da.ExecuteSP("pay_gateway_order",
                             da.Parameter("_order_id", Convert.ToInt64(OrderID))
                            , da.Parameter("_paidamt", Convert.ToDecimal(PaidAmt))
                            , da.Parameter("_paymentinfo", strPayInfo)
                            , da.Parameter("_rejectioninfo", strRejectInfo)
                            , da.Parameter("_status", Convert.ToInt32(status))
                            );

                    //diwakar 13/2/2014 clear cart if order is through ; using existing method so that code is not
                    //changed at multiple tiers. May be an overhead because the entire order data is retrived can change sp
                    //at later stage to return only value thats needed.
                    /************Start*****************************************/

                    ordr = get_order_data(Convert.ToInt64(OrderID));

                    if (ordr.email_id != "")
                    {
                        //  delete
                        sr.del_cart(ordr.email_id, ordr.store);

                    }
                    /************End*****************************************/

                }
                return "Success";
            }
            catch (Exception ex)
            {

                ExternalLogger.LogError(ex, this, "#");
                return "Failure";
            }
        }

        public ordadmin get_order_data(long Order_Id)
        {
            dal.DataAccess dal = new DataAccess();
            ///DataAccess da = new DataAccess();
            //DataTable dtResult = new DataTable();
            ordadmin ordr = new ordadmin();
            payment_repository opr = new payment_repository();
            try
            {
                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{order_id:" + Order_Id + "}");
                QueryDocument queryDoc = new QueryDocument(document);
                MongoCursor cursor = dal.execute_mongo_db("order", queryDoc);
                foreach (var c in cursor)
                {
                    ordr.order_id = check_field(c.ToBsonDocument(), "order_id") ? convertToLong(c.ToBsonDocument()["order_id"]) : 0;
                    ordr.order_display_id = check_field(c.ToBsonDocument(), "order_display_id") ? convertToString(c.ToBsonDocument()["order_display_id"]) : null;
                    ordr.total_amount = check_field(c.ToBsonDocument(), "total_amount") ? convertToDecimal(c.ToBsonDocument()["total_amount"]) : 0;
                    ordr.store = check_field(c.ToBsonDocument(), "store") ? convertToString(c.ToBsonDocument()["store"]) : null;
                    ordr.status = check_field(c.ToBsonDocument(), "status") ? convertToInt(c.ToBsonDocument()["status"]) : 0;
                    if (ordr.status == 1)
                    {
                        ordr.Order_Status = "Pending";
                    }
                    else if (ordr.status == 2)
                    {
                        ordr.Order_Status = "Payment Successful";
                    }
                    else if (ordr.status == 3)
                    {
                        ordr.Order_Status = "Payment Failed";
                    }
                    else if (ordr.status == 4)
                    {
                        ordr.Order_Status = "Will Ship";
                    }
                    else if (ordr.status == 5)
                    {
                        ordr.Order_Status = "Shipped";
                    }
                    else if (ordr.status == 6)
                    {
                        ordr.Order_Status = "Order Delayed";
                    }
                    else if (ordr.status == 7)
                    {
                        ordr.Order_Status = "Order Cancelled";
                    }
                    else if (ordr.status == 13)
                    {
                        ordr.Order_Status = "Order Disputed";
                    }
                    else
                    {
                        ordr.Order_Status = "";
                    }
                    ordr.order_date = check_field(c.ToBsonDocument(), "create_ts") ? convertToDate(c.ToBsonDocument()["create_ts"]).ToShortDateString() : System.DateTime.Now.ToLongDateString();
                    ordr.Cust_Name = convertToString(c.ToBsonDocument()["user_data"]["first_name"]) + " " + convertToString(c.ToBsonDocument()["user_data"]["last_name"]);
                    ordr.email_id = convertToString(c.ToBsonDocument()["user_data"]["email_id"]);
                    ordr.contact_number = convertToString(c.ToBsonDocument()["shipping_address"]["mobile_number"]);
                    var billing_address = convertToString(c.ToBsonDocument()["shipping_address"]["name"]) + "\n"
                                          + convertToString(c.ToBsonDocument()["shipping_address"]["address"]) + "\n"
                                          + convertToString(c.ToBsonDocument()["shipping_address"]["city"]) + "\n"
                                          + convertToString(c.ToBsonDocument()["shipping_address"]["state"]) + " "
                                          + convertToString(c.ToBsonDocument()["shipping_address"]["pincode"]);

                    ordr.billing_address = billing_address;
                    ordr.bal_points = convertToString(opr.GetUserPointBal(ordr.email_id, ordr.store));
                    ordr.paid_amount = convertToDouble(c.ToBsonDocument()["paid_amount"]);
                    ordr.points = convertToLong(c.ToBsonDocument()["points"]);
                    ordr.points_amount = convertToDouble(c.ToBsonDocument()["points_amount"]);

                    if (convertToString(c.ToBsonDocument()["egift_vou_no"]) != null && convertToString(c.ToBsonDocument()["egift_vou_no"]) != "")
                    {
                        var EgiftVouNos = convertToString(c.ToBsonDocument()["egift_vou_no"]);
                        string VouNo = "";
                        for (int m = 0; m < EgiftVouNos.Count(); m++)
                        {
                            if (EgiftVouNos[m].ToString() != "undefined" && EgiftVouNos[m].ToString() != "undef")
                            {
                                if (VouNo == "")
                                {
                                    VouNo = EgiftVouNos[m].ToString();
                                }
                                else
                                {
                                    VouNo = VouNo + "," + EgiftVouNos[m].ToString();
                                }
                            }
                        }
                        ordr.egift_vou_no = VouNo;
                    }
                    ordr.egift_vou_amt = convertToDouble(c.ToBsonDocument()["egift_vou_amt"]);
                    mongo_cart_data cart_item = JsonConvert.DeserializeObject<mongo_cart_data>(c.ToBsonDocument()["cart_data"].ToString());
                    ordr.cart_data = JsonConvert.DeserializeObject<List<cart_item>>(cart_item.cart_data);
                    ordr.shipping_info = convertToString(c.ToBsonDocument()["shipping_info"]);
                    ordr.courier_track_no = convertToString(c.ToBsonDocument()["courier_track_no"]);
                    ordr.courier_track_link = convertToString(c.ToBsonDocument()["courier_track_link"]);
                    ordr.courier_cost = convertToDouble(c.ToBsonDocument()["courier_cost"]);
                    ordr.shipping_date = check_field(c.ToBsonDocument(), "dispatch_date") ? convertToDate(c.ToBsonDocument()["dispatch_date"]).ToShortDateString() : "";
                    //(c.ToBsonDocument(), "total_amount") ? convertToDecimal(c.ToBsonDocument()["total_amount"]) : 0
                    //dispute dis = new dispute();
                    //dis.dispute_ts = check_field(c.ToBsonDocument(), "dispute_data") ? convertToDate(c.ToBsonDocument()["dispute_data"]["dispute_ts"]).ToShortDateString(): System.DateTime.Now.ToLongDateString();

                    ordr.dispute_data = JsonConvert.DeserializeObject<dispute>(c.ToBsonDocument()["dispute_data"].ToString());
                    //ordr.dispute_notes = convertToString(c.ToBsonDocument()["dispute_notes"]);





                }

            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return ordr;

        }

        public List<user_rigths> getUserRights(long USER_ID, long FLAG)
        {
            DataAccess da = new DataAccess();
            List<user_rigths> lur = new List<user_rigths>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_rights"
                                , da.Parameter("_user_id", USER_ID)
                                , da.Parameter("_flag", FLAG));

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        user_rigths our = new user_rigths();
                        our.user_right_id = convertToLong(dtResult.Rows[i]["user_right_id"].ToString());
                        our.user_right_desc = dtResult.Rows[i]["user_right_desc"].ToString();
                        lur.Add(our);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lur;

        }


        //public List<mongo_user_rights> getUserRights(long USER_ID, long FLAG)
        //{
        //    DataAccess da = new DataAccess();
        //    string q = "{'user_right_id':'" + convertToInt(USER_ID) + "'}";
        //    BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>(q);
        //    QueryDocument queryDoc = new QueryDocument(document);
        //    MongoCursor cursor = da.execute_mongo_db("user_rights", queryDoc);
        //    List<mongo_user_rights> user_right = new List<mongo_user_rights>();


        //    try
        //    {

        //        foreach (var c in cursor)
        //        {
        //            mongo_user_rights mongo_user = new mongo_user_rights();

        //            mongo_user.user_right_id = check_field(c.ToBsonDocument(), "user_right_id") ? convertToInt(c.ToBsonDocument()["user_right_id"]) : 0;
        //            mongo_user.user_right_desc = check_field(c.ToBsonDocument(), "user_right_desc") ? convertToString(c.ToBsonDocument()["user_right_desc"]) : null;
        //            user_right.Add(mongo_user);
        //        }


        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }
        //    return user_right;

        //}

        public List<registration> getUserIntimateData(string company, long pagenum, string useremail)
        {
            DataAccess da = new DataAccess();
            List<registration> lr = new List<registration>();
            DataTable dtResult = new DataTable();

            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_data_pagewise"
                                , da.Parameter("_company", company)
                                , da.Parameter("_user_email", useremail)
                                , da.Parameter("_flag", 1)
                                , da.Parameter("_pagenum", pagenum)
                                , da.Parameter("_record_per_page", 500)
                                , da.Parameter("_limit1", 0)
                                , da.Parameter("_limit2", 0));

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        registration or = new registration();
                        or.first_name = dtResult.Rows[i]["first_name"].ToString();
                        or.last_name = dtResult.Rows[i]["last_name"].ToString();
                        or.email_id = dtResult.Rows[i]["email_id"].ToString();
                        or.mobile_no = dtResult.Rows[i]["mobile_no"].ToString();
                        or.street = dtResult.Rows[i]["street"].ToString();
                        or.status = convertToInt(dtResult.Rows[i]["status"].ToString());
                        or.points = convertToDecimal(dtResult.Rows[i]["points"].ToString());
                        or.user_status = dtResult.Rows[i]["user_status"].ToString();
                        or.password = dtResult.Rows[i]["password"].ToString();
                        or.location = dtResult.Rows[i]["location"].ToString();
                        or.Total_Page = convertToLong(dtResult.Rows[i]["Total_Page"].ToString());
                        or.Total_Records = convertToLong(dtResult.Rows[i]["Total_Records"].ToString());
                        lr.Add(or);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lr;

        }

        //public List<OutCollection> postAssignRights(user_assign_rigths uar)
        //{

        //    DataAccess da = new DataAccess();
        //    List<OutCollection> oc = new List<OutCollection>();

        //    try
        //    {

        //        List<long> RIGHT_ID;
        //        RIGHT_ID = uar.RIGHTS_IDS;
        //        string strItemTagNo = "";
        //        for (int i = 0; i < RIGHT_ID.Count; i++)
        //        {
        //            int j = da.ExecuteSP("user_rights_ins_del", ref oc
        //            , da.Parameter("_user_id", uar.USER_ID)
        //            , da.Parameter("_right_id", convertToLong(RIGHT_ID[i].ToString()))
        //            , da.Parameter("_created_by", "admin")
        //            , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
        //            , da.Parameter("_flag", 1)
        //            , da.Parameter("_outmsg", String.Empty, true));

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        ExternalLogger.LogError(ex, this, "#");
        //        throw ex;
        //    }

        //    return oc;

        //}


        public string postAssignRights(mongo_user_assign_rigths uar)
        {
            try
            {
                List<long> RIGHT_ID;
                RIGHT_ID = uar.RIGHTS_IDS;
                uar.RIGHTS_IDS = RIGHT_ID;
                //  string strItemTagNo = "";

                // uar.create= System.DateTime.Now.ToLocalTime();
                dal.DataAccess dal = new DataAccess();
                BsonDocument bd_cat = uar.ToBsonDocument();
                string result = dal.mongo_write("user_rights_map", bd_cat);
                return result;





                //for (int i = 0; i < RIGHT_ID.Count; i++)
                //{
                //    //int j = da.ExecuteSP("user_rights_ins_del", ref oc
                //    //, da.Parameter("_user_id", uar.USER_ID)
                //    //, da.Parameter("_right_id", convertToLong(RIGHT_ID[i].ToString()))
                //    //, da.Parameter("_created_by", "admin")
                //    //, da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                //    //, da.Parameter("_flag", 1)
                //    //, da.Parameter("_outmsg", String.Empty, true));

                //}
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }


        }

        public string postExportCustList(expo_cust_grid ecg)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();

            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                string strCustFileId = "";
                strCustFileId = Guid.NewGuid().ToString();
                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strCustFileId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);

                sw.WriteLine("First Name,Last Name,User ID,Password,Status,Location");
                for (int i = 0; i < ecg.GridData.Count(); i++)
                {
                    bln = true;
                    strLine = convertToString(ecg.GridData[i].first_name) + "," + convertToString(ecg.GridData[i].last_name) + "," + convertToString(ecg.GridData[i].email_id) + "," + convertToString(ecg.GridData[i].password) + "," + convertToString(ecg.GridData[i].user_status) + "," + convertToString(ecg.GridData[i].location);
                    sw.WriteLine(strLine);
                }
                sw.Close();
                return strCustFileId;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }



        }

        //public void ExportToExcel(DataTable dt)
        //{
        //    if ((dt.Rows.Count > 0) && (dt != null))
        //    {
        //        string filename = "CMS_Report" + System.DateTime.Now.Date.ToString("yy-MM-dd") + ".xls";
        //        System.IO.StringWriter tw = new System.IO.StringWriter();
        //        System.Web.UI.HtmlTextWriter hw = new System.Web.UI.HtmlTextWriter(tw);
        //        DataGrid dgGrid = new DataGrid();
        //        dgGrid.DataSource = dt;
        //        dgGrid.DataBind();

        //        //Get the HTML for the control.
        //        dgGrid.RenderControl(hw);
        //        //Write the HTML back to the browser.
        //        //Response.ContentType = application/vnd.ms-excel;
        //        Response.ContentType = "application/vnd.ms-excel";
        //        Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + "");
        //        this.EnableViewState = false;
        //        Response.Write(tw.ToString());
        //        Response.End();

        //    }
        //    else
        //    {
        //        Response.Write("no record");
        //        Response.End();
        //    }
        //}


        public string getExportDailySalesReport(string company, string ReportFromDate, string ReportToDate)
        {

            DataAccess da = new DataAccess();
            dal.DataAccess dal = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                string strReportId = "";
                strReportId = "DSWReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");


                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);


                dtResult = da.ExecuteDataTable("get_daily_sales_wise_report"
                               , da.Parameter("_comapny", company)
                               , da.Parameter("_report_frm_date", ReportFromDate)
                               , da.Parameter("_report_to_date", ReportToDate)
                               );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {

                    MongoCursor cursor = dal.execute_mongo_db_sort("category", "Name");
                    var Category_List = new List<category>();

                    foreach (var c in cursor)
                    {
                        category cat = new category();
                        cat.id = check_field(c.ToBsonDocument(), "id") ? convertToString(c.ToBsonDocument()["id"]) : null;
                        cat.Name = check_field(c.ToBsonDocument(), "Name") ? convertToString(c.ToBsonDocument()["Name"]) : null;
                        Category_List.Add(cat);
                    }


                    sw.WriteLine("Sl.No, Date, Email/Mobile No, Alternate Email, Phone Number, Order Number, Customer Name, Contact No, Shipping Name, Address, City, State, Pin,Brand,Level 3 Category , Article No, Article Name, Size, Qty, Rate/Uni, Points Reedemed, Cash, Total Amount ,Order Status,Service Provider	,Dispatch Date,	Courier Tracking Number,Courier Tracking Link");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string cart_data = "";
                        cart_data = dtResult.Rows[i]["cart_data"].ToString();
                        List<cart_item> ordlist = JsonConvert.DeserializeObject<List<cart_item>>(cart_data.ToString());
                        if (ordlist.Count() > 0)
                        {
                            foreach (cart_item ordupdt in ordlist)
                            {
                                int count = 0;
                                string Product_ID = "";

                                var Prod_Id = ordupdt.id.ToString().Split('.');

                                Product_ID = Prod_Id[0].ToString();

                                BsonDocument document = MongoDB.Bson.Serialization.BsonSerializer.Deserialize<BsonDocument>("{id:'" + Product_ID + "'}");
                                QueryDocument queryDoc = new QueryDocument(document);
                                MongoCursor cursor1 = dal.execute_mongo_db("product", queryDoc);

                                foreach (var c1 in cursor1)
                                {
                                    string Category_Id = "";
                                    string CategoryName = "";
                                    Category_Id = check_field(c1.ToBsonDocument(), "parent_cat_id") ? convertToString(c1.ToBsonDocument()["parent_cat_id"]) : "0";

                                    if (Category_Id != "" && Category_Id != "0")
                                    {

                                        for (var m = 0; m < Category_List.Count(); m++)
                                        {
                                            if (Category_List[m].id.ToString() == Category_Id)
                                            {
                                                CategoryName = Category_List[m].Name.ToString();
                                                break;
                                            }
                                        }
                                    }
                                    string Size = "";
                                    if (ordupdt.selected_size != null)
                                    {
                                        Size = convertToString(ordupdt.selected_size);
                                    }
                                    string SlNo = convertToString(i + 1 + count);
                                    strLine = SlNo + "," + convertToString(dtResult.Rows[i]["ReportDate"]) + "," + convertToString(dtResult.Rows[i]["Email"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["Email"])) + "," + convertToString(dtResult.Rows[i]["PhoneNumber"]) + "," + convertToString(dtResult.Rows[i]["OrderNumber"]) + "," + convertToString(dtResult.Rows[i]["CustomerName"]) + "," + convertToString(dtResult.Rows[i]["ContactNo"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Name"]) + ",''" + (convertToString(dtResult.Rows[i]["Shipping_Address"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["Shipping_City"]) + "," + convertToString(dtResult.Rows[i]["Shipping_State"]) + "," + convertToString(dtResult.Rows[i]["Shipping_Pin"]) + "," + convertToString(ordupdt.brand) + "," + convertToString(CategoryName) + "," + convertToString(ordupdt.sku).Replace(",", " ") + "," + convertToString(ordupdt.name) + "," + Size + "," + convertToString(ordupdt.quantity) + "," + convertToString(ordupdt.mrp) + "," + convertToString(dtResult.Rows[i]["PointsReedemed"]) + "," + convertToString(dtResult.Rows[i]["Cash"]) + "," + convertToString(dtResult.Rows[i]["Totalamount"]) + "," + convertToString(dtResult.Rows[i]["OrderStatus"]) + "," + convertToString(dtResult.Rows[i]["ServiceProvider"]) + "," + convertToString(dtResult.Rows[i]["Dispatch_Date"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackNo"]) + "," + convertToString(dtResult.Rows[i]["CourierTrackLink"]);
                                    sw.WriteLine(strLine);
                                }


                                count++;
                            }
                        }
                    }
                    sw.Close();

                }
                return strReportId;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }



        }

        public string getExportCustPointBalReport(string company, string email_id)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            registration_repository rr = new registration_repository();
            try
            {
                Boolean bln = false;
                //evoke mongodb connection method in dal//
                string result = "";
                string toemail = "";
                string strFileName = "";
                string strLine = "";
                string strReportId = "";
                strReportId = "CPBReport_" + company.Replace(" ", "") + System.DateTime.Now.ToLocalTime().ToString("MM-dd-yyyy hh-mm-ss").Replace("-", "").Replace(" ", "");


                strFileName = System.Configuration.ConfigurationSettings.AppSettings["ordercsv"].ToString() + "\\" + strReportId + ".csv";
                System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);


                dtResult = da.ExecuteDataTable("get_user_points"
                               , da.Parameter("_company", company)
                               , da.Parameter("_email_id", email_id)
                               );


                if (dtResult != null && dtResult.Rows.Count > 0)
                {

                    sw.WriteLine("Sl.No, First Name,Last Name, Email/Mobile No, Alternate Email, Mobile No., Location, Points Balance, Points Alloted");
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        string SlNo = convertToString(i + 1);
                        strLine = SlNo + "," + convertToString(dtResult.Rows[i]["first_name"]) + "," + convertToString(dtResult.Rows[i]["last_name"]) + "," + convertToString(dtResult.Rows[i]["email_id"]) + "," + rr.getUserEMailAddress(convertToString(dtResult.Rows[i]["email_id"])) + "," + convertToString(dtResult.Rows[i]["mobile_no"]) + ",''" + (convertToString(dtResult.Rows[i]["location"]).Replace(",", " ")).Replace("\n", " ") + "''," + convertToString(dtResult.Rows[i]["PointsBalance"]) + "," + convertToString(dtResult.Rows[i]["PointsAlloted"]);
                        sw.WriteLine(strLine);
                    }
                    sw.Close();

                }
                return strReportId;
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }




        }

        public List<OutCollection> postDeleteAssignRights(user_assign_rigths uar)
        {

            DataAccess da = new DataAccess();
            List<OutCollection> oc = new List<OutCollection>();


            try
            {

                List<long> RIGHT_ID;
                RIGHT_ID = uar.RIGHTS_IDS;

                for (int i = 0; i < RIGHT_ID.Count; i++)
                {

                    int j = da.ExecuteSP("user_rights_ins_del", ref oc
                    , da.Parameter("_user_id", uar.USER_ID)
                    , da.Parameter("_right_id", convertToLong(RIGHT_ID[i].ToString()))
                    , da.Parameter("_created_by", "admin")
                    , da.Parameter("_created_ts", System.DateTime.Now.ToLocalTime())
                    , da.Parameter("_flag", 2)
                    , da.Parameter("_outmsg", String.Empty, true));

                }



            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }

            return oc;

        }

        public List<user_rigths> getuser_assign_rights(string USER_NAME, long Comp_ID)
        {

            DataAccess da = new DataAccess();
            DataTable dtResult = new DataTable();
            List<user_rigths> lur = new List<user_rigths>();
            try
            {
                string[] strout = new string[0];

                dtResult = da.ExecuteDataTable("get_user_assign_rights"
                                , da.Parameter("_user_name", USER_NAME)
                                , da.Parameter("_company", Comp_ID)
                                );

                if (dtResult != null && dtResult.Rows.Count > 0)
                {
                    for (int i = 0; i < dtResult.Rows.Count; i++)
                    {
                        user_rigths our = new user_rigths();
                        our.USER_ID = convertToLong(dtResult.Rows[i]["user_id"].ToString());
                        our.user_right_id = convertToLong(dtResult.Rows[i]["user_right_id"].ToString());
                        lur.Add(our);
                    }
                }
            }
            catch (Exception ex)
            {
                ExternalLogger.LogError(ex, this, "#");
                throw ex;
            }
            return lur;
        }

    }

}
