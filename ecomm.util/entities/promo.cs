﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
   public class promo
    {
       public string promo_id { get; set; }
       public string promo_name { get; set; }
       public string company { get; set; }
       public decimal min_purchase { get; set; }
       public string disc_type { get; set; }
       public decimal max_discount { get; set; }
       public string start_date { get; set; }
       public string end_date { get; set; }


        
    }                  
}
