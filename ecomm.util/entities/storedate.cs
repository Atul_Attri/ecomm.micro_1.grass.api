﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ecomm.util.entities
{
    public class storedate
    {
        public string start_date { get; set; }
        public string end_date { get; set; }
    }
}
